module.exports = {
  apps: [
    {
      name: 'RCN-BE-1',
      script: './bin/www',
      args: 'start',
      interpreter: "./node_modules/@babel/node/bin/babel-node.js",
      exec_mode: "fork_mode",
      env: {
        PORT: 3001,
        NODE_ENV: process.env.NODE_ENV
      },
      autorestart: true,
      max_memory_restart: "1G"
    },
    {
      name: 'RCN-BE-2',
      script: './bin/www',
      args: 'start',
      interpreter: "./node_modules/@babel/node/bin/babel-node.js",
      exec_mode: "fork_mode",
      env: {
        PORT: 3002,
        NODE_ENV: process.env.NODE_ENV
      },
      autorestart: true,
      max_memory_restart: "1G"
    },
    {
      name: 'RCN-BE-3',
      script: './bin/www',
      args: 'start',
      interpreter: "./node_modules/@babel/node/bin/babel-node.js",
      exec_mode: "fork_mode",
      env: {
        PORT: 3003,
        NODE_ENV: process.env.NODE_ENV
      },
      autorestart: true,
      max_memory_restart: "1G"
    },
    {
      name: 'RCN-BE-4',
      script: './bin/www',
      args: 'start',
      interpreter: "./node_modules/@babel/node/bin/babel-node.js",
      exec_mode: "fork_mode",
      env: {
        PORT: 3004,
        NODE_ENV: process.env.NODE_ENV
      },
      autorestart: true,
      max_memory_restart: "1G"
    }
  ]
}
