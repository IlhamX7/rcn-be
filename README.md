### requirement
1. install redis
2. install postgresql v12
3. install npm v6.14.13
4. install nodejs v14.17.3
OR
1. install docker.io v20.10.5
2. install docker-compose v1.29.0

### running app
```
cd RCN-BE
cp .env-example .env
npm install
npm run migrate
npm run seed
npm run dev
```

### reset db
```
npm run reset-db
```

### generate model sequelize from command
```
npx sequelize-cli model:generate --name User --attributes firstName:string,lastName:string,email:string
```

### create user and create new database postgresql
```
sudo -u postgres psql
postgres=# create database reeracoendb;
postgres=# create user reeracoen with encrypted password 'password';
postgres=# grant all privileges on database reeracoendb to reeracoen;
```

### show listen port
```
netstat -ano | findstr :8080
npx kill-port 8080
```