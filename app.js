import cookieParser from 'cookie-parser';
import cors from 'cors';
import express from 'express';
import fileUpload from 'express-fileupload';
import logger from 'morgan';

import { redisClient } from './helpers/redis';
import { respond } from './helpers/utils';
import db from './models/index';
import api from './routes/api';
import indexRouter from './routes/index';

const app = express();
redisClient.on('connect', () => {
  console.log("Client connected to redis...");
});

redisClient.on('error', (err) => {
  console.log(err.message);
});

db.sequelize.authenticate().then(function () {
  console.log("Connection database has been established successfully.")
}).catch(function (error) {
  console.log("Error creating connection:", error.message);
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());
app.use(fileUpload());
app.io = require('socket.io')();
app.io.use((socket, next) => {
  socket.id = socket.handshake.auth.email;
  socket.userId = socket.handshake.auth.id;
  next();
});
app.io.on('connection', function (socket) {
  console.log(`user ${socket.id} connected`);
  socket.on('disconnect', function () {
    console.log(`User ${socket.id} disconnected`);
  });
});
app.use(function (req, res, next) {
  req.io = app.io;
  next();
});

app.use('/', indexRouter);
app.use('/api/v1', api);

app.use((req, res) =>
  respond.failed(res, 404, 'The requested URL was not found on this server'));
module.exports = app;
