'use strict';
import { Op } from 'sequelize';

import { sequelizeOrLike, sequelizePagination } from '../helpers/utils';
import Model from '../models';
const Overtime = Model.sequelize.models.overtimes;

const repo = {
  /**
   * 
   * @param {{
   * user_id: Number
   * request_date: Date
   * attendance_shift_id: Number
   * overtime_before_duration: String
   * overtime_after_duration: String
   * break_before_duration: String
   * break_after_duration: String
   * notes: Text
   * company_id: Number
   * }} params 
   * @returns 
   */
  create: async (params) => {
    return Overtime.create({
      user_id: params.user_id,
      request_date: params.request_date,
      attendance_shift_id: params.attendance_shift_id,
      overtime_before_duration: params.overtime_before_duration,
      overtime_after_duration: params.overtime_after_duration,
      break_before_duration: params.break_before_duration,
      break_after_duration: params.break_after_duration,
      notes: params.notes,
      company_id: params.company_id
    })
  },

  /**
   * 
   * @param {{
   * user_id: Number
   * request_date: Date
   * attendance_shift_id: Number
   * overtime_before_duration: Time
   * overtime_after_duration: Time
   * break_before_duration: Time
   * break_after_duration: Time
   * notes: Text
   * company_id: Number
   * }} params 
   * @returns 
   */
  update: async (params) => {
    const res = await Overtime.update({
      request_date: params.request_date,
      overtime_before_duration: params.overtime_before_duration,
      overtime_after_duration: params.overtime_after_duration,
      break_before_duration: params.break_before_duration,
      break_after_duration: params.break_after_duration,
      notes: params.notes,
    }, {
      where: { id: params.id },
      returning: true,
      raw: true
    })

    if (res[1][0] === undefined)
      return null
    return res[1][0]
  },

  /**
   * 
   * @param {Number} id
   * @returns 
   */
  delete: async (id) => {
    return Overtime.destroy({
      where: { id: id },
      returning: true,
      raw: true
    })
  },

  /**
   * 
   * @param {Number} id 
   * @param {Number} companyId
   * @returns 
   */
  findOneById: async (id, companyId) => {
    return Overtime.findOne({
      where: { id: id, company_id: companyId }
    })
  },

  /**
   * @param {Number} companyId
   * @returns 
   */
  findAll: async (companyId) => {
    return Overtime.findAll({
      where: { company_id: companyId }
    })
  },

  /**
   * find all menu by search term
   * @param {string} term 
   * @param {Number} companyId
   * @param {number} page 
   * @param {number} limit 
   */
  findAllBySearchTermAndPaginated: async (term, companyId, page, limit) => {
    const query = {
      where: { company_id: companyId },
      raw: true,
      page,
      limit,
      order: [['id', 'DESC']],
    }

    if (term) {
      const searchLike = sequelizeOrLike(term, [
        'overtimes.user_id',
        'overtimes.request_date',
        'overtimes.attendance_shift_id',
        'overtimes.overtime_before_duration',
        'overtimes.overtime_after_duration',
        'overtimes.break_before_duration',
        'overtimes.break_after_duration',
        'overtimes.notes'
      ]);

      query.where[Op.and] = { [Op.or]: searchLike }
    }

    return sequelizePagination(Overtime, query);
  },
}

export default repo;