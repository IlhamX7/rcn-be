'use strict';
import { Op } from 'sequelize';

import { sequelizeOrLike, sequelizePagination } from '../helpers/utils';
import Model from '../models';
const GroupUser = Model.sequelize.models.group_users;

const repo = {
  /**
   * 
   * @param {{
   * code: String
   * title: String
   * company_id: Number
   * }} params 
   * @returns 
   */
  create: async (params) => {
    return GroupUser.create({
      code: params.code,
      title: params.title,
      company_id: params.company_id
    })
  },

  /**
   * 
   * @param {{
   * id: Number
   * title: String
   * }} params 
   * @returns 
   */
  update: async (params) => {
    const res = await GroupUser.update({
      title: params.title
    }, {
      where: { id: params.id },
      returning: true,
      raw: true
    })

    if (res[1][0] === undefined)
      return null
    return res[1][0]
  },

  /**
   * 
   * @param {Number} id
   * @returns 
   */
  delete: async (id) => {
    return GroupUser.destroy({
      where: { id: id }
    })
  },

  /**
   * 
   * @param {Number} id 
   * @param {Number} companyId
   * @returns 
   */
  findOneById: async (id, companyId) => {
    return GroupUser.findOne({
      where: { id: id, company_id: companyId }
    })
  },

  /**
   * @param {Number} companyId
   * @returns 
   */
  findAll: async (companyId) => {
    return GroupUser.findAll({
      where: { company_id: companyId }
    })
  },

  /**
   * find all menu by search term
   * @param {string} term 
   * @param {Number} companyId
   * @param {number} page 
   * @param {number} limit 
   */
  findAllBySearchTermAndPaginated: async (term, companyId, page, limit) => {
    const query = {
      where: { company_id: companyId },
      raw: true,
      page,
      limit,
      order: [['id', 'DESC']],
    }

    if (term) {
      const searchLike = sequelizeOrLike(term, [
        'group_users.code',
        'group_users.title'
      ]);

      query.where[Op.and] = { [Op.or]: searchLike }
    }

    return sequelizePagination(GroupUser, query);
  },
}

export default repo;