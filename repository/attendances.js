'use strict';
import { Op } from 'sequelize';

import { sequelizeOrLike, sequelizePagination } from '../helpers/utils';
import Model from '../models';
const Attendance = Model.sequelize.models.attendances;

const repo = {
  /**
   * 
   * /**
   * @param {{
   * user_id: number
   * attendance_shift_id: number
   * attendance_date: any
   * clock_in: any
   * clock_out: any
   * break_in: any
   * break_out: any
   * location: String
   * face_detection_accuracy: String
   * company_id: number
   * }} params 
   * @returns 
   */
  create: async (params) => {
    return Attendance.create({
      user_id: params.user_id,
      attendance_shift_id: params.attendance_shift_id,
      attendance_date: params.attendance_date,
      clock_in: params.clock_in,
      clock_out: params.clock_out,
      break_in: params.break_in,
      break_out: params.break_out,
      location: params.location,
      face_detection_accuracy: params.face_detection_accuracy || null,
      company_id: params.company_id
    })
  },

  /**
   * 
   * @param {{
   * id: Number
   * attendance_shift_id: number
   * attendance_date: any
   * clock_in: any
   * clock_out: any
   * break_in: any
   * break_out: any
   * location: String
   * face_detection_accuracy: String
   * }} params 
   * @returns 
   */
  update: async (params) => {
    const res = await Attendance.update({
      attendance_shift_id: params.attendance_shift_id,
      clock_in: params.clock_in,
      clock_out: params.clock_out,
      break_in: params.break_in,
      break_out: params.break_out,
      location: params.location,
      face_detection_accuracy: params.face_detection_accuracy
    }, {
      where: { id: params.id },
      returning: true,
      raw: true
    })

    if (res[1][0] === undefined)
      return null
    return res[1][0]
  },

  /**
   * 
   * @param {Number} id
   * @returns 
   */
  delete: async (id) => {
    return Attendance.destroy({ where: { id: id }})
  },

  /**
   * 
   * @param {Number} id 
   * @param {Number} companyId
   * @returns 
   */
  findOneById: async (id, companyId) => {
    return Attendance.findOne({
      where: { id: id, company_id: companyId }
    })
  },

  /**
   * @param {Number} companyId
   * @returns 
   */
  findAll: async (companyId) => {
    return Attendance.findAll({
      where: { company_id: companyId }
    })
  },

  /**
   * find all attendances by search term
   * @param {string} term 
   * @param {Number} companyId
   * @param {number} page 
   * @param {number} limit 
   */
  findAllBySearchTermAndPaginated: async (term, companyId, page, limit) => {
    const query = {
      where: { company_id: companyId },
      raw: true,
      page,
      limit,
      order: [['id', 'DESC']],
    }

    if (term) {
      const searchLike = sequelizeOrLike(term, [
        'attendances.user_id',
        'attendances.attendance_date'
      ]);

      query.where[Op.and] = { [Op.or]: searchLike }
    }

    return sequelizePagination(Attendance, query);
  },
}

export default repo;