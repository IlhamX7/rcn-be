'use strict';
import { Op } from 'sequelize';

import { sequelizeOrLike, sequelizePagination } from '../helpers/utils';
import Model from '../models';
const Position = Model.sequelize.models.positions;

const repo = {
  /**
   * @var {string} JOB_LEVEL_TOP_LEVEL
   */
   JOB_LEVEL_TOP_LEVEL: 'Top-level management',
   /**
    * @var {string} JOB_LEVEL_MIDDLE_LEVEL
    */
   JOB_LEVEL_MIDDLE_LEVEL: 'Middle-level management',
   /**
    * @var {string} JOB_LEVEL_LOWER_LEVEL
    */
   JOB_LEVEL_LOWER_LEVEL: 'Lower-level management',
  /**
   * 
   * @param {{
   * job_position: String
   * departement_id: Number
   * company_id: Number
   * }} params 
   * @returns 
   */
  create: async (params) => {
    return Position.create({
      job_position: params.job_position,
      job_level: repo.JOB_LEVEL_TOP_LEVEL,
      departement_id: params.departement_id,
      company_id: params.company_id
    })
  },

  /**
   * 
   * @param {{
   * job_position: String
   * departement_id: Number
   * }} params 
   * @returns 
   */
  update: async (params) => {
    const res = await Position.update({
      job_position: params.job_position,
      departement_id: params.departement_id
    }, {
      where: { id: params.id },
      returning: true,
      raw: true
    })

    if (res[1][0] === undefined)
      return null
    return res[1][0]
  },

  /**
   * 
   * @param {Number} id
   * @returns 
   */
  delete: async (id) => {
    return Position.destroy({ where: { id: id }})
  },

  /**
   * 
   * @param {Number} id 
   * @param {Number} companyId
   * @returns 
   */
  findOneById: async (id, companyId) => {
    return Position.findOne({
      where: { id: id, company_id: companyId }
    })
  },

  /**
   * @param {Number} companyId
   * @returns 
   */
  findAll: async (companyId) => {
    return Position.findAll({
      where: { company_id: companyId }
    })
  },

  /**
   * find all position by search term
   * @param {string} term 
   * @param {Number} companyId
   * @param {number} page 
   * @param {number} limit 
   */
  findAllBySearchTermAndPaginated: async (term, companyId, page, limit) => {
    const query = {
      where: { company_id: companyId },
      raw: true,
      page,
      limit,
      order: [['id', 'DESC']],
    }

    if (term) {
      const searchLike = sequelizeOrLike(term, [
        'positions.job_position',
        'positions.job_level',
        'positions.departement_id'
      ]);

      query.where[Op.and] = { [Op.or]: searchLike }
    }

    return sequelizePagination(Position, query);
  },
}

export default repo;