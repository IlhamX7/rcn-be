'use strict';
import { Op } from 'sequelize';

import { sequelizeOrLike, sequelizePagination } from '../helpers/utils';
import Model from '../models';
const TimeOffType = Model.sequelize.models.time_off_types;

const repo = {
  /**
   * 
   * /**
   * @param {{
   * title: String
   * active: boolean
   * company_id: number
   * }} params 
   * @returns 
   */
  create: async (params) => {
    return TimeOffType.create({
      title: params.title,
      active: params.active,
      company_id: params.company_id
    })
  },

  /**
   * 
   * @param {{
   * id: Number
   * title: String
   * active: boolean
   * }} params 
   * @returns 
   */
  update: async (params) => {
    const res = await TimeOffType.update({
      title: params.title,
      active: params.active,
    }, {
      where: { id: params.id },
      returning: true,
      raw: true
    })

    if (res[1][0] === undefined)
      return null
    return res[1][0]
  },

  /**
   * 
   * @param {Number} id
   * @returns 
   */
  delete: async (id) => {
    return TimeOffType.destroy({ where: { id: id }})
  },

  /**
   * 
   * @param {Number} id 
   * @param {Number} companyId
   * @returns 
   */
  findOneById: async (id, companyId) => {
    return TimeOffType.findOne({
      where: { id: id, company_id: companyId }
    })
  },

  /**
   * @param {Number} companyId
   * @returns 
   */
  findAll: async (companyId) => {
    return TimeOffType.findAll({
      where: { company_id: companyId }
    })
  },

  /**
   * 
   * @param {string} term 
   * @param {Number} companyId
   * @param {number} page 
   * @param {number} limit 
   */
  findAllBySearchTermAndPaginated: async (term, companyId, page, limit) => {
    const query = {
      where: { company_id: companyId },
      raw: true,
      page,
      limit,
      order: [['id', 'DESC']],
    }

    if (term) {
      const searchLike = sequelizeOrLike(term, [
        'time_off_types.title',
        'time_off_types.active'
      ]);

      query.where[Op.and] = { [Op.or]: searchLike }
    }

    return sequelizePagination(TimeOffType, query);
  },
}

export default repo;