'use strict';
import { Op } from 'sequelize';

import { sequelizeOrLike, sequelizePagination } from '../helpers/utils';
import Model from '../models';
const Menu = Model.sequelize.models.menus;

const repo = {
  /**
   * 
   * @param {{
   * title: String
   * link: String
   * parent_id: Number
   * icon: String
   * menu_for: String
   * company_id: Number
   * }} params 
   * @returns 
   */
  create: async (params) => {
    return Menu.create({
      title: params.title,
      link: params.link,
      parent_id: params.parent_id || 0,
      icon: params.icon || null,
      menu_for: params.menu_for,
      company_id: params.company_id
    })
  },

  /**
   * 
   * @param {{
   * id: Number
   * title: String
   * link: String
   * parent_id: Number
   * icon: String
   * menu_for: String
   * }} params 
   * @returns 
   */
  update: async (params) => {
    const res = await Menu.update({
      title: params.title,
      link: params.link,
      parent_id: params.parent_id || 0,
      icon: params.icon || null,
      menu_for: params.menu_for
    }, {
      where: { id: params.id },
      returning: true,
      raw: true
    })

    if (res[1][0] === undefined)
      return null
    return res[1][0]
  },

  /**
   * 
   * @param {Number} id
   * @returns 
   */
  delete: async (id) => {
    return Menu.destroy({
      where: { id: id },
      returning: true,
      raw: true
    })
  },

  /**
   * 
   * @param {Number} id 
   * @param {Number} companyId
   * @returns 
   */
  findOneById: async (id, companyId) => {
    return Menu.findOne({
      where: { id: id, company_id: companyId }
    })
  },

  /**
   * @param {Number} companyId
   * @returns 
   */
  findAll: async (companyId) => {
    return Menu.findAll({
      where: { company_id: companyId }
    })
  },

  /**
   * find all menu by search term
   * @param {string} term 
   * @param {Number} companyId
   * @param {number} page 
   * @param {number} limit 
   */
  findAllBySearchTermAndPaginated: async (term, companyId, page, limit) => {
    const query = {
      where: { company_id: companyId },
      raw: true,
      page,
      limit,
      order: [['id', 'DESC']],
    }

    if (term) {
      const searchLike = sequelizeOrLike(term, [
        'menus.title',
        'menus.link',
        'menus.menu_for'
      ]);

      query.where[Op.and] = { [Op.or]: searchLike }
    }

    return sequelizePagination(Menu, query);
  },
}

export default repo;