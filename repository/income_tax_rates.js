'use strict';
import { Op } from 'sequelize';

import { sequelizeOrLike, sequelizePagination } from '../helpers/utils';
import Model from '../models';
const IncomeTaxRates = Model.sequelize.models.income_tax_rates;

const repo = {
  /**
   * 
   * @param {{
   * title: String
   * income_minimum: Number
   * income_maximum: Number
   * percentage_of_tax_rate: Number
   * with_tax_id_number: Number
   * company_id: Number
   * }} params 
   * @returns 
   */
  create: async (params) => {
    return IncomeTaxRates.create({
      title: params.title,
      income_minimum: params.income_minimum ,
      income_maximum: params.income_maximum,
      percentage_of_tax_rate: params.percentage_of_tax_rate,
      with_tax_id_number: params.with_tax_id_number,
      company_id: params.company_id
    })
  },

  /**
   * 
   * @param {{
   * id: Number
   * title: String
   * income_minimum: Number
   * income_maximum: Number
   * percentage_of_tax_rate: Number
   * with_tax_id_number: Number
   * }} params 
   * @returns 
   */
  update: async (params) => {
    const res = await IncomeTaxRates.update({
      title: params.title,
      income_minimum: params.income_minimum,
      income_maximum: params.income_maximum,
      percentage_of_tax_rate: params.percentage_of_tax_rate,
      with_tax_id_number: params.with_tax_id_number
    }, {
      where: { id: params.id },
      returning: true,
      raw: true
    })

    if (res[1][0] === undefined)
      return null
    return res[1][0]
  },

  /**
   * 
   * @param {Number} id
   * @returns 
   */
  delete: async (id) => {
    return IncomeTaxRates.destroy({
      where: { id: id }
    })
  },

  /**
   * 
   * @param {Number} id 
   * @param {Number} companyId
   * @returns 
   */
  findOneById: async (id, companyId) => {
    return IncomeTaxRates.findOne({
      where: { id: id, company_id: companyId }
    })
  },

  /**
   * @param {Number} companyId
   * @returns 
   */
  findAll: async (companyId) => {
    return IncomeTaxRates.findAll({
      where: { company_id: companyId }
    })
  },

  /**
   * @param {string} term 
   * @param {Number} companyId
   * @param {number} page 
   * @param {number} limit 
   */
  findAllBySearchTermAndPaginated: async (term, companyId, page, limit) => {
    const query = {
      where: { company_id: companyId },
      raw: true,
      page,
      limit,
      order: [['id', 'DESC']],
    }

    if (term) {
      const searchLike = sequelizeOrLike(term, [
        'income_tax_rates.title',
        'income_tax_rates.income_minimum',
        'income_tax_rates.income_maximum',
        'income_tax_rates.percentage_of_tax_rate',
        'income_tax_rates.with_tax_id_number'
      ]);

      query.where[Op.and] = { [Op.or]: searchLike }
    }

    return sequelizePagination(IncomeTaxRates, query);
  },
}

export default repo;