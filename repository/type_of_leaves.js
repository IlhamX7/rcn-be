'use strict';
import { Op } from 'sequelize';

import { sequelizeOrLike, sequelizePagination } from '../helpers/utils';
import Model from '../models';
const TypeOfLeave = Model.sequelize.models.type_of_leaves;

const repo = {
  /**
   * 
   * /**
   * @param {{
   * title: String
   * time_period: number
   * active: boolean
   * company_id: number
   * }} params 
   * @returns 
   */
  create: async (params) => {
    return TypeOfLeave.create({
      title: params.title,
      time_period: params.time_period,
      active: params.active,
      company_id: params.company_id
    })
  },

  /**
   * 
   * @param {{
   * id: Number
   * title: String
   * time_period: number
   * active: boolean
   * }} params 
   * @returns 
   */
  update: async (params) => {
    const res = await TypeOfLeave.update({
      title: params.title,
      time_period: params.time_period,
      active: params.active
    }, {
      where: { id: params.id },
      returning: true,
      raw: true
    })

    if (res[1][0] === undefined)
      return null
    return res[1][0]
  },

  /**
   * 
   * @param {Number} id
   * @returns 
   */
  delete: async (id) => {
    return TypeOfLeave.destroy({ where: { id: id }})
  },

  /**
   * 
   * @param {Number} id 
   * @param {Number} companyId
   * @returns 
   */
  findOneById: async (id, companyId) => {
    return TypeOfLeave.findOne({
      where: { id: id, company_id: companyId }
    })
  },

  /**
   * @param {Number} companyId
   * @returns 
   */
  findAll: async (companyId) => {
    return TypeOfLeave.findAll({
      where: { company_id: companyId }
    })
  },

  /**
   * 
   * @param {string} term 
   * @param {Number} companyId
   * @param {number} page 
   * @param {number} limit 
   */
  findAllBySearchTermAndPaginated: async (term, companyId, page, limit) => {
    const query = {
      where: { company_id: companyId },
      raw: true,
      page,
      limit,
      order: [['id', 'DESC']],
    }

    if (term) {
      const searchLike = sequelizeOrLike(term, [
        'type_of_leaves.title',
        'type_of_leaves.time_period',
        'time_off_types.active'
      ]);

      query.where[Op.and] = { [Op.or]: searchLike }
    }

    return sequelizePagination(TypeOfLeave, query);
  },
}

export default repo;