'use strict';
import { Op } from 'sequelize';

import { sequelizeOrLike, sequelizePagination } from '../helpers/utils';
import Model from '../models';
const FormalEducation = Model.sequelize.models.formal_educations;

const repo = {
  /**
   * 
   * @param {{
   * grade: Number
   * institution_name: String
   * majors: String
   * start_year: String
   * end_year: String
   * score: Number
   * user_id: Number
   * company_id: Number
   * }} params 
   * @returns 
   */
  create: async (params) => {
    return FormalEducation.create({
      grade: params.title,
      institution_name: params.institution_name,
      majors: params.majors,
      start_year: params.start_year,
      end_year: params.end_year,
      score: params.score,
      user_id: params.user_id,  
      company_id: params.company_id
    })
  },

  /**
   * 
   * @param {{
   * id: Number
   * grade: Number
   * institution_name: String
   * majors: String
   * start_year: String
   * end_year: String
   * score: Number
   * }} params 
   * @returns 
   */
  update: async (params) => {
    const res = await FormalEducation.update({
      grade: params.grade,
      institution_name: params.institution_name,
      majors: params.majors,
      start_year: params.start_year,
      end_year: params.end_year,
      score: params.score
    }, {
      where: { id: params.id },
      returning: true,
      raw: true
    })

    if (res[1][0] === undefined)
      return null
    return res[1][0]
  },

  /**
   * 
   * @param {Number} id
   * @returns 
   */
  delete: async (id) => {
    return FormalEducation.destroy({
      where: { id: id },
      returning: true,
      raw: true
    })
  },

  /**
   * 
   * @param {Number} id 
   * @param {Number} companyId
   * @returns 
   */
  findOneById: async (id, companyId) => {
    return FormalEducation.findOne({
      where: { id: id, company_id: companyId }
    })
  },

  /**
   * @param {Number} companyId
   * @returns 
   */
  findAll: async (companyId) => {
    return FormalEducation.findAll({
      where: { company_id: companyId }
    })
  },

  /**
   * find all formal education by search term
   * @param {string} term 
   * @param {Number} companyId
   * @param {number} page 
   * @param {number} limit 
   */
  findAllBySearchTermAndPaginated: async (term, companyId, page, limit) => {
    const query = {
      where: { company_id: companyId },
      raw: true,
      page,
      limit,
      order: [['id', 'DESC']],
    }

    if (term) {
      const searchLike = sequelizeOrLike(term, [
        'formal_educations.grade',
        'formal_educations.institution_name',
        'formal_educations.majors',
        'formal_educations.start_year',
        'formal_educations.end_year',
        'formal_educations.score'
      ]);

      query.where[Op.and] = { [Op.or]: searchLike }
    }

    return sequelizePagination(FormalEducation, query);
  },
}

export default repo;