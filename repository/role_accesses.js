'use strict';
import { Op } from 'sequelize';

import { sequelizeOrLike, sequelizePagination } from '../helpers/utils';
import Model from '../models';

const RoleAccess = Model.sequelize.models.role_accesses;
const Menu = Model.sequelize.models.menus;

const repo = {
  /**
   * 
   * @param {number} userRole
   * @returns 
   */
  deleteByUserRole: async (userRole) => {
    return RoleAccess.destroy({
      where: { user_role: userRole }
    })
  },

  /**
   * 
   * @param {[{
   * user_role: string
   * menu_id: number
   * active: boolean
   * is_view: boolean
   * is_edit: boolean
   * is_insert: boolean
   * is_delete: boolean
   * }]} params 
   * @returns 
   */
  bulkCreate: async (params) => {
    return RoleAccess.bulkCreate(params)
  },

  /**
   * 
   * @param {string} link 
   * @param {boolean} active 
   * @param {string} role
   * @param {number} companyId
   * @returns 
   */
  findIncludesMenuByLinkAndActiveAndRole: (link, active, role, companyId) => {
    return RoleAccess.findOne({
      attributes: ['is_view', 'is_edit', 'is_delete', 'is_insert'],
      where: { active: active, user_role: role, company_id: companyId },
      include: [{
        model: Menu,
        attributes: [],
        required: true,
        where: { link: link }
      }],
      raw: true,
      nest: true,
      subQuery: false
    })
  },

  /**
   * find all role access by search term
   * @param {string} term 
   * @param {Number} companyId
   * @param {string} role
   * @param {number} page 
   * @param {number} limit 
   */
  findAllBySearchTermAndPaginated: async (term, companyId, role, page, limit) => {
    const query = {
      attributes: [
        'menus.id',
        'menus.title',
        'menus.menu_for',
        'menus.link',
        [Model.sequelize.col('role_access.active'), 'active'],
        [Model.sequelize.col('role_access.menu_id'), 'menu_id'],
        [Model.sequelize.col('role_access.ordering'), 'ordering'],
        [Model.sequelize.col('role_access.is_edit'), 'is_edit'],
        [Model.sequelize.col('role_access.is_insert'), 'is_insert'],
        [Model.sequelize.col('role_access.is_view'), 'is_view'],
        [Model.sequelize.col('role_access.is_delete'), 'is_delete'],
      ],
      where: { company_id: companyId },
      raw: true,
      nest: true,
      subQuery: false,
      page,
      limit,
      include: [{
        model: RoleAccess,
        attributes: [],
        required: false,
        where: { user_role: role.toUpperCase() }
      }],
      order: [[{ model: RoleAccess }, 'ordering', 'ASC']],
    }

    if (term) {
      const searchLike = sequelizeOrLike(term, [
        'menus.title',
        'menus.link',
        'menus.menu_for'
      ]);

      query.where[Op.and] = { [Op.or]: searchLike }
    }

    return sequelizePagination(Menu, query);
  },

  /**
   * 
   * @param {string} role 
   * @param {number} companyId 
   */
  findByRole: async (role, companyId) => {
   return RoleAccess.findAll({
      attributes: [
        [Model.sequelize.col('menu.id'), 'id'],
        [Model.sequelize.col('menu.parent_id'), 'parent_id'],
        [Model.sequelize.col('menu.title'), 'title'],
        [Model.sequelize.col('menu.link'), 'link'],
        [Model.sequelize.col('menu.icon'), 'icon'],
        'is_insert',
        'is_edit',
        'is_delete',
        'is_view'
      ],
      where: { active: true, user_role: role, company_id: companyId },
      order: [['ordering', 'asc']],
      raw: true,
      subQuery: false,
      nest: true,
      include: [{
        model: Menu,
        attributes: [],
        required: true,
        where: { menu_for: 'frontend' },
      }]
    })
  },

  /**
   * 
   * @param {number} ordering 
   * @param {number} companyId 
   * @returns 
   */
  findOneByOrdering: async (ordering, companyId) => {
    return RoleAccess.findOne({
      attributes: ['id','ordering'],
      where: { ordering: ordering, company_id: companyId },
      raw: true,
      subQuery: false,
      nest: true
    })
  },

  /**
   * 
   * @param {string} role 
   * @param {number} ordering 
   * @param {number} companyId 
   * @param {string} orderBy
   * @returns 
   */
  findOneIncludesMenuByRoleAndOrdering: async (role, ordering, companyId, orderBy) => {
    return RoleAccess.findOne({
      attributes: ['id', 'ordering'],
      include: [{
        model: Menu,
        attributes: [],
        required: true,
        where: { menu_for: 'frontend' }
      }],
      where: {
        user_role: role,
        company_id: companyId,
        ordering: {
          [Op.gt]: parseInt(ordering)
        }
      },
      raw: true,
      subQuery: false,
      nest: true,
      order: [['ordering', orderBy]]
    })
  },

  /**
   * 
   * @param {number} ordering 
   * @param {number} id 
   * @param {number} companyId
   * @returns 
   */
  updateOrdering: async (ordering, id, companyId) => {
    const res = await RoleAccess.update(
      { ordering: ordering },
      {
        where: { id: id, company_id: companyId },
        returning: true,
        raw: true
      })

    if (res[1][0] === undefined)
      return null
    return res[1][0]
  }
}

export default repo;