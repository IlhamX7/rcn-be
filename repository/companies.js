'use strict';
import { Op } from 'sequelize';

import { sequelizeOrLike, sequelizePagination } from '../helpers/utils';
import Model from '../models';
const Company = Model.sequelize.models.companies;

const repo = {
  /**
   * 
   * @param {{
   * company_name: String
   * organization_name: String
   * branch_id: Number
   * company_logo_url: String
   * address: Text
   * telp: String
   * email: String
   * founded_date: Dateonly
   * website_address: String
   * created_by: Number
   * updated_by: Number
   * active: boolean
   * }} params 
   * @returns 
   */
  create: async (params) => {
    return Company.create({
      company_name: params.company_name,
      organization_name: params.organization_name,
      branch_id: params.branch_id,
      company_logo_url: params.company_logo_url || null,
      address: params.address || null,
      telp: params.telp || null,
      email: params.email || null,
      founded_date: params.founded_date,
      website_address: params.website_address || null,
      active: params.active,
      created_by: params.created_by,
      updated_by: params.updated_by
    })
  },

  /**
   * 
   * @param {{
   * id: Number
   * company_name: String
   * organization_name: String
   * branch_id: Number
   * company_logo_url: String
   * address: Text
   * telp: String
   * email: String
   * founded_date: Dateonly
   * website_address: string
   * active: boolean
   * updated_by: Number
   * }} params 
   * @returns 
   */
  update: async (params) => {
    const res = await Company.update({
        company_name: params.company_name,
        organization_name: params.organization_name,
        branch_id: params.branch_id,
        company_logo_url: params.company_logo_url || null,
        address: params.address || null,
        telp: params.telp || null,
        email: params.email || null,
        founded_date: params.founded_date || null,
        website_address: params.website_address || null,
        active: params.active,
        updated_by: params.updated_by
    }, {
      where: { id: params.id },
      returning: true,
      raw: true
    })

    if (res[1][0] === undefined)
      return null
    return res[1][0]
  },

  /**
   * 
   * @param {Number} id
   * @returns 
   */
  delete: async (id) => {
    return Company.destroy({
      where: { id: id }
    })
  },

  /**
   * 
   * @param {Number} id 
   * @returns 
   */
  findOneById: async (id) => {
    return Company.findByPk(id)
  },

  /**
   * @returns 
   */
  findAll: async () => {
    return Company.findAll()
  },

  /**
   * find all menu by search term
   * @param {string} term 
   * @param {number} page 
   * @param {number} limit 
   */
  findAllBySearchTermAndPaginated: async (term, page, limit) => {
    const query = {
      where: {},
      raw: true,
      page,
      limit,
      order: [['id', 'DESC']],
    }

    if (term) {
      const searchLike = sequelizeOrLike(term, [
        'companies.company_name',
        'companies.organization_name',
        'companies.branch_id',
        'companies.company_logo_url',
        'companies.address',
        'companies.telp',
        'companies.email',
        'companies.founded_date',
        'companies.website_address',
        'companies.created_by',
        'companies.updated_by',
        'companies.active'
      ]);

      query.where[Op.and] = { [Op.or]: searchLike }
    }

    return sequelizePagination(Company, query);
  },
}

export default repo;