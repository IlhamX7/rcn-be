'use strict';
import { Op } from 'sequelize';

import { sequelizeOrLike, sequelizePagination } from '../helpers/utils';
import Model from '../models';
const Province = Model.sequelize.models.provinces;

const repo = {
  /**
   * 
   * @param {Number} id 
   * @returns 
   */
  findOneById: async (id) => {
    return Province.findOne({
      where: { id: id }
    })
  },

  /**
   * @returns 
   */
  findAll: async () => {
    return Province.findAll()
  },

  /**
   * find all province by search term
   * @param {string} term 
   * @param {number} page 
   * @param {number} limit 
   */
  findAllBySearchTermAndPaginated: async (term, page, limit) => {
    const query = {
      where: {},
      raw: true,
      page,
      limit,
      order: [['id', 'DESC']],
    }

    if (term) {
      const searchLike = sequelizeOrLike(term, [
        'provinces.province_name'
      ]);

      query.where[Op.and] = { [Op.or]: searchLike }
    }

    return sequelizePagination(Province, query);
  },
}

export default repo;