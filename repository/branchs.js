'use strict';
import { Op } from 'sequelize';

import { sequelizeOrLike, sequelizePagination } from '../helpers/utils';
import Model from '../models';
const Branch = Model.sequelize.models.branchs;

const repo = {
  /**
   * 
   * @param {{
   * title: String
   * description: Text
   * created_by: Number
   * updated_by: Number
   * }} params 
   * @returns 
   */
  create: async (params) => {
    return Branch.create({
      title: params.title,
      description: params.description,
      created_by: params.created_by,
      updated_by: params.updated_by,
    })
  },

  /**
   * 
   * @param {{
   * id: Number
   * title: String
   * description: Text
   * created_by: Number
   * updated_by: Number
   * }} params 
   * @returns 
   */
  update: async (params) => {
    const res = await Branch.update({
      title: params.title,
      description: params.description,
      updated_by: params.updated_by
    }, {
      where: { id: params.id },
      returning: true,
      raw: true
    })

    if (res[1][0] === undefined)
      return null
    return res[1][0]
  },

  /**
   * 
   * @param {Number} id
   * @returns 
   */
  delete: async (id) => {
    return Branch.destroy({
      where: { id: id }
    })
  },

  /**
   * 
   * @param {Number} id 
   * @returns 
   */
  findOneById: async (id) => {
    return Branch.findByPk(id)
  },

  /**
   * @returns 
   */
  findAll: async () => {
    return Branch.findAll()
  },

  /**
   * find all menu by search term
   * @param {string} term 
   * @param {number} page 
   * @param {number} limit  
   */
  findAllBySearchTermAndPaginated: async (term, page, limit) => {
    const query = {
      where: {},
      raw: true,
      page,
      limit,
      order: [['id', 'DESC']],
    }

    if (term) {
      const searchLike = sequelizeOrLike(term, [
        'branchs.title',
        'branchs.description',
        'branch.created_by',
        'branch.updated_by'
      ]);

      query.where[Op.and] = { [Op.or]: searchLike }
    }

    return sequelizePagination(Branch, query);
  },
}

export default repo;