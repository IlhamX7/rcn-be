'use strict';
import { Op } from 'sequelize';

import { sequelizeOrLike, sequelizePagination } from '../helpers/utils';
import Model from '../models';
const WorkingExperience = Model.sequelize.models.working_experiences;

const repo = {
  /**
   * 
   * @param {{
   * company: String
   * position: String
   * from_date: String
   * to_date: String
   * length_of_service: Number
   * certificate_of_employment: String
   * user_id: Number
   * company_id: Number
   * }} params 
   * @returns 
   */
  create: async (params) => {
    return WorkingExperience.create({
      company: params.company,
      position: params.position,
      from_date: params.from_date,
      to_date: params.to_date,
      length_of_service: params.length_of_service,
      certificate_of_employment: params.certificate_of_employment,
      user_id: params.user_id,
      company_id: params.company_id
    })
  },

  /**
   * 
   * @param {{
   * id: Number
   * company: String
   * position: String
   * from_date: String
   * to_date: String
   * length_of_service: Number
   * certificate_of_employment: String
   * }} params 
   * @returns 
   */
  update: async (params) => {
    const res = await WorkingExperience.update({
      company: params.company,
      position: params.position,
      from_date: params.from_date,
      to_date: params.to_date,
      length_of_service: params.length_of_service,
      certificate_of_employment: params.certificate_of_employment
    }, {
      where: { id: params.id },
      returning: true,
      raw: true
    })

    if (res[1][0] === undefined)
      return null
    return res[1][0]
  },

  /**
   * 
   * @param {Number} id
   * @returns 
   */
  delete: async (id) => {
    return WorkingExperience.destroy({
      where: { id: id },
      returning: true,
      raw: true
    })
  },

  /**
   * 
   * @param {Number} id 
   * @param {Number} companyId
   * @returns 
   */
  findOneById: async (id, companyId) => {
    return WorkingExperience.findOne({
      where: { id: id, company_id: companyId }
    })
  },

  /**
   * @param {Number} companyId
   * @returns 
   */
  findAll: async (companyId) => {
    return WorkingExperience.findAll({
      where: { company_id: companyId }
    })
  },

  /**
   * find all working experiences by search term
   * @param {string} term 
   * @param {Number} companyId
   * @param {number} page 
   * @param {number} limit 
   */
  findAllBySearchTermAndPaginated: async (term, companyId, page, limit) => {
    const query = {
      where: { company_id: companyId },
      raw: true,
      page,
      limit,
      order: [['id', 'DESC']],
    }

    if (term) {
      const searchLike = sequelizeOrLike(term, [
        'working_experiences.company',
        'working_experiences.position',
        'working_experiences.from_date',
        'working_experiences.to_date',
        'working_experiences.length_of_service',
        'working_experiences.certificate_of_employment'
      ]);

      query.where[Op.and] = { [Op.or]: searchLike }
    }

    return sequelizePagination(WorkingExperience, query);
  },
}

export default repo;