'use strict';
import { Op } from 'sequelize';

import { sequelizeOrLike, sequelizePagination } from '../helpers/utils';
import Model from '../models';
const AttendanceShift = Model.sequelize.models.attendance_shift;

const repo = {
  /**
   * 
   * /**
   * @param {{
   * title: String
   * schedule_clock_in: String
   * schedule_clock_out: String
   * schedule_break_in: String
   * schedule_break_out: String
   * late_tolerance: String
   * company_id: number
   * active: boolean
   * }} params 
   * @returns 
   */
  create: async (params) => {
    return AttendanceShift.create({
      title: params.title,
      schedule_clock_in: params.schedule_clock_in,
      schedule_clock_out: params.schedule_clock_out,
      schedule_break_in: params.schedule_break_in,
      schedule_break_out: params.schedule_break_out,
      late_tolerance: params.late_tolerance,
      active: params.active,
      company_id: params.company_id
    })
  },

  /**
   * 
   * @param {{
   * id: Number
   * title: String
   * schedule_clock_in: String
   * schedule_clock_out: String
   * schedule_break_in: String
   * schedule_break_out: String
   * late_tolerance: String
   * active: boolean
   * }} params 
   * @returns 
   */
  update: async (params) => {
    const res = await AttendanceShift.update({
      title: params.title,
      schedule_clock_in: params.schedule_clock_in,
      schedule_clock_out: params.schedule_clock_out,
      schedule_break_in: params.schedule_break_in,
      schedule_break_out: params.schedule_break_out,
      late_tolerance: params.late_tolerance,
      active: params.active,
    }, {
      where: { id: params.id },
      returning: true,
      raw: true
    })

    if (res[1][0] === undefined)
      return null
    return res[1][0]
  },

  /**
   * 
   * @param {Number} id
   * @returns 
   */
  delete: async (id) => {
    return AttendanceShift.destroy({ where: { id: id }})
  },

  /**
   * 
   * @param {Number} id 
   * @param {Number} companyId
   * @returns 
   */
  findOneById: async (id, companyId) => {
    return AttendanceShift.findOne({
      where: { id: id, company_id: companyId }
    })
  },

  /**
   * @param {Number} companyId
   * @returns 
   */
  findAll: async (companyId) => {
    return AttendanceShift.findAll({
      where: { company_id: companyId }
    })
  },

  /**
   * find all attendances shift by search term
   * @param {string} term 
   * @param {Number} companyId
   * @param {number} page 
   * @param {number} limit 
   */
  findAllBySearchTermAndPaginated: async (term, companyId, page, limit) => {
    const query = {
      where: { company_id: companyId },
      raw: true,
      page,
      limit,
      order: [['id', 'DESC']],
    }

    if (term) {
      const searchLike = sequelizeOrLike(term, [
        'attendance_shifts.title',
        'attendance_shifts.active'
      ]);

      query.where[Op.and] = { [Op.or]: searchLike }
    }

    return sequelizePagination(AttendanceShift, query);
  },
}

export default repo;