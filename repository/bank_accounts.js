'use strict';
import { Op } from 'sequelize';

import { sequelizeOrLike, sequelizePagination } from '../helpers/utils';
import Model from '../models';
const BankAccount = Model.sequelize.models.bank_accounts;

const repo = {
  /**
   * 
   * @param {{
   * user_id: Number
   * bank_name: String
   * bank_account_holder: String
   * account_of_bank_number: String
   * bank_branch: String
   * expiry_date_of_bank_account: String
   * company_id: Number
   * }} params 
   * @returns 
   */
  create: async (params) => {
    return BankAccount.create({
      user_id: params.user_id,
      bank_name: params.bank_name,
      bank_account_holder: params.bank_account_holder,
      account_of_bank_number: params.account_of_bank_number,
      bank_branch: params.bank_branch,
      expiry_date_of_bank_account: params.expiry_date_of_bank_account,
      company_id: params.company_id
    })
  },

  /**
   * 
   * @param {{
   * id: Number
   * bank_name: String
   * bank_account_holder: String
   * account_of_bank_number: String
   * bank_branch: String
   * expiry_date_of_bank_account: String
   * }} params 
   * @returns 
   */
  update: async (params) => {
    const res = await BankAccount.update({
      bank_name: params.bank_name,
      bank_account_holder: params.bank_account_holder,
      account_of_bank_number: params.account_of_bank_number,
      bank_branch: params.bank_branch,
      expiry_date_of_bank_account: params.expiry_date_of_bank_account
    }, {
      where: { id: params.id },
      returning: true,
      raw: true
    })

    if (res[1][0] === undefined)
      return null
    return res[1][0]
  },

  /**
   * 
   * @param {Number} id
   * @returns 
   */
  delete: async (id) => {
    return BankAccount.destroy({
      where: { id: id },
      returning: true,
      raw: true
    })
  },

  /**
   * 
   * @param {Number} id 
   * @param {Number} companyId
   * @returns 
   */
  findOneById: async (id, companyId) => {
    return BankAccount.findOne({
      where: { id: id, company_id: companyId }
    })
  },

  /**
   * @param {Number} companyId
   * @returns 
   */
  findAll: async (companyId) => {
    return BankAccount.findAll({
      where: { company_id: companyId }
    })
  },

  /**
   * find all bank account by search term
   * @param {string} term 
   * @param {Number} companyId
   * @param {number} page 
   * @param {number} limit 
   */
  findAllBySearchTermAndPaginated: async (term, companyId, page, limit) => {
    const query = {
      where: { company_id: companyId },
      raw: true,
      page,
      limit,
      order: [['id', 'DESC']],
    }

    if (term) {
      const searchLike = sequelizeOrLike(term, [
        'bank_accounts.bank_name',
        'bank_accounts.bank_account_holder',
        'bank_accounts.account_of_bank_number',
        'bank_accounts.bank_branch',
        'bank_accounts.expiry_date_of_bank_account'
      ]);

      query.where[Op.and] = { [Op.or]: searchLike }
    }

    return sequelizePagination(BankAccount, query);
  },
}

export default repo;