'use strict';
import { Op } from 'sequelize';

import { sequelizeOrLike, sequelizePagination } from '../helpers/utils';
import Model from '../models';
const AllowanceType = Model.sequelize.models.allowance_types;

const repo = {
  /**
   * 
   * @param {{
   * title: String
   * nominal: String
   * formula: String
   * active: Boolean
   * company_id: Number
   * }} params 
   * @returns 
   */
  create: async (params) => {
    return AllowanceType.create({
      title: params.title,
      nominal: params.nominal,
      formula: params.formula,
      active: params.active,
      company_id: params.company_id
    })
  },

  /**
   * 
   * @param {{
   * id: Number
   * title: String
   * nominal: String
   * formula: String
   * active: Boolean
   * }} params 
   * @returns 
   */
  update: async (params) => {
    const res = await AllowanceType.update({
      title: params.title,
      nominal: params.nominal,
      formula: params.formula,
      active: params.active,
    }, {
      where: { id: params.id },
      returning: true,
      raw: true
    })

    if (res[1][0] === undefined)
      return null
    return res[1][0]
  },

  /**
   * 
   * @param {Number} id
   * @returns 
   */
  delete: async (id) => {
    return AllowanceType.destroy({
      where: { id: id },
      returning: true,
      raw: true
    })
  },

  /**
   * 
   * @param {Number} id 
   * @param {Number} companyId
   * @returns 
   */
  findOneById: async (id, companyId) => {
    return AllowanceType.findOne({
      where: { id: id, company_id: companyId }
    })
  },

  /**
   * @param {Number} companyId
   * @returns 
   */
  findAll: async (companyId) => {
    return AllowanceType.findAll({
      where: { company_id: companyId }
    })
  },

  /**
   * find all allowance type by search term
   * @param {string} term 
   * @param {Number} companyId
   * @param {number} page 
   * @param {number} limit 
   */
  findAllBySearchTermAndPaginated: async (term, companyId, page, limit) => {
    const query = {
      where: { company_id: companyId },
      raw: true,
      page,
      limit,
      order: [['id', 'DESC']],
    }

    if (term) {
      const searchLike = sequelizeOrLike(term, [
        'allowance_types.title',
        'allowance_types.nominal',
        'allowance_types.formula',
        'allowance_types.active'
      ]);

      query.where[Op.and] = { [Op.or]: searchLike }
    }

    return sequelizePagination(AllowanceType, query);
  },
}

export default repo;