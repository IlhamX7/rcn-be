'use strict';
import { Op } from 'sequelize';

import { sequelizeOrLike, sequelizePagination } from '../helpers/utils';
import Model from '../models';
const TimeOff = Model.sequelize.models.time_offs;

const repo = {
  /**
   * @var {string} TIME_OFF_STATUS_PROCESS
   */
  TIME_OFF_STATUS_PROCESS: 'Process',
  /**
   * @var {string} TIME_OFF_STATUS_APPROVAL
   */
  TIME_OFF_STATUS_APPROVAL: 'Approval',
  /**
   * @var {string} TIME_OFF_STATUS_PENDING
   */
  TIME_OFF_STATUS_PENDING: 'Pending',
  /**
   * @var {string} TIME_OFF_STATUS_REJECT
   */
  TIME_OFF_STATUS_REJECT: 'Reject',
  /**
   * 
   * /**
   * @param {{
   * user_id: number
   * time_off_type_id: number
   * start_date: date
   * end_date: date
   * notes: String
   * canceled: boolean
   * attachment: String
   * replacement: number
   * company_id: number
   * }} params 
   * @returns 
   */
  create: async (params) => {
    return TimeOff.create({
      user_id: params.user_id,
      time_off_type_id: params.time_off_type_id,
      start_date: params.start_date,
      end_date: params.end_date,
      notes: params.notes,
      time_off_status: repo.TIME_OFF_STATUS_PROCESS,
      canceled: params.canceled || false,
      attachment: params.attachment,
      replacement: params.replacement || null,
      approval_replacement: null,
      approval_by: null,
      approval_date: null,
      company_id: params.company_id
    })
  },

  /**
   * 
   * @param {{
   * time_off_type_id: number
   * start_date: date
   * end_date: date
   * notes: String
   * canceled: boolean
   * attachment: String
   * replacement: number
   * }} params 
   * @returns 
   */
  update: async (params) => {
    const res = await TimeOff.update({
      time_off_type_id: params.time_off_type_id,
      start_date: params.start_date,
      end_date: params.end_date,
      notes: params.notes,
      canceled: params.canceled || false,
      attachment: params.attachment
    }, {
      where: { id: params.id },
      returning: true,
      raw: true
    })

    if (res[1][0] === undefined)
      return null
    return res[1][0]
  },

  /**
   * 
   * @param {Number} id
   * @returns 
   */
  delete: async (id) => {
    return TimeOff.destroy({ where: { id: id }})
  },

  /**
   * 
   * @param {Number} id 
   * @param {Number} companyId
   * @returns 
   */
  findOneById: async (id, companyId) => {
    return TimeOff.findOne({
      where: { id: id, company_id: companyId }
    })
  },

  /**
   * @param {Number} companyId
   * @returns 
   */
  findAll: async (companyId) => {
    return TimeOff.findAll({
      where: { company_id: companyId }
    })
  },

  /**
   * 
   * @param {string} term 
   * @param {Number} companyId
   * @param {number} page 
   * @param {number} limit 
   */
  findAllBySearchTermAndPaginated: async (term, companyId, page, limit) => {
    const query = {
      where: { company_id: companyId },
      raw: true,
      page,
      limit,
      order: [['id', 'DESC']],
    }

    if (term) {
      const searchLike = sequelizeOrLike(term, [
        'time_offs.user_id',
        'time_offs.time_off_type_id',
        'time_offs.start_date',
        'time_offs.end_date',
        'time_offs.notes',
        'time_offs.time_off_status',
        'time_offs.canceled',
        'time_offs.replacement'
      ]);

      query.where[Op.and] = { [Op.or]: searchLike }
    }

    return sequelizePagination(TimeOff, query);
  },
}

export default repo;