'use strict';
import { Op } from 'sequelize';

import { sequelizeOrLike, sequelizePagination } from '../helpers/utils';
import Model from '../models';
const EmailTemplate = Model.sequelize.models.email_templates;

const repo = {
  /**
   * 
   * @param {{
   * template_for: String
   * template_name_for: String
   * subject: string
   * body: String
   * active: boolean
   * company_id: Number
   * }} params 
   * @returns 
   */
  create: async (params) => {
    return EmailTemplate.create({
      template_for: params.template_for,
      template_name_for: params.template_name_for,
      subject: params.subject,
      body: params.body,
      active: params.active || true,
      company_id: params.company_id
    })
  },

  /**
   *
   * @param {{
   * id: Number
   * template_for: String
   * template_name_for: String
   * subject: string
   * body: String
   * active: boolean
   * }} params
   * @returns
   */
  update: async (params) => {
    const res = await EmailTemplate.update({
      template_for: params.template_for,
      template_name_for: params.template_name_for,
      subject: params.subject,
      body: params.body,
      active: params.active || true
    }, {
      where: { id: params.id },
      returning: true,
      raw: true
    })

    if (res[1][0] === undefined)
      return null
    return res[1][0]
  },

  /**
   * 
   * @param {Number} id
   * @returns 
   */
  delete: async (id) => {
    return EmailTemplate.destroy({
      where: { id: id },
      returning: true,
      raw: true
    })
  },

  /**
   * 
   * @param {Number} id 
   * @param {Number} companyId
   * @returns 
   */
  findOneById: async (id, companyId) => {
    return EmailTemplate.findOne({
      where: { id: id, company_id: companyId }
    })
  },
  
  /**
   * 
   * @param {Number} templateFor
   * @param {Number} companyId
   * @returns 
   */
  findOneByTemplateForAndStatus: async (templateFor, companyId) => {
    return EmailTemplate.findOne({
      where: { template_for: templateFor, active: true, company_id: companyId }
    })
  },

  /**
   * @param {Number} companyId
   * @returns 
   */
  findAll: async (companyId) => {
    return EmailTemplate.findAll({
      where: { company_id: companyId }
    })
  },

  /**
   * find all menu by search term
   * @param {string} term 
   * @param {Number} companyId
   * @param {number} page 
   * @param {number} limit 
   */
  findAllBySearchTermAndPaginated: async (term, companyId, page, limit) => {
    const query = {
      where: { company_id: companyId },
      raw: true,
      page,
      limit,
      order: [['id', 'DESC']],
    }

    if (term) {
      const searchLike = sequelizeOrLike(term, [
        'email_templates.template_for',
        'email_templates.subject',
        'email_templates.body'
      ]);

      query.where[Op.and] = { [Op.or]: searchLike }
    }

    return sequelizePagination(EmailTemplate, query);
  },
}

export default repo;