'use strict';
import bcrypt from 'bcrypt';
import { Op } from 'sequelize';

import { sequelizeOrLike, sequelizePagination } from '../helpers/utils';
import Model from '../models';

const User = Model.sequelize.models.users;
const saltRounds = 10;

const repo = {
  /**
   * 
   * @param {string} password 
   * @returns 
   */
  hash: async (password) => {
    const salt = await bcrypt.genSalt(saltRounds);
    const hashed = await bcrypt.hash(password, salt);
    return hashed;
  },

  /**
   * 
   * @param {string} email 
   * @returns 
   */
  findByEmail: async (email) => {
    return await User.findOne({
      where: { email: email },
    })
  },

  /**
   * 
   * @param {{
   * name: string
   * email: string
   * password: string
   * mobile_number: string
   * role: number
   * date_of_birth: date
   * place_of_birth: string
   * province_id: number
   * city_id: number
   * profile_picture_url: number
   * account_status: boolean
   * employement_status: string
   * gender: string
   * marital_status: String
   * blood_type: string
   * religion: string
   * id_type: string
   * id_number: string
   * id_expiration_date: date
   * postal_code: number
   * citizen_id_address: number
   * residential_address: string
   * position_id: number
   * employee_id: string
   * join_date: date
   * contract_expiration_date: date
   * permanent: boolean
   * created_by: number
   * approval_by: number
   * non_taxable_income_id: number
   * tax_identification_number: number
   * income_tax_rate_id: number
   * leader_id: number
   * salary: number
   * company_id: number
   * }} params 
   * @returns 
   */
  create: async (params) => {
    return User.create({
      name : params.name,
      email : params.email,
      password : params.password,
      mobile_number : params.mobile_number,
      role : params.role || 'EMP',
      date_of_birth : params.date_of_birth,
      place_of_birth : params.place_of_birth,
      province_id : params.province_id,
      city_id : params.city_id,
      profile_picture_url : params.profile_picture_url,
      account_status : params.account_status,
      employement_status : params.employement_status,
      gender : params.gender,
      marital_status : params.marital_status,
      blood_type : params.blood_type,
      religion : params.religion,
      id_type : params.id_type,
      id_number : params.id_number,
      id_expiration_date : params.id_expiration_date,
      postal_code : params.postal_code,
      citizen_id_address : params.citizen_id_address,
      residential_address : params.residential_address,
      position_id : params.position_id,
      employee_id : params.employee_id,
      join_date : params.join_date,
      contract_expiration_date : params.contract_expiration_date,
      permanent : params.permanent,
      created_by : params.created_by,
      approval_by : 0,
      non_taxable_income_id : params.non_taxable_income_id,
      tax_identification_number : params.tax_identification_number,
      income_tax_rate_id : params.income_tax_rate_id,
      leader_id : params.leader_id,
      salary : params.salary,
      company_id : params.company_id
    })
  },

  /**
   * 
   * @param {{
   * id: Number
   * name: string
   * email: string
   * mobile_number: string
   * role: number
   * date_of_birth: date
   * place_of_birth: string
   * province_id: number
   * city_id: number
   * profile_picture_url: number
   * account_status: boolean
   * employement_status: string
   * gender: string
   * marital_status: String
   * blood_type: string
   * religion: string
   * id_type: string
   * id_number: string
   * id_expiration_date: date
   * postal_code: number
   * citizen_id_address: number
   * residential_address: string
   * position_id: number
   * employee_id: string
   * join_date: date
   * contract_expiration_date: date
   * permanent: boolean
   * non_taxable_income_id: number
   * tax_identification_number: number
   * income_tax_rate_id: number
   * leader_id: number
   * salary: number
   * }} params 
   * @returns 
   */
  update: async (params) => {
    const res = await User.update({
      name : params.name,
      email : params.email,
      mobile_number : params.mobile_number,
      role : params.role || 'EMP',
      date_of_birth : params.date_of_birth,
      place_of_birth : params.place_of_birth,
      province_id : params.province_id,
      city_id : params.city_id,
      profile_picture_url : params.profile_picture_url,
      account_status : params.account_status,
      employement_status : params.employement_status,
      gender : params.gender,
      marital_status : params.marital_status,
      blood_type : params.blood_type,
      religion : params.religion,
      id_type : params.id_type,
      id_number : params.id_number,
      id_expiration_date : params.id_expiration_date,
      postal_code : params.postal_code,
      citizen_id_address : params.citizen_id_address,
      residential_address : params.residential_address,
      position_id : params.position_id,
      employee_id : params.employee_id,
      join_date : params.join_date,
      contract_expiration_date : params.contract_expiration_date,
      permanent : params.permanent,
      non_taxable_income_id : params.non_taxable_income_id,
      tax_identification_number : params.tax_identification_number,
      income_tax_rate_id : params.income_tax_rate_id,
      leader_id : params.leader_id,
      salary : params.salary
    }, {
      where: { id: params.id },
      returning: true,
      raw: true
    })

    if (res[1][0] === undefined)
      return null
    return res[1][0]
  },

  /**
   * 
   * @param {number} id 
   * @param {boolean} accountStatus 
   * @param {number} companyId 
   * @param {boolean} exclude
   * @returns 
   */
  findByIdAndAccountStatusAndExcludeCondition: async (id, accountStatus, companyId, exclude=false) => {
    const toBeExcluded = !exclude ? null : ['password', 'account_status', 'created_at', 'updated_at'];
    return User.findOne({
      attributes: { exclude: toBeExcluded },
      where: { id: id, account_status: accountStatus, company_id: companyId }
    })
  },

  /**
   * 
   * @param {string} password 
   * @param {number} id 
   * @returns 
   */
  updatePassword: async (password, id) => {
    const res = await User.update({
      password: password
    }, {
      where: { id: id },
      returning: true,
      raw: true
    })

    if (res[1][0] === undefined)
      return null
    return res[1][0]
  },

  /**
   * 
   * @param {Number} id
   * @returns 
   */
  delete: async (id) => {
    return User.destroy({
      where: { id: id }
    })
  },

  /**
   * 
   * @param {Number} id 
   * @param {Number} companyId
   * @returns 
   */
  findOneById: async (id, companyId) => {
    return User.findOne({
      where: { id: id, company_id: companyId }
    })
  },

  /**
   * find all menu by search term
   * @param {string} term 
   * @param {Number} companyId
   * @param {number} page 
   * @param {number} limit 
   */
  findAllBySearchTermAndPaginated: async (term, companyId, page, limit) => {
    const query = {
      where: { company_id: companyId },
      raw: true,
      page,
      limit,
      order: [['id', 'DESC']],
    }

    if (term) {
      const searchLike = sequelizeOrLike(term, [
        'users.name',
        'users.email',
        'users.mobile_number',
        'users.place_of_birth',
        'users.employement_status',
        'users.gender',
        'users.marital_status',
        'users.blood_type',
        'users.religion',
        'users.id_type',
        'users.id_number',
        'users.postal_code',
        'users.citizen_id_address',
        'users.residential_address',
        'users.tax_identification_number'
      ]);

      query.where[Op.and] = { [Op.or]: searchLike }
    }

    return sequelizePagination(User, query);
  },
}

export default repo;