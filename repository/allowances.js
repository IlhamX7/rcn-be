'use strict';
import { Op } from 'sequelize';

import { sequelizeOrLike, sequelizePagination } from '../helpers/utils';
import Model from '../models';
const Allowance = Model.sequelize.models.allowances;

const repo = {
  /**
   * 
   * @param {{
   * user_id: Number
   * allowance_type_id: Number
   * company_id: Number
   * created_by: Number
   * }} params 
   * @returns 
   */
  create: async (params) => {
    return Allowance.create({
      user_id: params.user_id,
      allowance_type_id: params.allowance_type_id,
      company_id: params.company_id,
      created_by: params.created_by
    })
  },

  /**
   * 
   * @param {{
   * id: Number
   * title: String
   * link: String
   * parent_id: Number
   * icon: String
   * menu_for: String
   * }} params 
   * @returns 
   */
  update: async (params) => {
    const res = await Allowance.update({
      user_id: params.user_id,
      allowance_type_id: params.allowance_type_id
    }, {
      where: { id: params.id },
      returning: true,
      raw: true
    })

    if (res[1][0] === undefined)
      return null
    return res[1][0]
  },

  /**
   * 
   * @param {Number} id
   * @returns 
   */
  delete: async (id) => {
    return Allowance.destroy({
      where: { id: id },
      returning: true,
      raw: true
    })
  },

  /**
   * 
   * @param {Number} id 
   * @param {Number} companyId
   * @returns 
   */
  findOneById: async (id, companyId) => {
    return Allowance.findOne({
      where: { id: id, company_id: companyId }
    })
  },

  /**
   * @param {Number} companyId
   * @returns 
   */
  findAll: async (companyId) => {
    return Allowance.findAll({
      where: { company_id: companyId }
    })
  },

  /**
   * find all allowance by search term
   * @param {string} term 
   * @param {Number} companyId
   * @param {number} page 
   * @param {number} limit 
   */
  findAllBySearchTermAndPaginated: async (term, companyId, page, limit) => {
    const query = {
      where: { company_id: companyId },
      raw: true,
      page,
      limit,
      order: [['id', 'DESC']],
    }

    if (term) {
      const searchLike = sequelizeOrLike(term, [
        'allowances.user_id',
        'allowances.allowance_type_id',
        'allowances.company_id',
        'allowances.created_by'
      ]);

      query.where[Op.and] = { [Op.or]: searchLike }
    }

    return sequelizePagination(Allowance, query);
  },
}

export default repo;