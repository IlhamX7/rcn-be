'use strict';
import { Op } from 'sequelize';

import { sequelizeOrLike, sequelizePagination } from '../helpers/utils';
import Model from '../models';
const NonTaxableIncomes = Model.sequelize.models.non_taxable_incomes;

const repo = {
  /**
   * 
   * @param {{
   * title: String
   * rate: Number
   * company_id: Number
   * }} params 
   * @returns 
   */
  create: async (params) => {
    return NonTaxableIncomes.create({
      title: params.title,
      rate: params.rate || 0,
      company_id: params.company_id
    })
  },

  /**
   * 
   * @param {{
   * id: Number
   * title: String
   * rate: Number
   * }} params 
   * @returns 
   */
  update: async (params) => {
    const res = await NonTaxableIncomes.update({
      title: params.title,
      rate: params.rate || 0
    }, {
      where: { id: params.id },
      returning: true,
      raw: true
    })

    if (res[1][0] === undefined)
      return null
    return res[1][0]
  },

  /**
   * 
   * @param {Number} id
   * @returns 
   */
  delete: async (id) => {
    return NonTaxableIncomes.destroy({
      where: { id: id }
    })
  },

  /**
   * 
   * @param {Number} id 
   * @param {Number} companyId
   * @returns 
   */
  findOneById: async (id, companyId) => {
    return NonTaxableIncomes.findOne({
      where: { id: id, company_id: companyId }
    })
  },

  /**
   * @param {Number} companyId
   * @returns 
   */
  findAll: async (companyId) => {
    return NonTaxableIncomes.findAll({
      where: { company_id: companyId }
    })
  },

  /**
   * find all menu by search term
   * @param {string} term 
   * @param {Number} companyId
   * @param {number} page 
   * @param {number} limit 
   */
  findAllBySearchTermAndPaginated: async (term, companyId, page, limit) => {
    const query = {
      where: { company_id: companyId },
      raw: true,
      page,
      limit,
      order: [['id', 'DESC']],
    }

    if (term) {
      const searchLike = sequelizeOrLike(term, [
        'non_taxable_incomes.title',
        'non_taxable_incomes.rate'
      ]);

      query.where[Op.and] = { [Op.or]: searchLike }
    }

    return sequelizePagination(NonTaxableIncomes, query);
  },
}

export default repo;