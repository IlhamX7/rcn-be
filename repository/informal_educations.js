'use strict';
import { Op } from 'sequelize';

import { sequelizeOrLike, sequelizePagination } from '../helpers/utils';
import Model from '../models';
const InformalEducation = Model.sequelize.models.informal_educations;

const repo = {
  /**
   * 
   * @param {{
   * name: String
   * held_by: String
   * start_date: String
   * end_date: String
   * fee: String
   * certificate: String
   * user_id: Number
   * company_id: Number
   * }} params 
   * @returns 
   */
  create: async (params) => {
    return InformalEducation.create({
      name: params.name,
      held_by: params.held_by,
      start_date: params.start_date,
      end_date: params.end_date,
      fee: params.fee,
      certificate: params.certificate,
      user_id: params.user_id,
      company_id: params.company_id
    })
  },

  /**
   * 
   * @param {{
   * id: Number
   * name: String
   * held_by: String
   * start_date: String
   * end_date: String
   * fee: String
   * certificate: String
   * }} params 
   * @returns 
   */
  update: async (params) => {
    const res = await InformalEducation.update({
      name: params.name,
      held_by: params.held_by,
      start_date: params.start_date,
      end_date: params.end_date,
      fee: params.fee,
      certificate: params.certificate
    }, {
      where: { id: params.id },
      returning: true,
      raw: true
    })

    if (res[1][0] === undefined)
      return null
    return res[1][0]
  },

  /**
   * 
   * @param {Number} id
   * @returns 
   */
  delete: async (id) => {
    return InformalEducation.destroy({
      where: { id: id },
      returning: true,
      raw: true
    })
  },

  /**
   * 
   * @param {Number} id 
   * @param {Number} companyId
   * @returns 
   */
  findOneById: async (id, companyId) => {
    return InformalEducation.findOne({
      where: { id: id, company_id: companyId }
    })
  },

  /**
   * @param {Number} companyId
   * @returns 
   */
  findAll: async (companyId) => {
    return InformalEducation.findAll({
      where: { company_id: companyId }
    })
  },

  /**
   * find all informal education by search term
   * @param {string} term 
   * @param {Number} companyId
   * @param {number} page 
   * @param {number} limit 
   */
  findAllBySearchTermAndPaginated: async (term, companyId, page, limit) => {
    const query = {
      where: { company_id: companyId },
      raw: true,
      page,
      limit,
      order: [['id', 'DESC']],
    }

    if (term) {
      const searchLike = sequelizeOrLike(term, [
        'informal_educations.name',
        'informal_educations.held_by',
        'informal_educations.start_date',
        'informal_educations.end_date',
        'informal_educations.fee',
        'informal_educations.certificate'
      ]);

      query.where[Op.and] = { [Op.or]: searchLike }
    }

    return sequelizePagination(InformalEducation, query);
  },
}

export default repo;