'use strict';
import { Op } from 'sequelize';

import { sequelizeOrLike, sequelizePagination } from '../helpers/utils';
import Model from '../models';
const City = Model.sequelize.models.cities;

const repo = {
  /**
   * 
   * @param {Number} id 
   * @returns 
   */
  findOneById: async (id) => {
    return City.findOne({
      where: { id: id }
    })
  },

  /**
   * @returns 
   */
  findAll: async () => {
    return City.findAll()
  },

  /**
   * find all city by search term
   * @param {string} term 
   * @param {number} page 
   * @param {number} limit 
   */
  findAllBySearchTermAndPaginated: async (term, page, limit) => {
    const query = {
      where: {},
      raw: true,
      page,
      limit,
      order: [['id', 'DESC']],
    }

    if (term) {
      const searchLike = sequelizeOrLike(term, [
        'cities.city_name'
      ]);

      query.where[Op.and] = { [Op.or]: searchLike }
    }

    return sequelizePagination(City, query);
  },
}

export default repo;