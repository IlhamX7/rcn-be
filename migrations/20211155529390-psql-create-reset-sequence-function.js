'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`
      CREATE OR REPLACE FUNCTION "public"."reset_sequence"("tablename" text, "columnname" text, "sequence_name" text)
      RETURNS "pg_catalog"."void" AS $BODY$
        DECLARE
        BEGIN
        EXECUTE 'SELECT setval( ''' || sequence_name  || ''', ' || '(SELECT MAX(' || columnname ||') FROM ' || tablename || ')' || '+1)';
        END;
      $BODY$
      LANGUAGE plpgsql VOLATILE
      COST 100
    `)
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('DROP function if exists reset_sequence;');
  }
};
