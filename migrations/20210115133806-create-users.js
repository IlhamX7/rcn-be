'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING
      },
      password: {
        type: Sequelize.STRING
      },
      mobile_number: {
        type: Sequelize.STRING
      },
      role: {
        type: Sequelize.STRING,
        allowNull: false
      },
      date_of_birth: {
        type: Sequelize.DATE
      },
      place_of_birth: {
        type: Sequelize.STRING
      },
      province_id: {
        type: Sequelize.INTEGER
      },
      city_id: {
        type: Sequelize.INTEGER
      },
      profile_picture_url: {
        type: Sequelize.STRING
      },
      account_status: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      employement_status: {
        allowNull: false,
        type: Sequelize.ENUM(['Contract', 'Fulltime', 'Frelance', 'Internship', 'Permanent'])
      },
      gender: {
        allowNull: false,
        type: Sequelize.ENUM(['Male', 'Female'])
      },
      marital_status: {
        allowNull: false,
        type: Sequelize.ENUM(['single', 'married', 'widow', 'widower'])
      },
      blood_type: {
        type: Sequelize.ENUM(['A', 'B', 'AB','O'])
      },
      religion: {
        allowNull: false,
        type: Sequelize.ENUM(['Islam', 'Catholic', 'Christian', 'Buddha', 'Hindu', 'Confucius', 'Others'])
      },
      id_type: {
        allowNull: false,
        type: Sequelize.ENUM(['KTP', 'Passport','SIM'])
      },
      id_number: {
        allowNull: false,
        type: Sequelize.STRING
      },
      id_expiration_date: {
        allowNull: false,
        type: Sequelize.DATEONLY,
        defaultValue: '1970-01-01'
      },
      postal_code: {
        type: Sequelize.BIGINT
      },
      citizen_id_address: {
        type: Sequelize.TEXT
      },
      residential_address: {
        type: Sequelize.TEXT
      },
      position_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: 'positions',
          key: 'id'
        }
      },
      employee_id: {
        type: Sequelize.STRING
      },
      join_date: {
        type: Sequelize.DATEONLY
      },
      contract_expiration_date: {
        type: Sequelize.DATEONLY
      },
      permanent: {
        type: Sequelize.BOOLEAN
      },
      created_by: {
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: 'users',
          key: 'id'
        }
      },
      approval_by: {
        defaultValue: () => null,
        type: Sequelize.INTEGER
      },
      non_taxable_income_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: 'non_taxable_incomes',
          key: 'id'
        }
      },
      tax_identification_number: {
        type: Sequelize.STRING
      },
      income_tax_rate_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: 'income_tax_rates',
          key: 'id'
        }
      },
      leader_id: {
        type: Sequelize.INTEGER,
        defaultValue: () => 0
      },
      salary: {
        type: Sequelize.STRING
      },
      company_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: 'companies',
          key: 'id'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('users');
  }
};
