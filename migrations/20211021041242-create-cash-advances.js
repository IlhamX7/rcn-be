'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('cash_advances', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: 'users',
          key: 'id'
        }
      },
      request_id: {
        type: Sequelize.INTEGER
      },
      request_date: {
        type: Sequelize.DATEONLY
      },
      policy: {
        type: Sequelize.STRING
      },
      porpose: {
        type: Sequelize.STRING
      },
      amount: {
        type: Sequelize.DECIMAL
      },
      status: {
        type: Sequelize.INTEGER
      },
      approval_of_leader: {
        type: Sequelize.INTEGER
      },
      approval_date_of_leader: {
        type: Sequelize.DATEONLY
      },
      approval_of_hrd: {
        type: Sequelize.INTEGER
      },
      approval_date_of_hrd: {
        type: Sequelize.DATEONLY
      },
      approval_of_finance: {
        type: Sequelize.INTEGER
      },
      approval_date_of_finance: {
        type: Sequelize.DATE
      },
      company_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: 'companies',
          key: 'id'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('cash_advances');
  }
};