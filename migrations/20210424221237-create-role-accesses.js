'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('role_accesses', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_role: {
        allowNull: false,
        type: Sequelize.STRING,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: 'group_users',
          key: 'code'
        }
      },
      menu_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: 'menus',
          key: 'id'
        }
      },
      active: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      is_insert: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      is_edit: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      is_view: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      is_delete: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      ordering: {
        allowNull: false,
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      company_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: 'companies',
          key: 'id'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('role_accesses');
  }
};