'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('reimbursements', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        type: Sequelize.INTEGER
      },
      transaction_id: {
        type: Sequelize.INTEGER,
        comment: 'autogenerate combination date n serial number'
      },
      reimbursement_name: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.TEXT
      },
      effective_date: {
        type: Sequelize.DATEONLY
      },
      costs: {
        type: Sequelize.DECIMAL
      },
      reimbursement_status: {
        type: Sequelize.INTEGER,
        comment: 'approved, pending, reject'
      },
      approval_of_leader: {
        type: Sequelize.INTEGER
      },
      approval_date_of_leader: {
        type: Sequelize.DATEONLY
      },
      approval_of_hrd: {
        type: Sequelize.INTEGER
      },
      approval_date_of_hrd: {
        type: Sequelize.DATEONLY
      },
      approval_of_finance: {
        type: Sequelize.INTEGER
      },
      approval_date_of_finance: {
        type: Sequelize.DATE
      },
      attachments: {
        type: Sequelize.STRING
      },
      company_id: {
        type: Sequelize.INTEGER
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('reimbursements');
  }
};