'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('insurances', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: 'users',
          key: 'id'
        }
      },
      insurance_type_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: 'insurance_types',
          key: 'id'
        }
      },
      insurance_id: {
        type: Sequelize.STRING
      },
      premi: {
        type: Sequelize.DECIMAL
      },
      polis_url: {
        type: Sequelize.STRING
      },
      insured: {
        type: Sequelize.STRING
      },
      premium_holiday: {
        type: Sequelize.BOOLEAN
      },
      rider: {
        type: Sequelize.DECIMAL
      },
      cash_value: {
        type: Sequelize.DECIMAL
      },
      lapse: {
        type: Sequelize.BOOLEAN
      },
      grace_period: {
        type: Sequelize.DATEONLY
      },
      acquisition_cost: {
        type: Sequelize.DECIMAL
      },
      claim: {
        type: Sequelize.DECIMAL
      },
      company_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: 'companies',
          key: 'id'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('insurances');
  }
};