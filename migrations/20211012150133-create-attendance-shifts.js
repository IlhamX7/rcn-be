'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('attendance_shifts', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        type: Sequelize.STRING
      },
      schedule_clock_in: {
        type: Sequelize.TIME
      },
      schedule_clock_out: {
        type: Sequelize.TIME
      },
      schedule_break_in: {
        type: Sequelize.TIME
      },
      schedule_break_out: {
        type: Sequelize.TIME
      },
      late_tolerance: {
        type: Sequelize.BIGINT
      },
      company_id: {
        type: Sequelize.INTEGER
      },
      active: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('attendance_shifts');
  }
};