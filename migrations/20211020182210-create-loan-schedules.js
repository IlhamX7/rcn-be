'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('loan_schedules', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      load_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: 'loans',
          key: 'id'
        }
      },
      pay_date: {
        type: Sequelize.DATEONLY
      },
      outstanding: {
        type: Sequelize.DECIMAL
      },
      mounthly_payment: {
        type: Sequelize.DECIMAL
      },
      interest: {
        type: Sequelize.DECIMAL
      },
      principal: {
        type: Sequelize.DECIMAL
      },
      total_principal_payment: {
        type: Sequelize.DECIMAL
      },
      monthly_payment_number: {
        type: Sequelize.INTEGER
      },
      company_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: 'companies',
          key: 'id'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('loan_schedules');
  }
};