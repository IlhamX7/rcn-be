'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('loans', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      first_payment: {
        type: Sequelize.DATEONLY
      },
      amount_of_loan: {
        type: Sequelize.DECIMAL
      },
      number_of_year: {
        type: Sequelize.INTEGER
      },
      interest_rate: {
        type: Sequelize.DOUBLE
      },
      monthly_payment_amount: {
        type: Sequelize.DECIMAL
      },
      approval_of_leader: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: 'users',
          key: 'id'
        }
      },
      approval_date_of_leader: {
        type: Sequelize.DATEONLY
      },
      approval_of_hrd: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: 'users',
          key: 'id'
        }
      },
      approval_date_of_hrd: {
        type: Sequelize.DATEONLY
      },
      approval_of_finance: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: 'users',
          key: 'id'
        }
      },
      approval_date_of_finance: {
        type: Sequelize.DATE
      },
      company_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: 'companies',
          key: 'id'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('loans');
  }
};