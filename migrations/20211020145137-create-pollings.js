'use strict';
module.exports = {
  // https://mysql.tutorials24x7.com/blog/guide-to-design-database-for-poll-in-mysql
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('pollings', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: 'users',
          key: 'id'
        }
      },
      title: {
        type: Sequelize.STRING
      },
      slug: {
        type: Sequelize.STRING
      },
      summary: {
        type: Sequelize.TEXT
      },
      polling_of_type: {
        type: Sequelize.INTEGER
      },
      published: {
        type: Sequelize.BOOLEAN
      },
      starts_at: {
        type: Sequelize.DATE
      },
      ends_at: {
        type: Sequelize.DATE
      },
      content: {
        type: Sequelize.TEXT
      },
      company_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: 'companies',
          key: 'id'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('pollings');
  }
};