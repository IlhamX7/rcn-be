'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('non_taxable_incomes', null, {}).then(() => {
      return queryInterface.bulkInsert('non_taxable_incomes', [
        {
          "id": 1,
          "title": "PTKP TK/0: tidak kawin dan tidak ada tanggungan.",
          "rate": 54000000,
          "company_id": 1,
          "created_at": new Date(),
          "updated_at": new Date()
        },
        {
          "id": 2,
          "title": "PTKP TK/1: tidak kawin dan 1 tanggungan.",
          "rate": 58500000,
          "company_id": 1,
          "created_at": new Date(),
          "updated_at": new Date()
        },
        {
          "id": 3,
          "title": "PTKP TK/2: tidak kawin dan 2 tanggungan.",
          "rate": 63000000,
          "company_id": 1,
          "created_at": new Date(),
          "updated_at": new Date()
        },
        {
          "id": 4,
          "title": "PTKP TK/3: tidak kawin dan 3 tanggungan.",
          "rate": 67500000,
          "company_id": 1,
          "created_at": new Date(),
          "updated_at": new Date()
        },
        {
          "id": 5,
          "title": "PTKP K/0: kawin dan tidak ada tanggungan.",
          "rate": 58500000,
          "company_id": 1,
          "created_at": new Date(),
          "updated_at": new Date()
        },
        {
          "id": 6,
          "title": "PTKP K/1: kawin dan 1 tanggungan.",
          "rate": 63000000,
          "company_id": 1,
          "created_at": new Date(),
          "updated_at": new Date()
        },
        {
          "id": 7,
          "title": "PTKP K/2: kawin dan 2 tanggungan.",
          "rate": 67500000,
          "company_id": 1,
          "created_at": new Date(),
          "updated_at": new Date()
        },
        {
          "id": 8,
          "title": "PTKP K/3: kawin dan 3 tanggungan.",
          "rate": 72000000,
          "company_id": 1,
          "created_at": new Date(),
          "updated_at": new Date()
        },
        {
          "id": 9,
          "title": "PTKP K/I/0: penghasilan suami dan istri digabung dan tidak ada tanggungan.",
          "rate": 112500000,
          "company_id": 1,
          "created_at": new Date(),
          "updated_at": new Date()
        },
        {
          "id": 10,
          "title": "PTKP K/I/1: penghasilan suami dan istri digabung dan 1 tanggungan.",
          "rate": 117000000,
          "company_id": 1,
          "created_at": new Date(),
          "updated_at": new Date()
        },
        {
          "id": 11,
          "title": "PTKP K/I/2: penghasilan suami dan istri digabung dan 2 tanggungan.",
          "rate": 121500000,
          "company_id": 1,
          "created_at": new Date(),
          "updated_at": new Date()
        },
        {
          "id": 12,
          "title": "PTKP K/I/3: penghasilan suami dan istri digabung dan 3 tanggungan.",
          "rate": 126000000,
          "company_id": 1,
          "created_at": new Date(),
          "updated_at": new Date()
        },
      ])
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('non_taxable_incomes', null, {});
  }
};
