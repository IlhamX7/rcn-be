'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`
      SELECT table_name || '_' || column_name || '_seq', 
      reset_sequence(table_name, column_name, table_name || '_' || column_name || '_seq') 
      FROM information_schema.columns where column_default like 'nextval%';
    `)
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`
      SELECT SETVAL(c.oid, 1)
      FROM pg_class c JOIN pg_namespace n ON n.oid = c.relnamespace
      WHERE c.relkind = 'S' and n.nspname = 'public'
    `)
  }
};
