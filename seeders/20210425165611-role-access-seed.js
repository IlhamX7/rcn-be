'use strict';
const ACTIVE = true;

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const menuData = await queryInterface.sequelize.query('SELECT * FROM menus', { type: queryInterface.sequelize.QueryTypes.SELECT, raw: true })
    const newData = [];
    for (const item of menuData) {
      newData.push({
        user_role: 'SA',
        menu_id: item.id,
        active: ACTIVE,
        is_insert: ACTIVE,
        is_edit: ACTIVE,
        is_view: ACTIVE,
        is_delete: ACTIVE,
        company_id: 1,
        created_at: new Date(),
        updated_at: new Date()
      })
    }

    return queryInterface.bulkDelete('role_accesses', null, {}).then(() => {
      return queryInterface.bulkInsert('role_accesses', newData);
    })
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('role_accesses', null, {});
  }
};