'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('positions', null, {}).then(() => {
      return queryInterface.bulkInsert('positions', [
        {
          "id": "1",
          "job_position": "Software Developer Advisor",
          "job_level": "Middle-level management",
          "departement_id": 1,
          "company_id": 1,
          "created_at": new Date(),
          "updated_at": new Date()
        }
      ])
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('positions', null, {});
  }
};
