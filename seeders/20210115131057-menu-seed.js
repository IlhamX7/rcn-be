'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('menus', null, {}).then(() => {
      return queryInterface.bulkInsert('menus', [
        {
          "id": 1,
          "title": "Users",
          "link": "users",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-09-27T07:09:51.181Z",
          "updated_at": "2021-09-27T07:09:51.181Z"
        },
        {
          "id": 2,
          "title": "Email Templates",
          "link": "email-templates",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-09-27T07:09:59.318Z",
          "updated_at": "2021-09-27T07:09:59.318Z"
        },
        {
          "id": 3,
          "title": "Email Templates All",
          "link": "email-templates-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-09-27T07:09:59.318Z",
          "updated_at": "2021-09-27T07:09:59.318Z"
        },
        {
          "id": 4,
          "title": "Group Users",
          "link": "group-users",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-09-27T07:18:49.306Z",
          "updated_at": "2021-09-27T07:18:49.306Z"
        },
        {
          "id": 5,
          "title": "Group Users All",
          "link": "group-users-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-09-27T07:18:49.306Z",
          "updated_at": "2021-09-27T07:18:49.306Z"
        },
        {
          "id": 6,
          "title": "Role Accesses",
          "link": "role-accesses",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-09-27T07:19:07.889Z",
          "updated_at": "2021-09-27T07:19:07.889Z"
        },
        {
          "id": 7,
          "title": "Menu",
          "link": "menus",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-09-27T14:19:53.000Z",
          "updated_at": "2021-09-27T14:19:55.000Z"
        },
        {
          "id": 8,
          "title": "Menu All",
          "link": "menus-all",
          "parent_id": 0,
          "icon": "mdi-playlist-plus",
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-09-27T07:15:45.379Z",
          "updated_at": "2021-09-27T07:15:45.379Z"
        },
        {
          "id": 9,
          "title": "Role Accesses Group",
          "link": "role-accesses-group",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-02T19:52:16.653Z",
          "updated_at": "2021-11-02T19:52:16.653Z"
        },
        {
          "id": 10,
          "title": "Role Accesses Order Down",
          "link": "role-accesses-order-down",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-02T20:48:05.354Z",
          "updated_at": "2021-11-02T20:48:05.354Z"
        },
        {
          "id": 11,
          "title": "Role Accesses Order Up",
          "link": "role-accesses-order-up",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-02T21:44:03.139Z",
          "updated_at": "2021-11-02T21:44:03.139Z"
        },
        {
          "id": 12,
          "title": "Attendances",
          "link": "attendances",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 13,
          "title": "Attendances All",
          "link": "attendances-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 14,
          "title": "Attendance Shifts",
          "link": "attendance-shifts",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 16,
          "title": "Attendance Shifts All",
          "link": "attendance-shifts-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 17,
          "title": "Time Off Types",
          "link": "time-off-types",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 18,
          "title": "Time Off Types All",
          "link": "time-off-types-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 19,
          "title": "Time Offs",
          "link": "time-offs",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 20,
          "title": "Time Offs All",
          "link": "time-offs-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 21,
          "title": "Allowance Types",
          "link": "allowance-types",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 22,
          "title": "Allowance Types All",
          "link": "allowance-types-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 23,
          "title": "Allowances",
          "link": "allowances",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 24,
          "title": "Allowances All",
          "link": "allowances-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 25,
          "title": "Announcements",
          "link": "announcements",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 26,
          "title": "Announcements All",
          "link": "announcements-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 27,
          "title": "Asset Categories",
          "link": "asset-categories",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 28,
          "title": "Asset Categories All",
          "link": "asset-categories-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 29,
          "title": "Assets",
          "link": "assets",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 30,
          "title": "Assets All",
          "link": "assets-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 31,
          "title": "Bank Accounts",
          "link": "bank-accounts",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 32,
          "title": "Bank Accounts All",
          "link": "bank-accounts-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 33,
          "title": "Branchs",
          "link": "branchs",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 34,
          "title": "Branchs All",
          "link": "branchs-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 35,
          "title": "Cash Advances",
          "link": "cash-advances",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 36,
          "title": "Cash Advances All",
          "link": "cash-advances-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 37,
          "title": "Companies",
          "link": "companies",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 38,
          "title": "Companies All",
          "link": "companies-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 39,
          "title": "Departements",
          "link": "departements",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 40,
          "title": "Departements All",
          "link": "departements-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 41,
          "title": "Files",
          "link": "files",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 42,
          "title": "Files All",
          "link": "files-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 43,
          "title": "Formal Educations",
          "link": "formal-educations",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 44,
          "title": "Formal Educations ALl",
          "link": "formal-educations-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 45,
          "title": "Income Tax Rates",
          "link": "income-tax-rates",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 46,
          "title": "Income Tax Rates All",
          "link": "income-tax-rates-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 47,
          "title": "Informal Educations",
          "link": "informal-educations",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 48,
          "title": "Informal Educations All",
          "link": "informal-educations-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 49,
          "title": "Insurance Types",
          "link": "insurance-types",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 50,
          "title": "Insurance Types All",
          "link": "insurance-types-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 51,
          "title": "Insurances",
          "link": "insurances",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 52,
          "title": "Insurances All",
          "link": "insurances-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 53,
          "title": "Loans",
          "link": "loans",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 54,
          "title": "Loans All",
          "link": "loans-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 55,
          "title": "Loan Schedules",
          "link": "loan-schedules",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 56,
          "title": "Loan Schedules All",
          "link": "loan-schedules-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 57,
          "title": "Non Taxable Incomes",
          "link": "non-taxable-incomes",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 58,
          "title": "Non Taxable Incomes All",
          "link": "non-taxable-incomes-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 59,
          "title": "Onboarding Types",
          "link": "onboarding-types",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 60,
          "title": "Onboarding Types All",
          "link": "onboarding-types-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 61,
          "title": "Onboardings",
          "link": "onboardings",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 62,
          "title": "Onboardings All",
          "link": "onboardings-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 63,
          "title": "Overtimes",
          "link": "overtimes",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 64,
          "title": "Overtimes All",
          "link": "overtimes-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 65,
          "title": "Pollings",
          "link": "pollings",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 66,
          "title": "Pollings All",
          "link": "pollings-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 67,
          "title": "Positions",
          "link": "positions",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 68,
          "title": "Positions All",
          "link": "positions-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 69,
          "title": "Reimbursements",
          "link": "reimbursements",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 70,
          "title": "Reimbursements All",
          "link": "reimbursements-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 71,
          "title": "Reprimand Of Types",
          "link": "reprimand-of-types",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 72,
          "title": "Reprimand Of Types All",
          "link": "reprimand-of-types-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 73,
          "title": "Reprimands",
          "link": "reprimands",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 74,
          "title": "Settings",
          "link": "settings",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 75,
          "title": "Settings All",
          "link": "settings-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 76,
          "title": "Tasks",
          "link": "tasks",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 77,
          "title": "Tasks All",
          "link": "tasks-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 78,
          "title": "Type Of Leaves",
          "link": "type-of-leaves",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 79,
          "title": "Type Of Leaves All",
          "link": "type-of-leaves-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 80,
          "title": "Working Experiences",
          "link": "working-experiences",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 81,
          "title": "Working Experiences All",
          "link": "working-experiences-all",
          "parent_id": 0,
          "icon": null,
          "menu_for": "backend",
          "company_id": 1,
          "created_at": "2021-11-03T16:42:35.041Z",
          "updated_at": "2021-11-03T16:42:35.041Z"
        },
        {
          "id": 83,
          "title": "Master",
          "link": "",
          "parent_id": 0,
          "icon": "apartment",
          "menu_for": "frontend",
          "company_id": 1,
          "created_at": "2021-11-04T18:52:34.425Z",
          "updated_at": "2021-11-04T18:52:34.425Z"
        },
        {
          "id": 84,
          "title": "Attendance",
          "link": "/attendances",
          "parent_id": 83,
          "icon": "apartment",
          "menu_for": "frontend",
          "company_id": 1,
          "created_at": "2021-11-05T14:02:15.559Z",
          "updated_at": "2021-11-05T14:02:15.559Z"
        },
        {
          "id": 85,
          "title": "Time Off",
          "link": "/time-off",
          "parent_id": 84,
          "icon": "apartment",
          "menu_for": "frontend",
          "company_id": 1,
          "created_at": "2021-11-05T14:02:57.602Z",
          "updated_at": "2021-11-05T14:02:57.602Z"
        },
        {
          "id": 86,
          "title": "Employee",
          "link": "/employee",
          "parent_id": 85,
          "icon": "apartment",
          "menu_for": "frontend",
          "company_id": 1,
          "created_at": "2021-11-05T14:03:14.609Z",
          "updated_at": "2021-11-05T14:03:14.609Z"
        },
        {
          "id": 87,
          "title": "Company",
          "link": "/company",
          "parent_id": 0,
          "icon": "apartment",
          "menu_for": "frontend",
          "company_id": 1,
          "created_at": "2021-11-06T07:08:21.430Z",
          "updated_at": "2021-11-06T07:08:21.430Z"
        }
      ])
    })
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('menus', null, {});
  }
};
