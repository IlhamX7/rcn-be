'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('income_tax_rates', null, {}).then(() => {
      return queryInterface.bulkInsert('income_tax_rates', [
        {
          "id": 1,
          "title": "Penghasilan tahunan Rp 0 sampai dengan Rp. 50.000.000",
          "income_minimum": 0,
          "income_maximum": 50000000,
          "percentage_of_tax_rate": 5,
          "with_tax_id_number": true,
          "company_id": 1,
          "created_at": new Date(),
          "updated_at": new Date()
        },
        {
          "id": 2,
          "title": "Penghasilan tahunan Rp 50.000.000 sampai dengan Rp. 250.000.000",
          "income_minimum": 50000000,
          "income_maximum": 250000000,
          "percentage_of_tax_rate": 15,
          "with_tax_id_number": true,
          "company_id": 1,
          "created_at": new Date(),
          "updated_at": new Date()
        },
        {
          "id": 3,
          "title": "Penghasilan tahunan Rp 250.000.000 sampai dengan Rp. 500.000.000",
          "income_minimum": 250000000,
          "income_maximum": 500000000,
          "percentage_of_tax_rate": 25,
          "with_tax_id_number": true,
          "company_id": 1,
          "created_at": new Date(),
          "updated_at": new Date()
        },
        {
          "id": 4,
          "title": "Penghasilan tahunan Rp 250.000.000 sampai dengan Rp. 500.000.000",
          "income_minimum": 250000000,
          "income_maximum": 500000000,
          "percentage_of_tax_rate": 25,
          "with_tax_id_number": true,
          "company_id": 1,
          "created_at": new Date(),
          "updated_at": new Date()
        },
        {
          "id": 5,
          "title": "Penghasilan tahunan diatas 500.000.000",
          "income_minimum": 500000000,
          "income_maximum": -1,
          "percentage_of_tax_rate": 30,
          "with_tax_id_number": true,
          "company_id": 1,
          "created_at": new Date(),
          "updated_at": new Date()
        },
      ])
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('income_tax_rates', null, {});
  }
};
