'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('branchs', null, {}).then(() => {
      return queryInterface.bulkInsert('branchs', [
        {
          "id": "1",
          "title": "Pusat",
          "description": "Kantor Pusat",
          "created_by": 1,
          "updated_by": 1,
          "created_at": new Date(),
          "updated_at": new Date()
        }
      ])
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('branchs', null, {});
  }
};
