'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const Op = Sequelize.Op
    return queryInterface.bulkDelete('group_users', { id: { [Op.in]: [1, 2] } }, {}).then(() => {
        return queryInterface.bulkInsert('group_users', [{
          id: 1,
          code: 'SA',
          title: 'Super Admin',
          company_id: 1,
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 2,
          code: 'HRD',
          title: 'Human Resource Department',
          company_id: 1,
          created_at: new Date(),
          updated_at: new Date()
        }])
      })
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('group_users', null, {});
  }
};
