'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const Op = Sequelize.Op
    return queryInterface.bulkDelete('users', { email: { [Op.in]: ['if.hamzah93@gmail.com'] } }, {}).then(() => {
      return queryInterface.bulkInsert('users', [{
        id: 1,
        name: 'Super Admin',
        email: 'if.hamzah93@gmail.com',
        password: '$2b$10$3Vp4pdRc8F.z6qxA6lUTAekvxkhbZPytpkeL44HOkPrdI44LUwvzm',
        mobile_number: '+6281222538641',
        role: 'SA',
        date_of_birth: '1993-06-19',
        place_of_birth: 'Payuputat',
        province_id: 1,
        city_id: 1,
        profile_picture_url: '',
        account_status: true,
        employement_status: 'Contract',
        gender: 'Male',
        marital_status: 'single',
        blood_type: 'B',
        religion: 'Islam',
        id_type: 'KTP',
        id_number: '1674011906930006',
        position_id: 1,
        created_by: 1,
        approval_by: 1,
        non_taxable_income_id: 1,
        income_tax_rate_id: 1,
        leader_id: 1,
        company_id: 1,
        created_at: new Date(),
        updated_at: new Date()
      }])
    })
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('users', null, {});
  }
};
