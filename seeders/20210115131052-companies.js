'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('companies', null, {}).then(() => {
      return queryInterface.bulkInsert('companies', [
        {
          "id": "1",
          "company_name": "PT. Reeracoen Indonesia",
          "organization_name": "Portal Recruiting Service and HR Tech",
          "branch_id": "1",
          "company_logo_url": "",
          "address": "Wisma KEIAI, 17th Floor., Jalan Jendral Sudirman Kav. 3-4, Karet Tengsin, Tanah Abang, RT.10/RW.11, Karet Tengsin, Kecamatan Tanah Abang, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10220",
          "telp": "(021) 5723322",
          "email": "",
          "founded_date": "2013-10-08",
          "website_address": "Kenichi Fujiki",
          "created_by": 1,
          "updated_by": 1,
          "created_at": new Date(),
          "updated_at": new Date()
        }
      ])
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('companies', null, {});
  }
};
