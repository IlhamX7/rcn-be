'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('departements', null, {}).then(() => {
      return queryInterface.bulkInsert('departements', [
        {
          "id": "1",
          "title": "Teknologi Informasi (IT)",
          "active": true,
          "company_id": 1,
          "created_at": new Date(),
          "updated_at": new Date()
        }
      ])
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('departements', null, {});
  }
};
