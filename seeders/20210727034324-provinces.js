'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('provinces', null, {}).then(() => {
      return queryInterface.bulkInsert('provinces', [
        {
          id: 11,
          province_name: 'ACEH',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 12,
          province_name: 'SUMATERA UTARA',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 13,
          province_name: 'SUMATERA BARAT',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 14,
          province_name: 'RIAU',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 15,
          province_name: 'JAMBI',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 16,
          province_name: 'SUMATERA SELATAN',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 17,
          province_name: 'BENGKULU',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 18,
          province_name: 'LAMPUNG',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 19,
          province_name: 'KEPULAUAN BANGKA BELITUNG',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 21,
          province_name: 'KEPULAUAN RIAU',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 31,
          province_name: 'DKI JAKARTA',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 32,
          province_name: 'JAWA BARAT',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 33,
          province_name: 'JAWA TENGAH',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 34,
          province_name: 'DI YOGYAKARTA',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 35,
          province_name: 'JAWA TIMUR',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 36,
          province_name: 'BANTEN',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 51,
          province_name: 'BALI',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 52,
          province_name: 'NUSA TENGGARA BARAT',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 53,
          province_name: 'NUSA TENGGARA TIMUR',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 61,
          province_name: 'KALIMANTAN BARAT',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 62,
          province_name: 'KALIMANTAN TENGAH',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 63,
          province_name: 'KALIMANTAN SELATAN',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 64,
          province_name: 'KALIMANTAN TIMUR',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 65,
          province_name: 'KALIMANTAN UTARA',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 71,
          province_name: 'SULAWESI UTARA',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 72,
          province_name: 'SULAWESI TENGAH',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 73,
          province_name: 'SULAWESI SELATAN',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 74,
          province_name: 'SULAWESI TENGGARA',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 75,
          province_name: 'GORONTALO',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 76,
          province_name: 'SULAWESI BARAT',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 81,
          province_name: 'MALUKU',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 82,
          province_name: 'MALUKU UTARA',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 91,
          province_name: 'PAPUA BARAT',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: 94,
          province_name: 'PAPUA',
          created_at: new Date(),
          updated_at: new Date()
        }
      ])
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('provinces', null, {});
  }
};
