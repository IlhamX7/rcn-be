'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class companies extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  companies.init({
    company_name: DataTypes.STRING,
    organization_name: DataTypes.STRING,
    branch_id: DataTypes.INTEGER,
    company_logo_url: DataTypes.STRING,
    address: DataTypes.TEXT,
    telp: DataTypes.STRING,
    email: DataTypes.STRING,
    founded_date: DataTypes.DATEONLY,
    website_address: DataTypes.STRING,
    created_by: DataTypes.INTEGER,
    updated_by: DataTypes.INTEGER,
    active: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  }, {
    sequelize,
    modelName: 'companies',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return companies;
};