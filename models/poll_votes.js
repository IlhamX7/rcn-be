'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class pollVotes extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  pollVotes.init({
    poll_id: DataTypes.INTEGER,
    question_id: DataTypes.INTEGER,
    answer_id: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER,
    content: DataTypes.TEXT,
    company_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'poll_votes',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return pollVotes;
};