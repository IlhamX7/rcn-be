'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class onboardings extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  onboardings.init({
    title: DataTypes.STRING,
    done: DataTypes.BOOLEAN,
    stage: DataTypes.STRING,
    notes: DataTypes.TEXT,
    attachments: DataTypes.STRING,
    created_by: DataTypes.INTEGER,
    updated_by: DataTypes.INTEGER,
    company_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'onboardings',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return onboardings;
};