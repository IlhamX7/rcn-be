'use strict';
import { Model } from 'sequelize';
const ACTIVE = true;

module.exports = (sequelize, DataTypes) => {
  class RoleAccesses extends Model {
    static associate(models) {
      RoleAccesses.belongsTo(models.group_users, {
        foreignKey: 'user_role',
        targetKey: 'code'
      })
      RoleAccesses.belongsTo(models.menus, {
        foreignKey: 'menu_id',
        targetKey: 'id'
      })
    }
  };
  RoleAccesses.init({
    user_role: DataTypes.STRING,
    menu_id: DataTypes.INTEGER,
    active: {
      defaultValue: ACTIVE,
      type: DataTypes.BOOLEAN
    },
    is_insert: {
      defaultValue: ACTIVE,
      type: DataTypes.BOOLEAN
    },
    is_edit: {
      defaultValue: ACTIVE,
      type: DataTypes.BOOLEAN
    },
    is_view: {
      defaultValue: ACTIVE,
      type: DataTypes.BOOLEAN
    },
    is_delete: {
      defaultValue: ACTIVE,
      type: DataTypes.BOOLEAN
    },
    ordering: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    company_id: DataTypes.INTEGER,
  }, {
    sequelize,
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    modelName: 'role_accesses'
  });
  return RoleAccesses;
};