'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class positions extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  positions.init({
    job_position: DataTypes.STRING,
    job_level: DataTypes.ENUM(['Top-level management', 'Middle-level management', 'Lower-level management']),
    departement_id: DataTypes.INTEGER,
    company_id: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'positions',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return positions;
};