'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class assets extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  assets.init({
    user_id: DataTypes.INTEGER,
    asset_category_id: DataTypes.INTEGER,
    serial_number: DataTypes.STRING,
    description: DataTypes.TEXT,
    lend_date: DataTypes.DATEONLY,
    returned_date: DataTypes.DATEONLY,
    created_by: DataTypes.INTEGER,
    company_id: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'assets',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return assets;
};