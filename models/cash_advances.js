'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class cashAdvances extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  cashAdvances.init({
    user_id: DataTypes.INTEGER,
    request_id: DataTypes.INTEGER,
    request_date: DataTypes.DATEONLY,
    policy: DataTypes.STRING,
    porpose: DataTypes.STRING,
    amount: DataTypes.DECIMAL,
    status: DataTypes.INTEGER,
    company_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'cash_advances',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return cashAdvances;
};