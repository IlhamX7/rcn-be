'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class Provinces extends Model {
    static associate(models) {
      Provinces.hasOne(models.users, {
        foreignKey: 'province_id',
        targetKey: 'id'
      })
    }
  };
  Provinces.init({
    province_name: DataTypes.STRING
  }, {
    sequelize,
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    modelName: 'provinces',
  });
  return Provinces;
};
