'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class attendanceShifts extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  attendanceShifts.init({
    title: DataTypes.STRING,
    schedule_clock_in: DataTypes.TIME,
    schedule_clock_out: DataTypes.TIME,
    schedule_break_in: DataTypes.TIME,
    schedule_break_out: DataTypes.TIME,
    late_tolerance: DataTypes.BIGINT,
    company_id: DataTypes.INTEGER,
    active: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  }, {
    sequelize,
    modelName: 'attendance_shift',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return attendanceShifts;
};