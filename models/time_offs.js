'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class timeOffs extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  timeOffs.init({
    user_id: DataTypes.INTEGER,
    time_off_type_id: DataTypes.INTEGER,
    start_date: DataTypes.DATE,
    end_date: DataTypes.DATE,
    notes: DataTypes.TEXT,
    time_off_status: DataTypes.ENUM(['Approval', 'Pending', 'Process', 'Reject']),
    canceled: DataTypes.BOOLEAN,
    attachment: DataTypes.STRING,
    replacement: DataTypes.INTEGER,
    approval_replacement: DataTypes.DATE,
    approval_by: DataTypes.INTEGER,
    approval_date: DataTypes.DATE,
    company_id: DataTypes.INTEGER,
  }, {
    sequelize,
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    modelName: 'time_offs',
  });
  return timeOffs;
};