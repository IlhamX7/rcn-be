'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class Menus extends Model {
    static associate(models) {
      Menus.hasOne(models.role_accesses, {
        foreignKey: 'menu_id',
        targetKey: 'id'
      })
    }
  };
  Menus.init({
    title: DataTypes.STRING,
    link: DataTypes.STRING,
    parent_id: {
      defaultValue: 0,
      type: DataTypes.INTEGER
    },
    icon: DataTypes.STRING,
    menu_for: DataTypes.STRING,
    company_id: DataTypes.INTEGER,
  }, {
    sequelize,
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    modelName: 'menus',
  });
  return Menus;
};