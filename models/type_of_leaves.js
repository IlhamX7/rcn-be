'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class typeOfLeaves extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  typeOfLeaves.init({
    title: DataTypes.STRING,
    time_period: DataTypes.INTEGER,
    active: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    company_id: DataTypes.INTEGER,
  }, {
    sequelize,
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    modelName: 'type_of_leaves',
  });
  return typeOfLeaves;
};