'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class pollAnswers extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  pollAnswers.init({
    poll_id: DataTypes.INTEGER,
    question_id: DataTypes.INTEGER,
    active: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    content: DataTypes.TEXT,
    company_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'poll_answers',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return pollAnswers;
};