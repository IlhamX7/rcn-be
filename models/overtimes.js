'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class overtimes extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  overtimes.init({
    user_id: DataTypes.INTEGER,
    request_date: DataTypes.DATE,
    attendance_shift_id: DataTypes.INTEGER,
    overtime_before_duration: DataTypes.TIME,
    overtime_after_duration: DataTypes.TIME,
    break_before_duration: DataTypes.TIME,
    break_after_duration: DataTypes.TIME,
    notes: DataTypes.TEXT,
    company_id: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'overtimes',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return overtimes;
};