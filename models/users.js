'use strict'
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class users extends Model {
    static associate(models) {
      users.belongsTo(models.provinces, {
        foreignKey: 'province_id',
        targetKey: 'id'
      })
      users.belongsTo(models.cities, {
        foreignKey: 'city_id',
        targetKey: 'id'
      })
      users.belongsTo(models.group_users, {
        foreignKey: 'role',
        targetKey: 'code'
      })
    }
  }
  users.init({
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    mobile_number: DataTypes.STRING,
    role: DataTypes.STRING,
    date_of_birth: DataTypes.DATE,
    place_of_birth: DataTypes.STRING,
    province_id: DataTypes.INTEGER,
    city_id: DataTypes.INTEGER,
    profile_picture_url: DataTypes.STRING,
    account_status: DataTypes.BOOLEAN,
    employement_status: DataTypes.ENUM(['Contract', 'Fulltime', 'Frelance', 'Internship', 'Permanent']),
    gender: DataTypes.ENUM(['Male', 'Female']),
    marital_status: DataTypes.ENUM(['single', 'married', 'widow', 'widower']),
    blood_type: DataTypes.ENUM(['A', 'B', 'AB', 'O']),
    religion: DataTypes.ENUM(['Islam', 'Catholic', 'Christian', 'Buddha', 'Hindu', 'Confucius', 'Others']),
    id_type: DataTypes.ENUM(['KTP', 'Passport', 'SIM']),
    id_number: DataTypes.STRING,
    id_expiration_date: DataTypes.DATEONLY,
    postal_code: DataTypes.BIGINT,
    citizen_id_address: DataTypes.TEXT,
    residential_address: DataTypes.TEXT,
    position_id: DataTypes.INTEGER,
    employee_id: DataTypes.STRING,
    join_date: DataTypes.DATEONLY,
    contract_expiration_date: DataTypes.DATEONLY,
    permanent: DataTypes.BOOLEAN,
    created_by: DataTypes.INTEGER,
    approval_by: DataTypes.INTEGER,
    non_taxable_income_id: DataTypes.INTEGER,
    tax_identification_number: DataTypes.STRING,
    income_tax_rate_id: DataTypes.INTEGER,
    leader_id: {
      type: DataTypes.INTEGER,
      defaultValue: () => 0
    },
    salary: DataTypes.STRING,
    company_id: DataTypes.INTEGER,
  }, {
    sequelize,
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    modelName: 'users',
  })
  return users
}
