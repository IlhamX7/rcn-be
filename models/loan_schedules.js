'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class loanSchedules extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  loanSchedules.init({
    load_id: DataTypes.INTEGER,
    pay_date: DataTypes.DATEONLY,
    outstanding: DataTypes.DECIMAL,
    mounthly_payment: DataTypes.DECIMAL,
    interest: DataTypes.DECIMAL,
    principal: DataTypes.DECIMAL,
    total_principal_payment: DataTypes.DECIMAL,
    monthly_payment_number: DataTypes.INTEGER,
    company_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'loan_schedules',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return loanSchedules;
};