'use strict';
import { Model } from "sequelize"

module.exports = (sequelize, DataTypes) => {
  class EmailTemplate extends Model {
    static associate(models) {}
  };
  EmailTemplate.init({
    template_for: DataTypes.INTEGER,
    template_name_for: DataTypes.STRING,
    subject: DataTypes.STRING,
    body: DataTypes.TEXT,
    active: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    company_id: DataTypes.INTEGER,
  }, {
    sequelize,
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    modelName: 'email_templates',
  });
  return EmailTemplate;
};