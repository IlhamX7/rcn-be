'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class GroupUsers extends Model {
    static associate(models) {
      GroupUsers.hasMany(models.role_accesses, {
        foreignKey: 'user_role',
        sourceKey: 'code'
      })
    }
  };
  GroupUsers.init({
    code: DataTypes.STRING,
    title: DataTypes.STRING,
    company_id: DataTypes.INTEGER,
  }, {
    sequelize,
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    modelName: 'group_users'
  });
  return GroupUsers;
};