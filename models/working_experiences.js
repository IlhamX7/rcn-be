'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class workingExperiences extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  workingExperiences.init({
    company: DataTypes.STRING,
    position: DataTypes.STRING,
    from_date: DataTypes.DATEONLY,
    to_date: DataTypes.DATEONLY,
    length_of_service: DataTypes.INTEGER,
    certificate_of_employment: DataTypes.STRING,
    user_id: DataTypes.INTEGER,
    company_id: DataTypes.INTEGER,
  }, {
    sequelize,
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    modelName: 'working_experiences',
  });
  return workingExperiences;
};