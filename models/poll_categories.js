'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class pollCategories extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  pollCategories.init({
    poll_id: DataTypes.INTEGER,
    category_id: DataTypes.INTEGER,
    company_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'poll_categories',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return pollCategories;
};