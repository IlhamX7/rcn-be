'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class nonTaxableIncomes extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  nonTaxableIncomes.init({
    title: DataTypes.STRING,
    rate: DataTypes.DECIMAL,
    company_id: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'non_taxable_incomes',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return nonTaxableIncomes;
};