'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class allowances extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  allowances.init({
    user_id: DataTypes.INTEGER,
    allowance_type_id: DataTypes.INTEGER,
    company_id: DataTypes.INTEGER,
    created_by: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'allowances',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return allowances;
};