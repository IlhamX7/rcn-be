'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class announcements extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  announcements.init({
    title: DataTypes.STRING,
    description: DataTypes.TEXT,
    created_by: DataTypes.INTEGER,
    updated_by: DataTypes.INTEGER,
    company_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'announcements',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return announcements;
};