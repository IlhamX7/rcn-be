'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class insurances extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  insurances.init({
    user_id: DataTypes.INTEGER,
    insurance_type_id: DataTypes.INTEGER,
    insurance_id: DataTypes.STRING,
    premi: DataTypes.DECIMAL,
    polis_url: DataTypes.STRING,
    insured: DataTypes.STRING,
    premium_holiday: DataTypes.BOOLEAN,
    rider: DataTypes.DECIMAL,
    cash_value: DataTypes.DECIMAL,
    lapse: DataTypes.BOOLEAN,
    grace_period: DataTypes.DATEONLY,
    acquisition_cost: DataTypes.DECIMAL,
    claim: DataTypes.DECIMAL,
    company_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'insurance',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return insurances;
};