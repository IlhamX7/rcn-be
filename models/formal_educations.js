'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class formalEducations extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  formalEducations.init({
    grade: DataTypes.DOUBLE,
    institution_name: DataTypes.STRING,
    majors: DataTypes.STRING,
    start_year: DataTypes.DATEONLY,
    end_year: DataTypes.DATEONLY,
    score: DataTypes.DOUBLE,
    user_id: DataTypes.INTEGER,
    company_id: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'formal_educations',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return formalEducations;
};