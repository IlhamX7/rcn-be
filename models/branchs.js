'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class branchs extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  branchs.init({
    title: DataTypes.STRING,
    description: DataTypes.TEXT,
    created_by: DataTypes.INTEGER,
    updated_by: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'branchs',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return branchs;
};