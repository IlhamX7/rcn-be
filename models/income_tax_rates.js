'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class incomeTaxRates extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  incomeTaxRates.init({
    title: DataTypes.STRING,
    income_minimum: DataTypes.DECIMAL,
    income_maximum: DataTypes.DECIMAL,
    percentage_of_tax_rate: DataTypes.BIGINT,
    with_tax_id_number: DataTypes.BOOLEAN,
    company_id: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'income_tax_rates',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return incomeTaxRates;
};