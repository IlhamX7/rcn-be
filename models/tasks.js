'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class tasks extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  tasks.init({
    user_id: DataTypes.INTEGER,
    title: DataTypes.STRING,
    start_date: DataTypes.DATEONLY,
    end_date: DataTypes.DATEONLY,
    description: DataTypes.STRING,
    company_id: DataTypes.INTEGER,
    attachment: DataTypes.STRING,
    created_by: DataTypes.INTEGER,
    updated_by: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'tasks',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return tasks;
};