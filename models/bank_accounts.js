'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class bankAccounts extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  bankAccounts.init({
    user_id: DataTypes.INTEGER,
    bank_name: DataTypes.STRING,
    bank_account_holder: DataTypes.STRING,
    account_of_bank_number: DataTypes.STRING,
    bank_branch: DataTypes.STRING,
    expiry_date_of_bank_account: DataTypes.DATEONLY,
    company_id: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'bank_accounts',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return bankAccounts;
};