'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class departements extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  departements.init({
    title: DataTypes.STRING,
    active: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    company_id: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'departements',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return departements;
};