'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class reprimands extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  reprimands.init({
    user_id: DataTypes.INTEGER,
    reprimand_type_id: DataTypes.INTEGER,
    message: DataTypes.TEXT,
    start_date: DataTypes.DATEONLY,
    expired_date: DataTypes.DATEONLY,
    reprimand_status: DataTypes.BOOLEAN,
    created_by: DataTypes.INTEGER,
    updated_by: DataTypes.INTEGER,
    company_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'reprimands',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return reprimands;
};