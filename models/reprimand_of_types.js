'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class reprimandOfTypes extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  reprimandOfTypes.init({
    title: DataTypes.INTEGER,
    created_by: DataTypes.INTEGER,
    updated_by: DataTypes.INTEGER,
    company_id: DataTypes.INTEGER,
    active: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  }, {
    sequelize,
    modelName: 'reprimand_of_types',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return reprimandOfTypes;
};