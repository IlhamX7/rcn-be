'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class informalEducations extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  informalEducations.init({
    name: DataTypes.STRING,
    held_by: DataTypes.STRING,
    start_date: DataTypes.DATEONLY,
    end_date: DataTypes.DATEONLY,
    fee: DataTypes.STRING,
    certificate: DataTypes.STRING,
    user_id: DataTypes.INTEGER,
    company_id: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'informal_educations',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return informalEducations;
};