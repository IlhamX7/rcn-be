'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class pollings extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  pollings.init({
    user_id: DataTypes.INTEGER,
    title: DataTypes.STRING,
    slug: DataTypes.STRING,
    summary: DataTypes.TEXT,
    polling_of_type: DataTypes.INTEGER,
    published: DataTypes.BOOLEAN,
    starts_at: DataTypes.DATE,
    ends_at: DataTypes.DATE,
    content: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'pollings',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return pollings;
};