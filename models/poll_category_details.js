'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class pollCategoryDetails extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  pollCategoryDetails.init({
    parent_id: DataTypes.INTEGER,
    title: DataTypes.STRING,
    meta_title: DataTypes.STRING,
    slug: DataTypes.STRING,
    content: DataTypes.TEXT,
    company_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'poll_category_details',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return pollCategoryDetails;
};