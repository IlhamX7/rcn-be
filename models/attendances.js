'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class attendance extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  attendance.init({
    attendance_date: DataTypes.DATEONLY,
    user_id: DataTypes.INTEGER,
    attendance_shift_id: DataTypes.INTEGER,
    clock_in: DataTypes.TIME,
    clock_out: DataTypes.TIME,
    break_in: DataTypes.TIME,
    break_out: DataTypes.TIME,
    location: DataTypes.STRING,
    face_detection_accuracy: DataTypes.STRING,
    company_id: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'attendances',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return attendance;
};