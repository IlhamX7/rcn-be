'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class onboardingTypes extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  onboardingTypes.init({
    title: DataTypes.STRING,
    description: DataTypes.TEXT,
    active: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    created_by: DataTypes.INTEGER,
    updated_by: DataTypes.INTEGER,
    company_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'onboarding_types',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return onboardingTypes;
};