'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class insuranceTypes extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  insuranceTypes.init({
    title: DataTypes.STRING,
    active: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    created_by: DataTypes.INTEGER,
    updated_by: DataTypes.INTEGER,
    company_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'insurance_types',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return insuranceTypes;
};