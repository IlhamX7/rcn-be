'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class Cities extends Model {
    static associate(models) {
      Cities.hasOne(models.users, {
        foreignKey: 'city_id',
        targetKey: 'id'
      })
    }
  };
  Cities.init({
    province_id: DataTypes.INTEGER,
    city_name: DataTypes.STRING
  }, {
    sequelize,
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    modelName: 'cities'
  });
  return Cities;
};
