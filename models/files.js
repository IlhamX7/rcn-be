'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class files extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  files.init({
    path: DataTypes.STRING,
    file_for: DataTypes.STRING,
    bucket: DataTypes.STRING,
    onboarding_id: DataTypes.INTEGER,
    deleted_at: DataTypes.DATE,
    created_by: DataTypes.INTEGER,
    updated_by: DataTypes.INTEGER,
    company_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'files',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return files;
};