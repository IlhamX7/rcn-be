'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class reimbursements extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  reimbursements.init({
    user_id: DataTypes.INTEGER,
    transaction_id: DataTypes.INTEGER,
    reimbursement_name: DataTypes.STRING,
    description: DataTypes.TEXT,
    effective_date: DataTypes.DATEONLY,
    costs: DataTypes.DECIMAL,
    status: DataTypes.INTEGER,
    approval_of_leader: DataTypes.INTEGER,
    approval_date_of_leader: DataTypes.DATEONLY,
    approval_of_hrd: DataTypes.INTEGER,
    approval_date_of_hrd: DataTypes.DATEONLY,
    approval_of_finance: DataTypes.INTEGER,
    approval_date_of_finance: DataTypes.DATEONLY,
    attachments: DataTypes.STRING,
    company_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'reimbursements',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return reimbursements;
};