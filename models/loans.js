'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class loans extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  loans.init({
    first_payment: DataTypes.DATEONLY,
    amount_of_loan: DataTypes.DECIMAL,
    monthly_payment: DataTypes.INTEGER,
    interest_rate: DataTypes.DOUBLE,
    total_payment: DataTypes.DECIMAL,
    approval_of_leader: DataTypes.INTEGER,
    approval_date_of_leader: DataTypes.DATEONLY,
    approval_of_hrd: DataTypes.INTEGER,
    approval_date_of_hrd: DataTypes.DATEONLY,
    approval_of_finance: DataTypes.INTEGER,
    approval_date_of_finance: DataTypes.DATEONLY,
    company_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'loans',
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return loans;
};