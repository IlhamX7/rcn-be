import redis from 'redis'

const client = redis.createClient({
  port: process.env.REDIS_PORT || 6379,
  host: process.env.REDIS_HOST || 'localhost',
  auth_pass: process.env.REDIS_PASS || null
})

export const redisClient = client

/**
 * 
 * @param {string} key 
 * @param {*} value 
 * @returns 
 */
export const setCache = (key, value) => {
  return client.SET(key, value);
}

/**
 * 
 * @param {string} key 
 * @param {*} value 
 * @returns 
 */
export const deleteCache = (key, value) => {
  return client.DEL(key, value);
}

export const flushCache = () => {
  return client.flushdb();
}

export const closeClient = (callback) => {
  return client.quit(callback)
}

/**
 * 
 * @param {string} key 
 */
export const deleteMultiCache = (key) => {
  client.KEYS(key, (val) => {
    if (val) {
      val.forEach((v) => {
        client.DEL(v)
      })
    }
  });
}

/**
 * 
 * @param {string} key 
 * @returns 
 */
export const getCache =  (key) => {
  return new Promise((resolve, reject) => {
    client.get(key, (err, val) => {
      if (err) {
        resolve({
          status: false,
          result: err
        })
        return
      }

      if (val == null) {
        resolve({
          status: false,
          result: null
        })
        return
      }

      try {
        resolve({
          status: true,
          result: JSON.parse(val)
        })
      } catch (ex) {
        resolve({
          status: false,
          result: val
        })
      }
    })
  })
}
