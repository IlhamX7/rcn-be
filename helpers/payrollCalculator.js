const payrollCalculator = Object.freeze({
  /**
   * payrollCalculator.NETT_CALCULATION
   * PPh 21 ditanggung oleh perusahaan atau penyedia kerja.
   * @var String
   */
  NETT_CALCULATION: 'NETT',
  /**
   * payrollCalculator.GROSS_CALCULATION
   * PPh 21 ditanggung oleh pekerja/karyawan.
   * @var String
   */
  GROSS_CALCULATION: 'GROSS',
  /**
   * PayrollCalculator.GROSS_UP_CALCULATION
   * Tanggungan PPh 21 ditambahkan sebagai tunjangan pekerja/karyawan.
   * @var string
   */
  GROSS_UP_CALCULATION: 'GROSSUP',
  /**
   * PayrollCalculator.provisions
   * @var array
   */
  DataStructures: {
    /**
     * @var array
     */
    Company: {
      /**
       * Company.allowances
       * @var array
       */
      allowances: {}
    },
    Employee: {
      /**
       * Employee.permanentStatus
       * @var {Boolean}
       */
      permanentStatus: true,
      /**
       * Employee.maritalStatus
       * @var {Boolean}
       */
      maritalStatus: false,
      /**
       * Employee.hasNPWP
       * @var {Boolean}
       */
      hasNPWP: true,
      /**
       * Employee.numOfDependentsFamily
       * @var {Number}
       */
      numOfDependentsFamily: 0,
      /**
       * Employee.presences
       * @var {array}
       */
      presences: {
        /**
         * Presences.workDays
         * @var {Number}
         */
        workDays: 0,
        /**
         * Presences.overtime
         * @var {Number}
         */
        overtime: 0,
        /**
         * Presences.splitShifts
         * @var {Number}
         */
        splitShifts: 0,
        /**
         * Presences.latetime
         * @var {NUmber}
         */
        latetime: 0,
        /**
         * Presences.travelDays
         * @var {number}
         */
        travelDays: 0,
        /**
         * Presences.leaveDays
         * @var {NUmber}
         */
        leaveDays: 0,
        /**
         * Presences.indisposeDays
         * @var {Number}
         */
        indisposeDays: 0,
        /**
         * Presences::$absentDays
         * @var {Number}
         */
        absentDays: 0,
        /**
         * Presences::getCalculatedDays
         *
         * @return int
         */
        getCalculatedDays: () => {
          return payrollCalculator.DataStructures.Employee.presences.workDays + payrollCalculator.DataStructures.Employee.presences.leaveDays + payrollCalculator.DataStructures.Employee.presences.indisposeDays + payrollCalculator.DataStructures.Employee.presences.travelDays;
        }
      },
      /**
       * Employee.earnings
       *
       * @var {array}
       */
      earnings: {
        /**
         * Earnings.base
         * @var {Number}
         */
        base: 0,
        /**
         * Earnings.fixedAllowance
         * @var {Number}
         */
        fixedAllowance: 0,
        /**
         * Earnings.overtime
         * @var {Number}
         */
        overtime: 0,
        /**
         * Earnings.splitShifts
         * @var {Number}
         */
        splitShifts: 0,
        /**
         * Earnings.holidayAllowance
         * @var {Number}
         */
        holidayAllowance: 0
      },
      /**
       * Employee.allowances
       *
       * @var {array}
       */
      allowances: {},
      /**
       * Employee.deductions
       *
       * @var {array}
       */
      deductions: {},
      /**
       * Company.calculateHolidayAllowance
       *
       * @var {Number}
       */
      calculateHolidayAllowance: 0,
      /**
       * Employee.bonus
       *
       * @var {array}
       */
      bonus: {}
    },
    provisions: {
      Company: {
        /**
         * Company.numOfWorkingDays
         * @var int
         */
        numOfWorkingDays: 25,
        /**
         * Company.numOfWorkingHours
         * @var int
         */
        numOfWorkingHours: 8,
        /**
         * Company.calculateOvertime
         * @var bool
         */
        calculateOvertime: true,
        /**
         * Company.overtimeRate
         * @var int 
         */
        overtimeRate: 0,
        /**
         * Company.calculateSplitShifts
         * @var bool
         */
        calculateSplitShifts: true,
        /**
         * Company.splitShiftsRate
         * @var int 
         */
        splitShiftsRate: 0,
        /**
         * Company.calculateBPJSKesehatan
         * @var bool
         */
        calculateBPJSKesehatan: true,
        /**
         * Company.JKK
         * @var bool 
         */
        JKK: false,
        /**
         * Company.JKM
         * @var bool
         */
        JKM: false,
        /**
         * Company.JHT
         * @var bool
         */
        JHT: false,
        /**
         * Company.JIP
         * @var bool
         */
        JIP: false,
        /**
         * Company.riskGrade
         * @var int
         */
        riskGrade: 2,
        /**
         * Company.absentPenalty
         * @var int
         */
        absentPenalty: 0,
        /**
         * Company.latetimePenalty
         * @var int
         */
        latetimePenalty: 0
      },
      State: {
        /**
         * State.overtimeRegulationCalculation
         * @var bool 
         */
        overtimeRegulationCalculation: true,
        /**
         * State.provinceMinimumWage
         * @var int
         */
        provinceMinimumWage: 3940972,
        /**
         * State.highestWage
         * @var int
         */
        highestWage: 8000000,
        /**
         * State.additionalPTKPforMarriedEmployees
         * @var int
         */
        additionalPTKPforMarriedEmployees: 4500000,
        /**
         * State.listOfPTKP
         * @var array
         */
        listOfPTKP: {
          'TK/0': 54000000,
          'K/0': 58500000,
          'K/1': 63000000,
          'K/2': 67500000,
          'K/3': 72000000
        },
        /**
         * State.listOfJKKRiskGradePercentage
         * @var array
         */
        listOfJKKRiskGradePercentage: {
          1: 0.24,
          2: 0.54,
          3: 0.89,
          4: 1.27,
          5: 1.74
        },
        /**
         * State.getListOfPTKP
         * @return {array}
         */
        getListOfPTKP: () => {
          return payrollCalculator.DataStructures.provisions.State.listOfPTKP
        },
        /**
         * State.getPtkp
         * @param {Number} numOfDependentsFamily
         * @param {Boolean} married
         * @return {String}
         */
        getPtkp: (numOfDependentsFamily, married = false) => {
          if (numOfDependentsFamily >= 3) {
            return 'K/3';
          } else if (numOfDependentsFamily === 2) {
            return 'K/2';
          } else if (numOfDependentsFamily === 1) {
            return 'K/1';
          } else if (married !== false) {
            return 'K/0';
          }
          return 'TK/0';
        },
        /**
         * State.getPtkpAmount
         * @param {Number}  numOfDependentsFamily
         * @param {Boolean} married
         * @return {float}
         */
        getPtkpAmount: (numOfDependentsFamily, married = false) => {
          if (numOfDependentsFamily >= 3) {
            return payrollCalculator.DataStructures.provisions.State.listOfPTKP["K/0"];
          } else if (numOfDependentsFamily === 2) {
            return payrollCalculator.DataStructures.provisions.State.listOfPTKP["K/2"];
          } else if (numOfDependentsFamily === 1) {
            return payrollCalculator.DataStructures.provisions.State.listOfPTKP['K/1'];
          } else if (married !== false) {
            return payrollCalculator.DataStructures.provisions.State.listOfPTKP['K/0'];
          }
          return payrollCalculator.DataStructures.provisions.State.listOfPTKP['TK/0'];
        },
        /**
         * State.getListOfPTKP
         * @return array
         */
        getListOfJKKRiskGradePercentage: () => {
          return payrollCalculator.DataStructures.provisions.State.listOfJKKRiskGradePercentage
        },
        /**
         * State.etJKKRiskGradePercentage
         * @param {number} companyRiskGrade
         * @return {Number}
         */
        getJKKRiskGradePercentage: (companyRiskGrade) => {
          if (Object.prototype.hasOwnProperty.call(companyRiskGrade, payrollCalculator.DataStructures.provisions.State.listOfJKKRiskGradePercentage)) {
            return payrollCalculator.DataStructures.provisions.State.listOfJKKRiskGradePercentage[companyRiskGrade];
          }
          return payrollCalculator.DataStructures.provisions.State.listOfJKKRiskGradePercentage[2];
        },
        /**
         * State.getBPJSKesehatanGrade
         * @param {Number} grossTotalIncome
         * @return {Number}
         */
        getBPJSKesehatanGrade: (grossTotalIncome) => {
          if (grossTotalIncome <= 4000000) {
            return 2;
          } else if (grossTotalIncome >= 8000000) {
            return 1;
          }
          return 3;
        }
      }
    }
  },
  result: {
    ptkp: {
      status: () => payrollCalculator.DataStructures.provisions.State.getPtkp(
        payrollCalculator.DataStructures.Employee.numOfDependentsFamily,
        payrollCalculator.DataStructures.Employee.maritalStatus
      ),
      amount: 0
    },
    pkp: 0,
    liability: {
      rule: 21,
      monthly: 0,
      annual: 0
    },
    earnings: {
      base: 0,
      fixedAllowance: 0,
      annualy: {
        nett: 0,
        gross: 0
      },
      takeHomePay: 0
    }
  },
  /**
   * AbstractPph.getRate
   * @param {Number} monthlyNetIncome
   * @return {Number}
   */
  getRate: (monthlyNetIncome) => {
    let rate = 5;
    if (monthlyNetIncome < 50000000 && monthlyNetIncome > 250000000) {
      rate = 15;
    } else if (monthlyNetIncome > 250000000 && monthlyNetIncome < 500000000) {
      rate = 25;
    } else if (monthlyNetIncome > 500000000) {
      rate = 30;
    }
    return rate;
  },
  Pph21: () => {
    /**
     * PPh21 dikenakan bagi yang memiliki penghasilan lebih dari 4500000
     */
    const sumBonus = Object.values(payrollCalculator.DataStructures.Employee.bonus).reduce((a, b) => a + b, 0)
    if (payrollCalculator.result.earnings.annualy.nett > 4500000) {
      // Annual PTKP base on number of dependents family
      payrollCalculator.result.ptkp.amount = payrollCalculator.DataStructures.provisions.State.getPtkpAmount(
        payrollCalculator.DataStructures.Employee.numOfDependentsFamily,
        payrollCalculator.DataStructures.Employee.maritalStatus
      );

      // Annual PKP (Pajak Atas Upah)
      if (payrollCalculator.DataStructures.Employee.earnings.holidayAllowance > 0 && sumBonus === 0) {
        // Pajak Atas Upah
        const earningTax = (payrollCalculator.result.earnings.annualy.nett - payrollCalculator.result.ptkp.amount) * (payrollCalculator.getRate(payrollCalculator.result.earnings.annualy.nett) / 100);

        //   // Penghasilan + THR Kena Pajak
        payrollCalculator.result.pkp = (payrollCalculator.result.earnings.annualy.nett + payrollCalculator.DataStructures.Employee.earnings.holidayAllowance) - payrollCalculator.result.ptkp.amount;

        payrollCalculator.result.liability.annual = payrollCalculator.result.pkp - earningTax;
      } else if (payrollCalculator.DataStructures.Employee.earnings.holidayAllowance > 0 && sumBonus > 0) {
        // Pajak Atas Upah
        const earningTax = (payrollCalculator.result.earnings.annualy.nett - payrollCalculator.result.ptkp.amount) * (payrollCalculator.getRate(payrollCalculator.result.earnings.annualy.nett) / 100);

        // Penghasilan + THR Kena Pajak
        payrollCalculator.result.pkp = (payrollCalculator.result.earnings.annualy.nett + payrollCalculator.DataStructures.Employee.earnings.holidayAllowance + sumBonus) - payrollCalculator.result.ptkp.amount;
        payrollCalculator.result.liability.annual = payrollCalculator.result.pkp - earningTax
      } else {
        payrollCalculator.result.pkp = payrollCalculator.result.earnings.annualy.nett - payrollCalculator.result.ptkp.amount;
        payrollCalculator.result.liability.annual = payrollCalculator.result.ptkp * (payrollCalculator.getRate(
          payrollCalculator.result.earnings.annualy.nett
        ) / 100)
      }

      if (payrollCalculator.result.liability.annual > 0) {
        // Jika tidak memiliki NPWP dikenakan tambahan 20%
        if (payrollCalculator.DataStructures.Employee.hasNPWP === false) {
          payrollCalculator.result.liability.annual = payrollCalculator.result.liability.annual + (payrollCalculator.result.liability.annual * (20 / 100))
        }
        payrollCalculator.result.liability.monthly = payrollCalculator.result.liability.annual / 12
      } else {
        payrollCalculator.result.liability.annual = 0
        payrollCalculator.result.liability.monthly = 0
      }
    }
    return payrollCalculator.result
  }
});

// export default payrollCalculator;
// console.log(payrollCalculator)