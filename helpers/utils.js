import currencyFormatter from "currency-formatter";
import { validationResult } from 'express-validator';
import nodemailer from "nodemailer";
import { Op,Sequelize, ValidationError } from 'sequelize';

export const databaseCommonErrorMessage = {
  success: false,
  message: (err) => {
    if(process.env.NODE_ENV === 'development') {
      if (err instanceof Sequelize.ForeignKeyConstraintError) {
        return err.original.detail
      }
      else if (err instanceof ValidationError) {
        return err.errors[0].message
      }
      return err.message
    } else {
      return "There's something wrong with our database, please try again!"
    }
  }
}

export function sequelizeOrLike(search, arrayColumn) {
  const params = [];
  for (let i = 0; i < arrayColumn.length; i++) {
    if (search) {
      params.push({
        [`$${arrayColumn[i]}$`]: {
          [Op.iLike]: `%${search}%`
        }
      });
    }
  }
  return params;
}

/**
 * Create paginated Query using sequelize Schema
 * @param {*} schema 
 * @param {{
 * page: number
 * limit: number
 * }} query 
 * @returns {{
 * totalItems: number
 * totalPages: number
 * perPage: number
 * currentPage: number
 * data: [*]
 * }}
 */
export function sequelizePagination(schema, query) {
  const page = query.page || 1;
  const limit = query.limit || 10;
  if (limit > 0) {
    query.limit = limit;
    query.offset = (page - 1) * limit;
  }

  return new Promise((resolve, reject) => {
    schema.findAndCountAll(query).then(data => {
      resolve({
        "totalItems": data.count,
        "totalPages": Math.ceil(data.count / limit),
        "perPage": limit,
        "currentPage": page,
        "data": data.rows,
      })
    }).catch(err => {
      reject(err);
    });
  });
}

export function mailer(params) {
  return new Promise((resolve, reject) => {
    const transporter = nodemailer.createTransport({
      service: process.env.MAIL_DRIVER || 'Outlook365',
      host: process.env.MAIL_HOST || 'smtp.office365.com',
      secure: process.env.MAIL_SECURE || false,
      port: process.env.MAIL_PORT || 587,
      auth: {
        user: process.env.MAIL_USERNAME,
        pass: process.env.MAIL_PASSWORD
      },
      tls: {
        ciphers: process.env.MAIL_CIPHERS || 'SSLv3',
        rejectUnauthorized: process.env.MAIL_REJECTUNAUTHORIZED || false
      },
      logger: process.env.MAIL_LOGGER || false,
      debug: process.env.MAIL_DEBUG || false,
      encryption: process.env.MAIL_ENCRYPTION || 'tls'
    });

    const mailOptions = {};
    mailOptions.from = {
      name: process.env.MAIL_FROM_NAME,
      address: process.env.MAIL_FROM_ADDRESS
    }

    mailOptions.to = params.to;
    if (params.cc) {
      mailOptions.cc = params.cc;
    }
    mailOptions.subject = params.subject;

    if (params.isTEXT) {
      mailOptions.text = params.message;
    } else {
      mailOptions.html = params.message;
    }

    if (params.attachments) {
      mailOptions.attachments = params.attachments;
    }

    if (params.bcc) {
      mailOptions.bcc = params.bcc;
    }

    transporter.sendMail(mailOptions, function (error, info) {
      if (!error) {
        resolve(info)
      } else {
        reject(error)
      }
    });
  })
}

export function currency(params) {
  return currencyFormatter.format(params, {
    symbol: '',
    decimalSeparator: ',',
    thousandsSeparator: '.',
    symbolOnLeft: true,
    decimalDigits: 2
  })
}

export function indoDate(params) {
  const oldDate = new Date(params.date);
  const year = oldDate.getFullYear();
  let month = oldDate.getMonth();
  const date = padLeadingZeros(oldDate.getDate(), 2);
  let day = oldDate.getDay();
  const hour = padLeadingZeros(oldDate.getHours(), 2);
  const minute = padLeadingZeros(oldDate.getMinutes(), 2);
  const secound = padLeadingZeros(oldDate.getSeconds(), 2);

  switch (day) {
    case 0: day = "Minggu"; break;
    case 1: day = "Senin"; break;
    case 2: day = "Selasa"; break;
    case 3: day = "Rabu"; break;
    case 4: day = "Kamis"; break;
    case 5: day = "Jum'at"; break;
    case 6: day = "Sabtu"; break;
  }
  switch (month) {
    case 0: month = "Januari"; break;
    case 1: month = "Februari"; break;
    case 2: month = "Maret"; break;
    case 3: month = "April"; break;
    case 4: month = "Mei"; break;
    case 5: month = "Juni"; break;
    case 6: month = "Juli"; break;
    case 7: month = "Agustus"; break;
    case 8: month = "September"; break;
    case 9: month = "Oktober"; break;
    case 10: month = "November"; break;
    case 11: month = "Desember"; break;
  }

  if (params.showDateWithDay) {
    return `${day}, ${date} ${month} ${year}`;
  }
  if (params.showDateWithoutDay) {
    return `${date} ${month} ${year}`;
  }
  if (params.showDateWithDayAndTime) {
    return `${day}, ${date} ${month} ${year} ${hour}:${minute}:${secound}`;
  }
  if (params.showDateWithTime) {
    return `${date} ${month} ${year} ${hour}:${minute}:${secound}`;
  }
}

export function padLeadingZeros(num, size) {
  let s = num + "";
  while (s.length < size) s = "0" + s;
  return s;
}

/**
 * Middleware to handle validation using express-validator
 */
 export const validationChecker = (req, res, next) => {
  const errors = validationResult(req);

  if (errors.isEmpty())
    return next();

  const extractedErrors = errors.array().map(error => {
    return { column: error.param, message: error.msg }
  });

  return respond.failed(res, 400, "invalid inputs", extractedErrors);
}

export function arraySort(dataArr, params) {
  for (let x = params.length - 1; x >= 0; x--) {
    if (params[x].order === 'asc') {
      dataArr.sort(function (a, b) {
        if ((isNumeric(a[params[x].col]) && isNumeric(b[params[x].col])) || (isDate(a[params[x].col]) && isDate(b[params[x].col]))) {
          return a[params[x].col] - b[params[x].col];
        } else {
          if (a[params[x].col].toString() > b[params[x].col].toString()) {
            return 1;
          } else if (a[params[x].col].toString() < b[params[x].col].toString()) {
            return -1;
          } else {
            return 0;
          }
        }
      });
    } else {
      dataArr.sort(function (a, b) {
        if ((isNumeric(a[params[x].col]) && isNumeric(b[params[x].col])) || (isDate(a[params[x].col]) && isDate(b[params[x].col]))) {
          return b[params[x].col] - a[params[x].col];
        } else {
          if (a[params[x].col].toString() < b[params[x].col].toString()) {
            return 1;
          } else if (a[params[x].col].toString() > b[params[x].col].toString()) {
            return -1;
          } else {
            return 0;
          }
        }
      });
    }
  }
  return dataArr;
}

function isNumeric(input) {
  return (input - 0) === input && input.length > 0;
}

function isDate(testValue) {
  let returnValue = false;
  let testDate;
  try {
    testDate = new Date(testValue);
    if (!isNaN(testDate)) {
      returnValue = true;
    } else {
      returnValue = false;
    }
  }
  catch (e) {
    returnValue = false;
  }
  return returnValue;
}

/**
 * 
 * @param {{
 * joinDate: Date
 * cutOff: Date
 * salary: Number
 * }} params 
 * @returns {Number}
 */
export function HolidayAllowance(params) {
  const joinDate = params.joinDate
  const cutOff = params.cutOff
  const salary = params.salary
  const workingPeriod = 12

  if(cutOff) {
    const between = DateDiff('year', joinDate, cutOff)
    if (between > 0) {
      return salary
    } else {
      const yearsOfService = DateDiff('month', joinDate, cutOff)
      return (yearsOfService * salary) / workingPeriod
    }
  } else {
    return 0
  }
}

/**
 * 
 * @param {String} type 
 * @param {Date} start 
 * @param {Date} end 
 * @returns {Number}
 */
export function DateDiff(type, start, end) {
  const
    years = end.getFullYear() - start.getFullYear();
    const monthsStart = start.getMonth();
    const monthsEnd = end.getMonth()

  if (['m', 'mm', 'month', 'months'].includes(type))
    return (((years * 12) - (12 - monthsEnd)) + (12 - monthsStart));
  else if (['y', 'yy', 'year', 'years'].includes(type))
    return years;
  else if (['d', 'dd', 'day', 'days'].indexOf(type) !== -1)
    return ((end - start) / (1000 * 60 * 60 * 24));
  else
    return -1;
}

export const respond = {
  /** 
   * json serialized result
   * 
   * @param {number} success status code
   */
  body: (success, message = "", result = null) => ({ success, message, result }),

  /** 
   * wrap res with success '200' serialized output
   * 
   * @param {Response} res Response object
   * @param {any} payload any value
   * @param {string} message output explanation
   * @param {String} token
   * @return {Response} modified Response object
   */
  success: (res, payload, message = "") =>
    res.json(respond.body(true, message, payload)),

  /** 
   * wrap res with fail status serialized output
   * 
   * @param {Response} res Response object
   * @param {number} status status code
   * @param {string} message output explanation
   * @return {Response} modified Response object
   */
  failed: (res, status = 400, message = "", payload = null) =>
    res.status(status).json(respond.body(false, message, payload))
}