import jwt from 'jsonwebtoken'

import repo from '../repository/role_accesses'
import { databaseCommonErrorMessage, respond } from './utils'

const checkToken = (req, res, next) => {
  const token = req.headers["x-access-token"]
  if (!token) {
    return respond.failed(res, 403, 'No token provided!')
  }

  jwt.verify(token, process.env.JWT, (err, decoded) => {
    if (err) {
      return respond.failed(res, 403, 'Unauthorized')
    }
    req.userId = decoded.userId
    req.role = decoded.role
    req.companyId = decoded.companyId
    next()
  })
}

const checkRole = async (req, res, next) => {
  try {
    const method = req.method
    const reqParams = Object.values(req.params)
    let reqOriginalUrl = req.originalUrl.split("?").shift()
    const accessMenu = []
    reqOriginalUrl = reqOriginalUrl.replace('/api/v1', "")
    reqOriginalUrl = reqOriginalUrl.split('/').filter(s => { return s !== '' })
    for (const item of reqOriginalUrl) {
      const matches = reqParams.filter(s => s.includes(item))
      if (!matches.length) {
        accessMenu.push(item)
      }
    }

    const link = accessMenu.join('-');
    const roleAccessInfo = await repo.findIncludesMenuByLinkAndActiveAndRole(link, true, req.role, req.companyId)
    if (!roleAccessInfo) {
      return respond.failed(res, 403, 'make sure the menu link is registered')
    }

    if (method.toUpperCase() === 'GET' && roleAccessInfo.is_view) {
      return next();
    }
    else if (method.toUpperCase() === 'POST' && roleAccessInfo.is_insert) {
      return next();
    }
    else if (method.toUpperCase() === 'PUT' && roleAccessInfo.is_edit) {
      return next();
    }
    else if (method.toUpperCase() === 'DELETE' && roleAccessInfo.is_delete) {
      return next();
    }
    else {
      return respond.failed(res, 403, 'Access denied')
    }
  } catch (err) {
    return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
  }
}

const authJwt = {
  checkToken,
  checkRole
}

export default authJwt;
