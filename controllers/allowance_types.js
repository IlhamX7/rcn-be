'use strict';
import { check } from 'express-validator'

import { databaseCommonErrorMessage, respond } from '../helpers/utils'
import repo from '../repository/allowance_types';

const controller = Object.freeze({
  allowanceTypeValidation: [
    check([
      'title',
      'nominal',
      'formula',
      'active',
    ], `input must be a not empty string`).isString().notEmpty()
  ],

  create: async (req, res) => {
    try {
      const allowanceTypesInfo = await repo.create({
        title: req.body.title,
        nominal: req.body.nominal,
        formula: req.body.formula,
        active: req.body.active,
        company_id: req.companyId
      })
      return respond.success(res, allowanceTypesInfo, `allowance type created with id ${allowanceTypesInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  update: async (req, res) => {
    try {
      const allowanceTypesInfo = await repo.update({
        id: req.params.id,
        title: req.body.title,
        nominal: req.body.nominal,
        formula: req.body.formula,
        active: req.body.active
      })
      if (!allowanceTypesInfo) return respond.failed(res, 400, `allowance type with id ${req.params.id} not found`)
      return respond.success(res, allowanceTypesInfo, `allowance type updated with id ${allowanceTypesInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  delete: async (req, res) => {
    try {
      const allowanceTypesInfo = await repo.delete(req.params.id)
      if (allowanceTypesInfo === 0) return respond.failed(res, 400, `allowance type with id ${req.params.id} not found`)
      return respond.success(res, allowanceTypesInfo, `allowance type deleted with id ${req.params.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findOneById: async (req, res) => {
    try {
      const allowanceTypesInfo = await repo.findOneById(req.params.id, req.companyId)
      if(!allowanceTypesInfo) return respond.failed(res, 400, `allowance type with id ${req.params.id} not found`)
      return respond.success(res, allowanceTypesInfo, `successfully get allowance type with id ${allowanceTypesInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAll: async (req, res) => {
    try {
      const allowanceTypesInfo = await repo.findAll(req.companyId)
      if (!allowanceTypesInfo) return respond.failed(res, 400, `allowance type not exists`)
      return respond.success(res, allowanceTypesInfo, `successfully get all allowance types`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAllBySearchTermAndPaginated: async (req, res) => {
    const { term, page, limit } = req.query;
    try {
      const allowanceTypesInfo = await repo.findAllBySearchTermAndPaginated(term, req.companyId, page, limit)
      if (!allowanceTypesInfo.data.length) return respond.success(res, allowanceTypesInfo, `allowance type with paginated not exist`)
      respond.success(res, allowanceTypesInfo, `successfully get allowance type paginated`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  }
})

export default controller;