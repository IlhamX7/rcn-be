'use strict';
import { check } from 'express-validator'

import { databaseCommonErrorMessage, respond } from '../helpers/utils'
import repo from '../repository/working_experiences';

const controller = Object.freeze({
  workingExperienceValidation: [
    check([
      'company',
      'position'
    ], `input must be a not empty string`).isString().notEmpty(),
    check('from_date', `input must be a non empty string`).isString().notEmpty(),
    check('to_date', `input must be a non empty string`).isString().notEmpty(),
    check('length_of_service', `input must be a integer`).isInt().optional({ nullable: true }),
    check('certificate_of_employment', `input must be a string or null`).isString().optional({ nullable: true }),
  ],

  create: async (req, res) => {
    try {
      const workingExperienceInfo = await repo.create({
        company: req.body.company,
        position: req.body.position,
        from_date: req.body.from_date,
        to_date: req.body.to_date,
        length_of_service: req.body.length_of_service,
        certificate_of_employment: req.body.certificate_of_employment,
        user_id: req.body.user_id,
        company_id: req.companyId
      })
      return respond.success(res, workingExperienceInfo, `working experience created with id ${workingExperienceInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  update: async (req, res) => {
    try {
      const workingExperienceInfo = await repo.update({
        id: req.params.id,
        company: req.body.company,
        position: req.body.position,
        from_date: req.body.from_date,
        to_date: req.body.to_date,
        length_of_service: req.body.length_of_service,
        certificate_of_employment: req.body.certificate_of_employment
      })
      if (!workingExperienceInfo) return respond.failed(res, 400, `working experiences with id ${req.params.id} not found`)
      return respond.success(res, workingExperienceInfo, `working experiences updated with id ${workingExperienceInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  delete: async (req, res) => {
    try {
      const workingExperienceInfo = await repo.delete(req.params.id)
      if (workingExperienceInfo === 0) return respond.failed(res, 400, `working experience with id ${req.params.id} not found`)
      return respond.success(res, workingExperienceInfo, `working experience deleted with id ${req.params.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findOneById: async (req, res) => {
    try {
      const workingExperienceInfo = await repo.findOneById(req.params.id, req.companyId)
      if(!workingExperienceInfo) return respond.failed(res, 400, `working experiences with id ${req.params.id} not found`)
      return respond.success(res, workingExperienceInfo, `successfully get working experiences with id ${workingExperienceInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAll: async (req, res) => {
    try {
      const workingExperienceInfo = await repo.findAll(req.companyId)
      if (!workingExperienceInfo) return respond.failed(res, 400, `working experience not exists`)
      return respond.success(res, workingExperienceInfo, `successfully get all working experience`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAllBySearchTermAndPaginated: async (req, res) => {
    const { term, page, limit } = req.query;
    try {
      const workingExperienceInfo = await repo.findAllBySearchTermAndPaginated(term, req.companyId, page, limit)
      if (!workingExperienceInfo.data.length) return respond.success(res, workingExperienceInfo, `working experience with paginated not exist`)
      respond.success(res, workingExperienceInfo, `successfully get working experience paginated`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  }
})

export default controller;