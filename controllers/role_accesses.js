'use strict';
import { check } from 'express-validator'

import { databaseCommonErrorMessage, respond } from '../helpers/utils'
import repo from '../repository/role_accesses';

const controller = Object.freeze({
  roleAccessValidation: [
    check('user_role', `input must be a non empty string`).isString().notEmpty(),
    check('menu_ids', `input must be a non empty array`).isArray().notEmpty()
  ],

  createOrUpdate: async (req, res) => {
    try {
      const dataMenus = [];
      const menuIds = req.body.menu_ids;
      if (!menuIds) {
        const roleAccessInfo = await repo.deleteByUserRole(req.body.user_role)
        if (roleAccessInfo === 0) return respond.failed(res, 400, `role access with id ${req.params.id} not found`)
        return respond.success(res, roleAccessInfo, `role access deleted with id ${req.params.id}`)
      } else {
        await repo.deleteByUserRole(req.body.user_role)
        for (const [i, v] of Object.entries(menuIds)) {
          dataMenus[i] = dataMenus[i] || {}
          dataMenus[i].user_role = req.body.user_role
          dataMenus[i].menu_id = v.id
          dataMenus[i].active = v.active
          dataMenus[i].is_view = v.is_view
          dataMenus[i].is_edit = v.is_edit
          dataMenus[i].is_insert = v.is_insert
          dataMenus[i].is_delete = v.is_delete
          dataMenus[i].company_id = req.companyId
        }
        const roleAccessInfo = await repo.bulkCreate(dataMenus);
        return respond.success(res, roleAccessInfo, `successfully insert role access`)
      }
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAllBySearchTermAndPaginated: async (req, res) => {
    const { term, page, limit } = req.query;
    try {
      const roleAccessInfo = await repo.findAllBySearchTermAndPaginated(term, req.companyId, req.params.id, page, limit)
      if (!roleAccessInfo.data.length) return respond.success(res, roleAccessInfo, `role access with paginated not exist`)
      roleAccessInfo.data = await Promise.all(roleAccessInfo.data.map((item) => {
        item.active = !!item.active;
        item.is_edit = !!item.is_edit;
        item.is_insert = !!item.is_insert;
        item.is_view = !!item.is_view;
        item.is_delete = !!item.is_delete;
        return item;
      }))
      return respond.success(res, roleAccessInfo, `successfully get role access paginated`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findByRole: async (req, res) => {
    try {
      const roleAccessInfo = await repo.findByRole(req.role, req.companyId)
      const nest = ((items, id = 0, parentId = 'parent_id') => {
        return items
          .filter(item => item[parentId] === id)
          .map(item => ({ ...item, children: nest(items, item.id) }))
      });
      if (!roleAccessInfo.length) return respond.failed(res, 400, 'role accesses for frontend is not exist')
      return respond.success(res, nest(roleAccessInfo), 'successfully get role accesses for frontend by role')
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  getByRoleAndCompany: async (role, company) => {
    try {
      const roleAccessInfo = await repo.findByRole(role, company)
      const nest = ((items, id = 0, parentId = 'parent_id') => {
        return items
          .filter(item => item[parentId] === id)
          .map(item => ({ ...item, children: nest(items, item.id) }))
      });
      if (!roleAccessInfo.length) return []
      return nest(roleAccessInfo);
    } catch (err) {
      throw new Error(err) 
    }
  },

  moveDown: async (req, res) => {
    try {
      const current = await repo.findOneByOrdering(req.params.ordering, req.companyId)
      const afterCurrent = await repo.findOneIncludesMenuByRoleAndOrdering(req.params.roleId, req.params.ordering, req.companyId, 'ASC')

      if (!current) {
        return respond.failed(res, 400, `can't move down`)
      }
      const currentInfo = await repo.updateOrdering(current.ordering, afterCurrent.id, req.companyId)
      const afterCurrentInfo = await repo.updateOrdering(afterCurrent.ordering, current.id, req.companyId)
      return respond.success(res, null, `Successfully move down role from ${currentInfo.ordering} to ${afterCurrentInfo.ordering}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  moveUp: async (req, res) => {
    try {
      const current = await repo.findOneByOrdering(req.params.ordering, req.companyId)
      const beforeCurrent = await repo.findOneIncludesMenuByRoleAndOrdering(req.params.roleId, req.params.ordering, req.companyId, 'DESC')

      if (!beforeCurrent) {
        return respond.failed(res, 400, `can't move up`)
      }

      const currentInfo = await repo.updateOrdering(current.ordering, beforeCurrent.id, req.companyId)
      const beforeCurrentInfo = await repo.updateOrdering(beforeCurrent.ordering, current.id, req.companyId)
      return respond.success(res, null, `Successfully move up role from ${currentInfo.ordering} to ${beforeCurrentInfo.ordering}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  }
})

export default controller;