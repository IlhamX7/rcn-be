'use strict';
import { databaseCommonErrorMessage, respond } from '../helpers/utils'
import repo from '../repository/provinces';

const controller = Object.freeze({
  findAll: async (req, res) => {
    try {
      const provinceInfo = await repo.findAll()
      if (!provinceInfo) return respond.failed(res, 400, `province not exist`)
      return respond.success(res, provinceInfo, 'successfully get all province')
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findOneById: async (req, res) => {
    try {
      const provinceInfo = await repo.findOneById(req.params.id)
      if (!provinceInfo) return respond.failed(res, 400, `province with id ${req.params.id} not exist`)
      return respond.success(res, provinceInfo, 'successfully get province by id')
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAllBySearchTermAndPaginated: async (req, res) => {
    const { term, page, limit } = req.query;
    try {
      const provinceInfo = await repo.findAllBySearchTermAndPaginated(term, page, limit)
      if (!provinceInfo.data.length) return respond.success(res, provinceInfo, `province with paginated not exist`)
      respond.success(res, provinceInfo, `successfully get province paginated`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  }
})

export default controller;