'use strict';
import { body,check } from 'express-validator'

import { databaseCommonErrorMessage, respond } from '../helpers/utils'
import repo from '../repository/menu';

const controller = Object.freeze({
  menuValidation: [
    check([
      'title',
      'menu_for'
    ], `input must be a not empty string`).isString().notEmpty(),
    check('link', `input must be a non empty string`).isString().notEmpty(),
    check('parent_id', `input must be a integer`).isInt().optional({ nullable: true }),
    check('icon', `input must be a string or null`).isString().optional({ nullable: true }),
    body('menu_for').custom((value, { req }) => {
      if (value === 'frontend') {
        if (!req.body.icon) {
          throw new Error("icon must be a non empty string")
        }
        return true;
      }
      return true;
    })
  ],

  create: async (req, res) => {
    try {
      const menuInfo = await repo.create({
        title: req.body.title,
        link: req.body.link,
        parent_id: req.body.parent_id,
        icon: req.body.icon,
        menu_for: req.body.menu_for,
        company_id: req.companyId
      })
      return respond.success(res, menuInfo, `menu created with id ${menuInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  update: async (req, res) => {
    try {
      const menuInfo = await repo.update({
        id: req.params.id,
        title: req.body.title,
        link: req.body.link,
        parent_id: req.body.parent_id,
        icon: req.body.icon,
        menu_for: req.body.menu_for
      })
      if (!menuInfo) return respond.failed(res, 400, `menu with id ${req.params.id} not found`)
      return respond.success(res, menuInfo, `menu updated with id ${menuInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  delete: async (req, res) => {
    try {
      const menuInfo = await repo.delete(req.params.id)
      if (menuInfo === 0) return respond.failed(res, 400, `menu with id ${req.params.id} not found`)
      return respond.success(res, menuInfo, `menu deleted with id ${req.params.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findOneById: async (req, res) => {
    try {
      const menuInfo = await repo.findOneById(req.params.id, req.companyId)
      if(!menuInfo) return respond.failed(res, 400, `menu with id ${req.params.id} not found`)
      return respond.success(res, menuInfo, `successfully get menu with id ${menuInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAll: async (req, res) => {
    try {
      const menuInfo = await repo.findAll(req.companyId)
      if (!menuInfo) return respond.failed(res, 400, `menu not exists`)
      return respond.success(res, menuInfo, `successfully get all menu`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAllBySearchTermAndPaginated: async (req, res) => {
    const { term, page, limit } = req.query;
    try {
      const menuInfo = await repo.findAllBySearchTermAndPaginated(term, req.companyId, page, limit)
      if (!menuInfo.data.length) return respond.success(res, menuInfo, `menu with paginated not exist`)
      respond.success(res, menuInfo, `successfully get menu paginated`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  }
})

export default controller;