'use strict';
import { check } from 'express-validator'

import { databaseCommonErrorMessage, respond } from '../helpers/utils'
import repo from '../repository/overtimes';

const controller = Object.freeze({
  overtimeValidation: [
    check('user_id', `input must be a integer`).isInt().notEmpty(),
    check('request_date', `input must be a integer`).isInt().notEmpty(),
    check('attendance_shift_id', `input must be a integer`).isInt().optional({ nullable: true }),
    check('overtime_before_duration', `input must be a integer`).isString().optional({ nullable: true }),
    check('overtime_after_duration', `input must be a integer`).isString().optional({ nullable: true }),
    check('break_before_duration', `input must be a integer`).isString().optional({ nullable: true }),
    check('break_after_duration', `input must be a integer`).isString().optional({ nullable: true }),
    check('notes', `input must be a string`).isString().notEmpty()
  ],

  create: async (req, res) => {
    try {
      const overtimeInfo = await repo.create({
        user_id: req.body.user_id,
        request_date: req.body.request_date,
        attendance_shift_id: req.body.attendance_shift_id,
        overtime_before_duration: req.body.overtime_before_duration,
        overtime_after_duration: req.body.overtime_after_duration,
        break_before_duration: req.body.break_before_duration,
        break_after_duration: req.body.break_after_duration,
        notes: req.body.text,
        company_id: req.companyId
      })
      return respond.success(res, overtimeInfo, `overtime created with id ${overtimeInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  update: async (req, res) => {
    try {
      const overtimeInfo = await repo.update({
        id: req.params.id,
        user_id: req.body.user_id,
        request_date: req.body.request_date,
        attendance_shift_id: req.body.attendance_shift_id,
        overtime_before_duration: req.body.overtime_before_duration,
        overtime_after_duration: req.body.overtime_after_duration,
        break_before_duration: req.body.break_before_duration,
        break_after_duration: req.body.break_after_duration,
        notes: req.body.notes
      })
      if (!overtimeInfo) return respond.failed(res, 400, `overtime with id ${req.params.id} not found`)
      return respond.success(res, overtimeInfo, `overtime updated with id ${overtimeInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  delete: async (req, res) => {
    try {
      const overtimeInfo = await repo.delete(req.params.id)
      if (overtimeInfo === 0) return respond.failed(res, 400, `overtime with id ${req.params.id} not found`)
      return respond.success(res, overtimeInfo, `overtime deleted with id ${req.params.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findOneById: async (req, res) => {
    try {
      const overtimeInfo = await repo.findOneById(req.params.id, req.companyId)
      if(!overtimeInfo) return respond.failed(res, 400, `overtime with id ${req.params.id} not found`)
      return respond.success(res, overtimeInfo, `successfully get overtime with id ${overtimeInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAll: async (req, res) => {
    try {
      const overtimeInfo = await repo.findAll(req.companyId)
      if (!overtimeInfo) return respond.failed(res, 400, `overtime not exists`)
      return respond.success(res, overtimeInfo, `successfully get all overtime`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAllBySearchTermAndPaginated: async (req, res) => {
    const { term, page, limit } = req.query;
    try {
      const overtimeInfo = await repo.findAllBySearchTermAndPaginated(term, req.companyId, page, limit)
      if (!overtimeInfo.data.length) return respond.success(res, overtimeInfo, `overtime with paginated not exist`)
      respond.success(res, overtimeInfo, `successfully get overtime paginated`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  }
})

export default controller;