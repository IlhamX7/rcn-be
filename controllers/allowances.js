'use strict';
import { check } from 'express-validator'

import { databaseCommonErrorMessage, respond } from '../helpers/utils'
import repo from '../repository/allowances';

const controller = Object.freeze({
  allowanceValidation: [
    check(['user_id', 'allowance_type_id', 'company_id', 'created_by'], `input must be a integer`).isInt().optional({ nullable: false })
  ],

  create: async (req, res) => {
    try {
      const allowanceInfo = await repo.create({
        user_id: req.body.user_id,
        allowance_type_id: req.body.allowance_type_id,
        company_id: req.body.company_id,
        created_by: req.body.created_by
      })
      return respond.success(res, allowanceInfo, `allowance created with id ${allowanceInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  update: async (req, res) => {
    try {
      const allowanceInfo = await repo.update({
        id: req.params.id,
        user_id: req.body.user_id,
        allowance_type_id: req.body.allowance_type_id
      })
      if (!allowanceInfo) return respond.failed(res, 400, `allowance with id ${req.params.id} not found`)
      return respond.success(res, allowanceInfo, `allowance updated with id ${allowanceInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  delete: async (req, res) => {
    try {
      const allowanceInfo = await repo.delete(req.params.id)
      if (allowanceInfo === 0) return respond.failed(res, 400, `allowance with id ${req.params.id} not found`)
      return respond.success(res, allowanceInfo, `allowance deleted with id ${req.params.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findOneById: async (req, res) => {
    try {
      const allowanceInfo = await repo.findOneById(req.params.id, req.companyId)
      if(!allowanceInfo) return respond.failed(res, 400, `allowance with id ${req.params.id} not found`)
      return respond.success(res, allowanceInfo, `successfully get allowance with id ${allowanceInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAll: async (req, res) => {
    try {
      const allowanceInfo = await repo.findAll(req.companyId)
      if (!allowanceInfo) return respond.failed(res, 400, `allowance not exists`)
      return respond.success(res, allowanceInfo, `successfully get all allowance`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAllBySearchTermAndPaginated: async (req, res) => {
    const { term, page, limit } = req.query;
    try {
      const allowanceInfo = await repo.findAllBySearchTermAndPaginated(term, req.companyId, page, limit)
      if (!allowanceInfo.data.length) return respond.success(res, allowanceInfo, `allowance with paginated not exist`)
      respond.success(res, allowanceInfo, `successfully get allowance paginated`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  }
})

export default controller;