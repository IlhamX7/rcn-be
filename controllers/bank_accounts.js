'use strict';
import { check } from 'express-validator'

import { databaseCommonErrorMessage, respond } from '../helpers/utils'
import repo from '../repository/bank_accounts';

const controller = Object.freeze({
  bankAccountValidation: [
    check('bank_name', `input must be a non empty string`).isString().notEmpty(),
    check('bank_account_holder', `input must be a non empty string`).isString().notEmpty(),
    check('account_of_bank_number', `input must be a non empty string`).isString().notEmpty(),
    check('bank_branch', `input must be a non empty string`).isString().notEmpty(),
    check('expiry_date_of_bank_account', `input must be a non empty string`).isString().notEmpty(),
  ],

  create: async (req, res) => {
    try {
      const bankAccountInfo = await repo.create({
        user_id: req.body.user_id,
        bank_name: req.body.bank_name,
        bank_account_holder: req.body.bank_account_holder,
        account_of_bank_number: req.body.account_of_bank_number,
        bank_branch: req.body.bank_branch,
        expiry_date_of_bank_account: req.body.expiry_date_of_bank_account,
        company_id: req.companyId
      })
      return respond.success(res, bankAccountInfo, `bank account created with id ${bankAccountInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  update: async (req, res) => {
    try {
      const bankAccountInfo = await repo.update({
        id: req.params.id,
        bank_name: req.body.bank_name,
        bank_account_holder: req.body.bank_account_holder,
        account_of_bank_number: req.body.account_of_bank_number,
        bank_branch: req.body.bank_branch,
        expiry_date_of_bank_account: req.body.expiry_date_of_bank_account
      })
      if (!bankAccountInfo) return respond.failed(res, 400, `bank account with id ${req.params.id} not found`)
      return respond.success(res, bankAccountInfo, `bank account updated with id ${bankAccountInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  delete: async (req, res) => {
    try {
      const bankAccountInfo = await repo.delete(req.params.id)
      if (bankAccountInfo === 0) return respond.failed(res, 400, `bank account with id ${req.params.id} not found`)
      return respond.success(res, bankAccountInfo, `bank account deleted with id ${req.params.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findOneById: async (req, res) => {
    try {
      const bankAccountInfo = await repo.findOneById(req.params.id, req.companyId)
      if(!bankAccountInfo) return respond.failed(res, 400, `bank account with id ${req.params.id} not found`)
      return respond.success(res, bankAccountInfo, `successfully get bank account with id ${bankAccountInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAll: async (req, res) => {
    try {
      const bankAccountInfo = await repo.findAll(req.companyId)
      if (!bankAccountInfo) return respond.failed(res, 400, `bank account not exists`)
      return respond.success(res, bankAccountInfo, `successfully get all bank account`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAllBySearchTermAndPaginated: async (req, res) => {
    const { term, page, limit } = req.query;
    try {
      const bankAccountInfo = await repo.findAllBySearchTermAndPaginated(term, req.companyId, page, limit)
      if (!bankAccountInfo.data.length) return respond.success(res, bankAccountInfo, `bank account with paginated not exist`)
      respond.success(res, bankAccountInfo, `successfully get bank account paginated`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  }
})

export default controller;