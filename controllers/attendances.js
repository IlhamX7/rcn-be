'use strict';
import { check } from 'express-validator'

import { databaseCommonErrorMessage, respond } from '../helpers/utils'
import repo from '../repository/attendances';

const controller = Object.freeze({
  attendanceValidation: [
    check([
      'attendance_shift_id'
    ], 'input must be a not empty integer').isInt().notEmpty(),
    check([
      'clock_in',
      'clock_out',
      'break_in',
      'break_out',
    ], 'input must be a not empty time').isString().notEmpty(),
  ],

  create: async (req, res) => {
    try {
      const attendanceInfo = await repo.create({
        user_id: req.userId,
        attendance_shift_id: req.body.attendance_shift_id,
        attendance_date: new Date(),
        clock_in: req.body.clock_in,
        clock_out: req.body.clock_out,
        break_in: req.body.break_in,
        break_out: req.body.break_out,
        location: req.body.location,
        face_detection_accuracy: req.body.face_detection_accuracy,
        company_id: req.companyId
      })
      return respond.success(res, attendanceInfo, `attendance created with id ${attendanceInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  update: async (req, res) => {
    try {
      const attendanceInfo = await repo.update({
        id: req.params.id,
        attendance_shift_id: req.body.attendance_shift_id,
        clock_in: req.body.clock_in,
        clock_out: req.body.clock_out,
        break_in: req.body.break_in,
        break_out: req.body.break_out,
        location: req.body.location,
        face_detection_accuracy: req.body.face_detection_accuracy
      })
      if (!attendanceInfo) return respond.failed(res, 400, `attendance with id ${req.params.id} not found`)
      return respond.success(res, attendanceInfo, `attendance updated with id ${attendanceInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  delete: async (req, res) => {
    try {
      const attendanceInfo = await repo.delete(req.params.id)
      if (attendanceInfo === 0) return respond.failed(res, 400, `attendance with id ${req.params.id} not found`)
      return respond.success(res, attendanceInfo, `attendance deleted with id ${req.params.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findOneById: async (req, res) => {
    try {
      const attendanceInfo = await repo.findOneById(req.params.id, req.companyId)
      if(!attendanceInfo) return respond.failed(res, 400, `attendance with id ${req.params.id} not found`)
      return respond.success(res, attendanceInfo, `successfully get attendance with id ${attendanceInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAll: async (req, res) => {
    try {
      const attendanceInfo = await repo.findAll(req.companyId)
      if (!attendanceInfo) return respond.failed(res, 400, `attendance not exists`)
      return respond.success(res, attendanceInfo, `successfully get all attendance`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAllBySearchTermAndPaginated: async (req, res) => {
    const { term, page, limit } = req.query;
    try {
      const attendanceInfo = await repo.findAllBySearchTermAndPaginated(term, req.companyId, page, limit)
      if (!attendanceInfo.data.length) return respond.success(res, attendanceInfo, `attendance with paginated not exist`)
      respond.success(res, attendanceInfo, `successfully get attendance paginated`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  }
})

export default controller;