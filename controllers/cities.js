'use strict';
import { databaseCommonErrorMessage, respond } from '../helpers/utils'
import repo from '../repository/cities';

const controller = Object.freeze({
  findAll: async (req, res) => {
    try {
      const cityInfo = await repo.findAll()
      if (!cityInfo) return respond.failed(res, 400, 'city not exist')
      return respond.success(res, cityInfo, 'successfully get all city')
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findOneById: async (req, res) => {
    try {
      const cityInfo = await repo.findOneById(req.params.id)
      if (!cityInfo) return respond.failed(res, 400, `city with id ${req.params.id} not exist`)
      return respond.success(res, cityInfo, 'successfully get city by id')
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },
  
  findAllBySearchTermAndPaginated: async (req, res) => {
    const { term, page, limit } = req.query;
    try {
      const cityInfo = await repo.findAllBySearchTermAndPaginated(term, page, limit)
      if (!cityInfo.data.length) return respond.success(res, cityInfo, `city with paginated not exist`)
      respond.success(res, cityInfo, `successfully get city paginated`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  }
})

export default controller;