'use strict';
import { check } from 'express-validator'

import { databaseCommonErrorMessage, respond } from '../helpers/utils'
import repo from '../repository/time_offs';

const controller = Object.freeze({
  timeOffValidation: [
    check('time_off_type_id', 'input must be a not empty').isInt().notEmpty(),
    check([
      'start_date',
      'end_date'
    ], 'input must be a not empty date format (Y-m-d').isDate({format: 'YYYY-MM-DD'}).notEmpty(),
    check(['notes','attachment'], 'input must be a string').isString().optional({nullable: true}),
    check('canceled', 'input must be a not empty boolean').isBoolean().optional({nullable: true}),
    check('replacement','input must be a not empty').isInt().optional({nullable: true})
  ],

  create: async (req, res) => {
    try {
      const timeOffInfo = await repo.create({
        user_id: req.userId,
        time_off_type_id: req.body.time_off_type_id,
        start_date: req.body.start_date,
        end_date: req.body.end_date,
        notes: req.body.notes,
        canceled: req.body.canceled,
        attachment: req.body.attachment,
        replacement: req.body.replacement,
        company_id: req.companyId
      })
      return respond.success(res, timeOffInfo, `time off created with id ${timeOffInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  update: async (req, res) => {
    try {
      const timeOffInfo = await repo.update({
        id: req.params.id,
        time_off_type_id: req.body.time_off_type_id,
        start_date: req.body.start_date,
        end_date: req.body.end_date,
        notes: req.body.notes,
        canceled: req.body.canceled,
        attachment: req.body.attachment,
        replacement: req.body.replacement
      })
      if (!timeOffInfo) return respond.failed(res, 400, `time off with id ${req.params.id} not found`)
      return respond.success(res, timeOffInfo, `time off updated with id ${timeOffInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  delete: async (req, res) => {
    try {
      const timeOffInfo = await repo.delete(req.params.id)
      if (timeOffInfo === 0) return respond.failed(res, 400, `time off with id ${req.params.id} not found`)
      return respond.success(res, timeOffInfo, `time off deleted with id ${req.params.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findOneById: async (req, res) => {
    try {
      const timeOffInfo = await repo.findOneById(req.params.id, req.companyId)
      if (!timeOffInfo) return respond.failed(res, 400, `time off with id ${req.params.id} not found`)
      return respond.success(res, timeOffInfo, `successfully get time off with id ${timeOffInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAll: async (req, res) => {
    try {
      const timeOffInfo = await repo.findAll(req.companyId)
      if (!timeOffInfo) return respond.failed(res, 400, `time off not exists`)
      return respond.success(res, timeOffInfo, `successfully get all time off`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAllBySearchTermAndPaginated: async (req, res) => {
    const { term, page, limit } = req.query;
    try {
      const timeOffInfo = await repo.findAllBySearchTermAndPaginated(term, req.companyId, page, limit)
      if (!timeOffInfo.data.length) return respond.success(res, timeOffInfo, `time off with paginated not exist`)
      respond.success(res, timeOffInfo, `successfully get time off paginated`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  }
})

export default controller;