'use strict';
import { check } from 'express-validator'

import { databaseCommonErrorMessage, respond } from '../helpers/utils'
import repo from '../repository/income_tax_rates';

const controller = Object.freeze({
  incomeTaxRatesValidation: [
    check('title',`input must be a not empty string`).isString().notEmpty(),
    check([
      'income_minimum',
      'income_maximum'
    ], `input must be a decimal`).isDecimal().notEmpty(),
    check('percentage_of_tax_rate', `input must be a integer`).isInt().notEmpty(),
    check('with_tax_id_number', `input must be a integer`).isBoolean().notEmpty(),
  ],

  create: async (req, res) => {
    try {
      const incomeTaxRateInfo = await repo.create({
        title: req.body.title,
        income_minimum: req.body.income_minimum,
        income_maximum: req.body.income_maximum,
        percentage_of_tax_rate: req.body.percentage_of_tax_rate,
        with_tax_id_number: req.body.with_tax_id_number,
        company_id: req.companyId
      })
      return respond.success(res, incomeTaxRateInfo, `income tax rates created with id ${incomeTaxRateInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  update: async (req, res) => {
    try {
      const incomeTaxRateInfo = await repo.update({
        id: req.params.id,
        title: req.body.title,
        income_minimum: req.body.income_minimum,
        income_maximum: req.body.income_maximum,
        percentage_of_tax_rate: req.body.percentage_of_tax_rate,
        with_tax_id_number: req.body.with_tax_id_number
      })
      if (!incomeTaxRateInfo) return respond.failed(res, 400, `income tax rates with id ${req.params.id} not found`)
      return respond.success(res, incomeTaxRateInfo, `income tax rates updated with id ${incomeTaxRateInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  delete: async (req, res) => {
    try {
      const incomeTaxRateInfo = await repo.delete(req.params.id)
      if (incomeTaxRateInfo === 0) return respond.failed(res, 400, `income tax rates with id ${req.params.id} not found`)
      return respond.success(res, incomeTaxRateInfo, `income tax rates deleted with id ${req.params.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findOneById: async (req, res) => {
    try {
      const incomeTaxRateInfo = await repo.findOneById(req.params.id, req.companyId)
      if(!incomeTaxRateInfo) return respond.failed(res, 400, `income tax rates with id ${req.params.id} not found`)
      return respond.success(res, incomeTaxRateInfo, `successfully get income tax rates with id ${incomeTaxRateInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAll: async (req, res) => {
    try {
      const incomeTaxRateInfo = await repo.findAll(req.companyId)
      if (!incomeTaxRateInfo) return respond.failed(res, 400, `income tax rates not exists`)
      return respond.success(res, incomeTaxRateInfo, `successfully get all income tax rates`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAllBySearchTermAndPaginated: async (req, res) => {
    const { term, page, limit } = req.query;
    try {
      const incomeTaxRateInfo = await repo.findAllBySearchTermAndPaginated(term, req.companyId, page, limit)
      if (!incomeTaxRateInfo.data.length) return respond.success(res, incomeTaxRateInfo, `income tax rates with paginated not exist`)
      respond.success(res, incomeTaxRateInfo, `successfully get income tax rates paginated`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  }
})

export default controller;