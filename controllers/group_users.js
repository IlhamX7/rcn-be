'use strict';
import { check } from 'express-validator'

import { databaseCommonErrorMessage, respond } from '../helpers/utils'
import repo from '../repository/group_users';

const controller = Object.freeze({
  groupUserValidation: [
    check([
      'code',
      'title'
    ], `input must be a not empty string`).isString().notEmpty()
  ],

  create: async (req, res) => {
    try {
      const groupUserInfo = await repo.create({
        code: req.body.code,
        title: req.body.title,
        company_id: req.companyId
      })
      return respond.success(res, groupUserInfo, `group user created with id ${groupUserInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  update: async (req, res) => {
    try {
      const groupUserInfo = await repo.update({
        id: req.params.id,
        title: req.body.title
      })
      if (!groupUserInfo) return respond.failed(res, 400, `group user with id ${req.params.id} not found`)
      return respond.success(res, groupUserInfo, `group user updated with id ${groupUserInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  delete: async (req, res) => {
    try {
      const groupUserInfo = await repo.delete(req.params.id)
      if (groupUserInfo === 0) return respond.failed(res, 400, `group user with id ${req.params.id} not found`)
      return respond.success(res, groupUserInfo, `group user deleted with id ${req.params.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findOneById: async (req, res) => {
    try {
      const groupUserInfo = await repo.findOneById(req.params.id, req.companyId)
      if (!groupUserInfo) return respond.failed(res, 400, `group user with id ${req.params.id} not found`)
      return respond.success(res, groupUserInfo, `successfully get group user with id ${groupUserInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAll: async (req, res) => {
    try {
      const groupUserInfo = await repo.findAll(req.companyId)
      if (!groupUserInfo) return respond.failed(res, 400, `group user not exists`)
      return respond.success(res, groupUserInfo, `successfully get all group user`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAllBySearchTermAndPaginated: async (req, res) => {
    const { term, page, limit } = req.query;
    try {
      const groupUserInfo = await repo.findAllBySearchTermAndPaginated(term, req.companyId, page, limit)
      if (!groupUserInfo.data.length) return respond.success(res, groupUserInfo, `group user with paginated not exist`)
      respond.success(res, groupUserInfo, `successfully get group user paginated`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  }
})

export default controller;