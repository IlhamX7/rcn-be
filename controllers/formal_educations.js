'use strict';
import { check } from 'express-validator'

import { databaseCommonErrorMessage, respond } from '../helpers/utils'
import repo from '../repository/formal_educations';

const controller = Object.freeze({
  formalEducationValidation: [
    check('grade', `input must be a integer`).isInt().optional({ nullable: true }),
    check('institution_name', `input must be a non empty string`).isString().notEmpty(),
    check('majors', `input must be a string or null`).isString().optional({ nullable: true }),
    check('start_year', `input must be a string or null`).isString().optional({ nullable: true }),
    check('end_year', `input must be a string or null`).isString().optional({ nullable: true }),
    check('score', `input must be a integer`).isInt().optional({ nullable: true }),
  ],

  create: async (req, res) => {
    try {
      const formalEducationInfo = await repo.create({
        grade: req.body.grade,
        institution_name: req.body.institution_name,
        majors: req.body.majors,
        start_year: req.body.start_year,
        end_year: req.body.end_year,
        score: req.body.score,
        user_id: req.body.user_id,
        company_id: req.companyId
      })
      return respond.success(res, formalEducationInfo, `formal education created with id ${formalEducationInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  update: async (req, res) => {
    try {
      const formalEducationInfo = await repo.update({
        id: req.params.id,
        grade: req.body.grade,
        institution_name: req.body.institution_name,
        majors: req.body.majors,
        start_year: req.body.start_year,
        end_year: req.body.end_year,
        score: req.params.score
      })
      if (!formalEducationInfo) return respond.failed(res, 400, `formal education with id ${req.params.id} not found`)
      return respond.success(res, formalEducationInfo, `formal education updated with id ${formalEducationInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  delete: async (req, res) => {
    try {
      const formalEducationInfo = await repo.delete(req.params.id)
      if (formalEducationInfo === 0) return respond.failed(res, 400, `formal education with id ${req.params.id} not found`)
      return respond.success(res, formalEducationInfo, `formal education deleted with id ${req.params.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findOneById: async (req, res) => {
    try {
      const formalEducationInfo = await repo.findOneById(req.params.id, req.companyId)
      if(!formalEducationInfo) return respond.failed(res, 400, `formal education with id ${req.params.id} not found`)
      return respond.success(res, formalEducationInfo, `successfully get formal education with id ${formalEducationInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAll: async (req, res) => {
    try {
      const formalEducationInfo = await repo.findAll(req.companyId)
      if (!formalEducationInfo) return respond.failed(res, 400, `formal education not exists`)
      return respond.success(res, formalEducationInfo, `successfully get all formal education`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAllBySearchTermAndPaginated: async (req, res) => {
    const { term, page, limit } = req.query;
    try {
      const formalEducationInfo = await repo.findAllBySearchTermAndPaginated(term, req.companyId, page, limit)
      if (!formalEducationInfo.data.length) return respond.success(res, formalEducationInfo, `formal education with paginated not exist`)
      respond.success(res, formalEducationInfo, `successfully get formal education paginated`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  }
})

export default controller;