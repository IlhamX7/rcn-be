'use strict';
import { check } from 'express-validator'

import { databaseCommonErrorMessage, respond } from '../helpers/utils'
import repo from '../repository/positions';

const controller = Object.freeze({
  positionValidation: [
    check('job_position', `input must be a not empty string`).isString().notEmpty()
  ],

  create: async (req, res) => {
    try {
      const positionInfo = await repo.create({
        job_position: req.body.job_position,
        departement_id: req.body.departement_id,
        company_id: req.companyId
      })
      return respond.success(res, positionInfo, `position created with id ${positionInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  update: async (req, res) => {
    try {
      const positionInfo = await repo.update({
        id: req.params.id,
        job_position: req.body.job_position
      })
      if (!positionInfo) return respond.failed(res, 400, `position with id ${req.params.id} not found`)
      return respond.success(res, positionInfo, `position updated with id ${positionInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  delete: async (req, res) => {
    try {
      const positionInfo = await repo.delete(req.params.id)
      if (positionInfo === 0) return respond.failed(res, 400, `position with id ${req.params.id} not found`)
      return respond.success(res, positionInfo, `position deleted with id ${req.params.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findOneById: async (req, res) => {
    try {
      const positionInfo = await repo.findOneById(req.params.id, req.companyId)
      if(!positionInfo) return respond.failed(res, 400, `position with id ${req.params.id} not found`)
      return respond.success(res, positionInfo, `successfully get position with id ${positionInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAll: async (req, res) => {
    try {
      const positionInfo = await repo.findAll(req.companyId)
      if (!positionInfo) return respond.failed(res, 400, `position not exists`)
      return respond.success(res, positionInfo, `successfully get all position`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAllBySearchTermAndPaginated: async (req, res) => {
    const { term, page, limit } = req.query;
    try {
      const positionInfo = await repo.findAllBySearchTermAndPaginated(term, req.companyId, page, limit)
      if (!positionInfo.data.length) return respond.success(res, positionInfo, `position with paginated not exist`)
      respond.success(res, positionInfo, `successfully get position paginated`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  }
})

export default controller;