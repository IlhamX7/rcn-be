'use strict';
import { check } from 'express-validator'

import { databaseCommonErrorMessage, respond } from '../helpers/utils'
import repo from '../repository/attendance_shifts';

const controller = Object.freeze({
  attendanceShiftValidation: [
    check([
      'title',
      'schedule_clock_in',
      'schedule_clock_out',
      'schedule_break_in',
      'schedule_break_out'
    ], 'input must be a not empty string').isString().notEmpty(),
    check('late_tolerance', 'input must be a not empty integer').isInt().notEmpty(),
    check('active', 'input must be a not empty boolean').isBoolean().notEmpty(),
  ],

  create: async (req, res) => {
    try {
      const attendanceShiftsInfo = await repo.create({
        title: req.body.title,
        schedule_clock_in: req.body.schedule_clock_in,
        schedule_clock_out: req.body.schedule_clock_out,
        schedule_break_in: req.body.schedule_break_in,
        schedule_break_out: req.body.schedule_break_out,
        late_tolerance: req.body.late_tolerance,
        company_id: req.companyId,
        active: req.body.active
      })
      return respond.success(res, attendanceShiftsInfo, `attendance shifts created with id ${attendanceShiftsInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  update: async (req, res) => {
    try {
      const attendanceShiftsInfo = await repo.update({
        id: req.params.id,
        title: req.body.title,
        schedule_clock_in: req.body.schedule_clock_in,
        schedule_clock_out: req.body.schedule_clock_out,
        schedule_break_in: req.body.schedule_break_in,
        schedule_break_out: req.body.schedule_break_out,
        late_tolerance: req.body.late_tolerance,
        active: req.body.active
      })
      if (!attendanceShiftsInfo) return respond.failed(res, 400, `attendance shifts with id ${req.params.id} not found`)
      return respond.success(res, attendanceShiftsInfo, `attendance shifts updated with id ${attendanceShiftsInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  delete: async (req, res) => {
    try {
      const attendanceShiftsInfo = await repo.delete(req.params.id)
      if (attendanceShiftsInfo === 0) return respond.failed(res, 400, `attendance shifts with id ${req.params.id} not found`)
      return respond.success(res, attendanceShiftsInfo, `attendance shifts deleted with id ${req.params.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findOneById: async (req, res) => {
    try {
      const attendanceShiftsInfo = await repo.findOneById(req.params.id, req.companyId)
      if (!attendanceShiftsInfo) return respond.failed(res, 400, `attendance shifts with id ${req.params.id} not found`)
      return respond.success(res, attendanceShiftsInfo, `successfully get attendance shifts with id ${attendanceShiftsInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAll: async (req, res) => {
    try {
      const attendanceShiftsInfo = await repo.findAll(req.companyId)
      if (!attendanceShiftsInfo) return respond.failed(res, 400, `attendance shifts not exists`)
      return respond.success(res, attendanceShiftsInfo, `successfully get all attendance shifts`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAllBySearchTermAndPaginated: async (req, res) => {
    const { term, page, limit } = req.query;
    try {
      const attendanceShiftsInfo = await repo.findAllBySearchTermAndPaginated(term, req.companyId, page, limit)
      if (!attendanceShiftsInfo.data.length) return respond.success(res, attendanceShiftsInfo, `attendance shifts with paginated not exist`)
      respond.success(res, attendanceShiftsInfo, `successfully get attendance shifts paginated`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  }
})

export default controller;