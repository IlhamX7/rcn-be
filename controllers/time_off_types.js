'use strict';
import { check } from 'express-validator'

import { databaseCommonErrorMessage, respond } from '../helpers/utils'
import repo from '../repository/time_off_types';

const controller = Object.freeze({
  timeOffTypeValidation: [
    check('title', 'input must be a not empty string').isString().notEmpty(),
    check('active', 'input must be a not empty boolean').isBoolean().notEmpty(),
  ],

  create: async (req, res) => {
    try {
      const timeOffTypeInfo = await repo.create({
        title: req.body.title,
        active: req.body.active,
        company_id: req.companyId
      })
      return respond.success(res, timeOffTypeInfo, `time off type created with id ${timeOffTypeInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  update: async (req, res) => {
    try {
      const timeOffTypeInfo = await repo.update({
        id: req.params.id,
        title: req.body.title,
        active: req.body.active
      })
      if (!timeOffTypeInfo) return respond.failed(res, 400, `time off type with id ${req.params.id} not found`)
      return respond.success(res, timeOffTypeInfo, `time off type updated with id ${timeOffTypeInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  delete: async (req, res) => {
    try {
      const timeOffTypeInfo = await repo.delete(req.params.id)
      if (timeOffTypeInfo === 0) return respond.failed(res, 400, `time off type with id ${req.params.id} not found`)
      return respond.success(res, timeOffTypeInfo, `time off type deleted with id ${req.params.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findOneById: async (req, res) => {
    try {
      const timeOffTypeInfo = await repo.findOneById(req.params.id, req.companyId)
      if (!timeOffTypeInfo) return respond.failed(res, 400, `time off type with id ${req.params.id} not found`)
      return respond.success(res, timeOffTypeInfo, `successfully get time off type with id ${timeOffTypeInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAll: async (req, res) => {
    try {
      const timeOffTypeInfo = await repo.findAll(req.companyId)
      if (!timeOffTypeInfo) return respond.failed(res, 400, `time off type not exists`)
      return respond.success(res, timeOffTypeInfo, `successfully get all time off type`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAllBySearchTermAndPaginated: async (req, res) => {
    const { term, page, limit } = req.query;
    try {
      const timeOffTypeInfo = await repo.findAllBySearchTermAndPaginated(term, req.companyId, page, limit)
      if (!timeOffTypeInfo.data.length) return respond.success(res, timeOffTypeInfo, `time off type with paginated not exist`)
      respond.success(res, timeOffTypeInfo, `successfully get time off type paginated`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  }
})

export default controller;