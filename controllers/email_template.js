'use strict';
import { check } from 'express-validator'

import { databaseCommonErrorMessage, mailer, respond } from '../helpers/utils'
import repo from '../repository/email_template';

const controller = Object.freeze({
  menuTemplateValidation: [
    check('template_for', `input must be a integer`).isInt().notEmpty(),
    check([
      'template_name_for',
      'subject',
      'body',
    ], `input must be a non empty string`).isString().notEmpty(),
    check('active', `input must be a boolean`).isBoolean().optional({ nullable: true })
  ],

  create: async (req, res) => {
    try {
      const emailTemplateInfo = await repo.create({
        template_for: req.body.template_for,
        template_name_for: req.body.template_name_for,
        subject: req.body.subject,
        body: req.body.body,
        active: req.body.active,
        company_id: req.companyId
      })
      return respond.success(res, emailTemplateInfo, `email template created with id ${emailTemplateInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  update: async (req, res) => {
    try {
      const emailTemplateInfo = await repo.update({
        id: req.params.id,
        template_for: req.body.template_for,
        template_name_for: req.body.template_name_for,
        subject: req.body.subject,
        body: req.body.body,
        active: req.body.active,
      })
      if (!emailTemplateInfo) return respond.failed(res, 400, `email template with id ${req.params.id} not found`)
      return respond.success(res, emailTemplateInfo, `email template updated with id ${emailTemplateInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  delete: async (req, res) => {
    try {
      const emailTemplateInfo = await repo.delete(req.params.id)
      if (emailTemplateInfo === 0) return respond.failed(res, 400, `email template with id ${req.params.id} not found`)
      return respond.success(res, emailTemplateInfo, `email template deleted with id ${req.params.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findOneById: async (req, res) => {
    try {
      const emailTemplateInfo = await repo.findOneById(req.params.id, req.companyId)
      if (!emailTemplateInfo) return respond.failed(res, 400, `email template with id ${req.params.id} not found`)
      return respond.success(res, emailTemplateInfo, `successfully get email template with id ${emailTemplateInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAll: async (req, res) => {
    try {
      const emailTemplateInfo = await repo.findAll(req.companyId)
      if (!emailTemplateInfo) return respond.failed(res, 400, `email template not exists`)
      return respond.success(res, emailTemplateInfo, `successfully get all email template`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAllBySearchTermAndPaginated: async (req, res) => {
    const { term, page, limit } = req.query;
    try {
      const emailTemplateInfo = await repo.findAllBySearchTermAndPaginated(term, req.companyId, page, limit)
      if (!emailTemplateInfo.data.length) return respond.success(res, emailTemplateInfo, `email template with paginated not exist`)
      respond.success(res, emailTemplateInfo, `successfully get email template paginated`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  getEmailNotification: async (params) => {
    const obj = {
      name: params.name,
      email: params.email,
      mobile_number: params.mobile_number,
      date_of_birth: params.date_of_birth,
      place_of_birth: params.place_of_birth
    }

    try {
      const emailTemplateInfo = await repo.findOneByTemplateForAndStatus(
        params.template_type,
        params.companyId
      )

      if (!emailTemplateInfo) return null;

      let emailMessage = "";
      Object.keys(obj).forEach((k, i) => {
        emailMessage = emailTemplateInfo.body.replace(/{(.*?)}/g, function (match, property) {
          return obj[property];
        })
      })

      const dataMail = await mailer({
        to: obj.email,
        subject: emailTemplateInfo.subject,
        message: emailMessage
      })
      return dataMail
    } catch (err) {
      return err;
    }
  }
})

export default controller;