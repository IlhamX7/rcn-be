'use strict';
import bcrypt from 'bcrypt';
import { check } from 'express-validator'
import jwt from 'jsonwebtoken';

import roleAccesses from '../controllers/role_accesses';
import { deleteCache, setCache } from '../helpers/redis';
import { databaseCommonErrorMessage, respond } from '../helpers/utils'
import companyRepo from '../repository/companies';
import userRepo from '../repository/users';

const controller = Object.freeze({
  usersValidation: [
    check([
      'email',
      'password',
      'mobile_number',
      'role',
      'employement_status',
      'gender',
      'marital_status',
      'id_type',
      'id_number',
      'employee_id',
      'citizen_id_address',
      'residential_address',
      'tax_identification_number'
    ], `input must be a not empty string`).isString().notEmpty(),
    check([
      'position_id',
      'non_taxable_income_id',
      'income_tax_rate_id',
      'leader_id'
    ], `input must be a not empty integer`).isInt().notEmpty(),
    check([
      'account_status',
      'permanent'
    ], `input must be a not empty boolean`).isBoolean().notEmpty(),
    check([
      'id_expiration_date',
      'join_date',
      'contract_expiration_date'
    ], `input must be a not empty date format yyyy-mm-dd`).isDate({format: 'YYYY-MM-DD'}).notEmpty(),
    check([
      'salary'
    ], `input must be a not empty number`).isNumeric().notEmpty(),
    check([
      'date_of_birth'
    ], `input must be a not empty date format yyyy-mm-dd`).isDate({format: 'YYYY-MM-DD'}).optional({nullable: true}),
    check([
      'profile_picture_url',
      'blood_type',
      'place_of_birth'
    ], `input must be a not empty string`).isString().optional({nullable: true}),
    check([
      'province_id',
      'city_id',
      'created_by',
      'approval_by'
    ], `input must be a not empty integer`).isInt().optional({nullable: true})
  ],

  passwordValidation: [
    check([
      'password',
      'new_password'
    ], 'input must be not empty strong password').isStrongPassword().notEmpty()
  ],

  login: async (req, res) => {
    try {
      const user = await userRepo.findByEmail(req.body.email)
      if (!user) {
        return respond.failed(res, 400, 'email is not match, please try again.')
      }

      if (!req.body.third_party) {
        const isMatch = await bcrypt.compare(req.body.password, user.password);
        if (!isMatch) {
          return respond.failed(res, 400, 'password is not match, please try again')
        }
      }

      if(!user.account_status) {
        return respond.failed(res, 400, 'your account is not active, please contact administrator')
      }

      const company = await companyRepo.findOneById(user.company_id)
      if(!company.active) {
        return respond.failed(res, 400, 'your company is not active, please contact administrator')
      }

      const token = jwt.sign(
        {
          userId: user.id,
          role: user.role,
          companyId: user.company_id
        },
        process.env.JWT,
        { expiresIn: Date.now() + 7 * 24 * 60 * 60 },
      );

      setCache(token, user.email);
      const menu = await roleAccesses.getByRoleAndCompany(user.role, user.company_id)

      return res.status(200).json({
        success: true,
        message: 'successfully get user login',
        user: { id: user.id, name: user.name, email: user.email, role: user.role },
        menu: menu,
        token: token
      })
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  logout: async (req, res) => {
    const token = req.headers['x-access-token'];
    if (!token) {
      return respond.failed(res, 400, `Can't find user token`)
    }

    const isLoggedOut = deleteCache(token);
    if (!isLoggedOut) {
      return respond.failed(res, 400, 'Unexpected things happen, please try again')
    }
    return respond.success(res, 400, 'Successfully logged out!')
  },

  create: async (req, res) => {
    try {
      const existedUser = await userRepo.findByEmail(req.body.email)
      if (existedUser) {
        return respond.failed(res, 400, 'The provided email is already exist!')
      }
      const hashedPassword = await userRepo.hash(req.body.password);
      const userInfo = await userRepo.create({
        name : req.body.name,
        email : req.body.email,
        password : hashedPassword,
        mobile_number : req.body.mobile_number,
        role : req.body.role,
        date_of_birth : req.body.date_of_birth,
        place_of_birth : req.body.place_of_birth,
        province_id : req.body.province_id,
        city_id : req.body.city_id,
        profile_picture_url : req.body.profile_picture_url,
        account_status : req.body.account_status,
        employement_status : req.body.employement_status,
        gender : req.body.gender,
        marital_status : req.body.marital_status,
        blood_type : req.body.blood_type,
        religion : req.body.religion,
        id_type : req.body.id_type,
        id_number : req.body.id_number,
        id_expiration_date : req.body.id_expiration_date,
        postal_code : req.body.postal_code,
        citizen_id_address : req.body.citizen_id_address,
        residential_address : req.body.residential_address,
        position_id : req.body.position_id,
        employee_id : req.body.employee_id,
        join_date : req.body.join_date,
        contract_expiration_date : req.body.contract_expiration_date,
        permanent : req.body.permanent,
        created_by : req.userId,
        approval_by : null,
        non_taxable_income_id : req.body.non_taxable_income_id,
        tax_identification_number : req.body.tax_identification_number,
        income_tax_rate_id : req.body.income_tax_rate_id,
        leader_id : req.body.leader_id,
        salary : req.body.salary,
        company_id : req.companyId
      })
      return respond.success(res, userInfo, `user created with id ${userInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  update: async (req, res) => {
    try {
      const userInfo = await userRepo.update({
        id: req.params.id,
        name : req.body.name,
        email : req.body.email,
        mobile_number : req.body.mobile_number,
        role : req.body.role,
        date_of_birth : req.body.date_of_birth,
        place_of_birth : req.body.place_of_birth,
        province_id : req.body.province_id,
        city_id : req.body.city_id,
        profile_picture_url : req.body.profile_picture_url,
        account_status : req.body.account_status,
        employement_status : req.body.employement_status,
        gender : req.body.gender,
        marital_status : req.body.marital_status,
        blood_type : req.body.blood_type,
        religion : req.body.religion,
        id_type : req.body.id_type,
        id_number : req.body.id_number,
        id_expiration_date : req.body.id_expiration_date,
        postal_code : req.body.postal_code,
        citizen_id_address : req.body.citizen_id_address,
        residential_address : req.body.residential_address,
        position_id : req.body.position_id,
        employee_id : req.body.employee_id,
        join_date : req.body.join_date,
        contract_expiration_date : req.body.contract_expiration_date,
        permanent : req.body.permanent,
        non_taxable_income_id : req.body.non_taxable_income_id,
        tax_identification_number : req.body.tax_identification_number,
        income_tax_rate_id : req.body.income_tax_rate_id,
        leader_id : req.body.leader_id,
        salary : req.body.salary
      })
      if (!userInfo) return respond.failed(res, 400, `user with id ${req.params.id} not found`)
      return respond.success(res, userInfo, `user updated with id ${userInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  updatePassword: async (req, res) => {
    try {
      if (req.body.password === req.body.new_password) {
        return respond.failed(res, 400, 'New password should be different than current one')
      }
      const user = await userRepo.findByIdAndAccountStatusAndExcludeCondition(req.userId, true, req.companyId)
      if (!user) {
        return respond.failed(res, 400, 'Please login first')
      }
      const isMatch = await bcrypt.compare(req.body.password, user.password);
      if (!isMatch) {
        return respond.failed(res, 400, 'Please correctly insert the current password')
      }
      const hashedNewPassword = await userRepo.hash(req.body.new_password);
    
      const userInfo = await userRepo.updatePassword(hashedNewPassword, user.id)
      return respond.success(res, userInfo, `Successfully changed the password with id ${userInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  profile: async (req, res) => {
    try {
      if (!req.userId) {
        return respond.failed(res, 400, 'Please login first')
      }
      const user = await userRepo.findByIdAndAccountStatusAndExcludeCondition(req.userId, true, req.companyId, true)
      if (!user) {
        respond.failed(res, 400, `Unable to get the current user's profile`)
      }
      return respond.success(res, user, `Successfully get the current user's profile`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findOneById: async (req, res) => {
    try {
      const userInfo = await userRepo.findOneById(req.params.id, req.companyId)
      if (!userInfo) return respond.failed(res, 400, `user with id ${req.params.id} not found`)
      return respond.success(res, userInfo, `successfully get user with id ${userInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAllBySearchTermAndPaginated: async (req, res) => {
    const { term, page, limit } = req.query;
    try {
      const userInfo = await userRepo.findAllBySearchTermAndPaginated(term, req.companyId, page, limit)
      if (!userInfo.data.length) return respond.success(res, userInfo, `user with paginated not exist`)
      respond.success(res, userInfo, `successfully get user paginated`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },
})

export default controller;