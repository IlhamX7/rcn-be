'use strict';
import { check } from 'express-validator'

import { databaseCommonErrorMessage, respond } from '../helpers/utils'
import repo from '../repository/type_of_leaves';

const controller = Object.freeze({
  TypeOfLeaveValidation: [
    check('title', 'input must be a not empty string').isString().notEmpty(),
    check('time_period', 'input must be a not empty integer').isInt().notEmpty(),
    check('active', 'input must be a not empty boolean').isBoolean().notEmpty(),
  ],

  create: async (req, res) => {
    try {
      const TypeOfLeaveInfo = await repo.create({
        title: req.body.title,
        time_period: req.body.time_period,
        active: req.body.active,
        company_id: req.companyId
      })
      return respond.success(res, TypeOfLeaveInfo, `type of leave created with id ${TypeOfLeaveInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  update: async (req, res) => {
    try {
      const TypeOfLeaveInfo = await repo.update({
        id: req.params.id,
        title: req.body.title,
        time_period: req.body.time_period,
        active: req.body.active
      })
      if (!TypeOfLeaveInfo) return respond.failed(res, 400, `type of leave with id ${req.params.id} not found`)
      return respond.success(res, TypeOfLeaveInfo, `type of leave updated with id ${TypeOfLeaveInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  delete: async (req, res) => {
    try {
      const TypeOfLeaveInfo = await repo.delete(req.params.id)
      if (TypeOfLeaveInfo === 0) return respond.failed(res, 400, `type of leave with id ${req.params.id} not found`)
      return respond.success(res, TypeOfLeaveInfo, `type of leave deleted with id ${req.params.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findOneById: async (req, res) => {
    try {
      const TypeOfLeaveInfo = await repo.findOneById(req.params.id, req.companyId)
      if (!TypeOfLeaveInfo) return respond.failed(res, 400, `type of leave with id ${req.params.id} not found`)
      return respond.success(res, TypeOfLeaveInfo, `successfully get type of leave with id ${TypeOfLeaveInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAll: async (req, res) => {
    try {
      const TypeOfLeaveInfo = await repo.findAll(req.companyId)
      if (!TypeOfLeaveInfo) return respond.failed(res, 400, `type of leave not exists`)
      return respond.success(res, TypeOfLeaveInfo, `successfully get all type of leave`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAllBySearchTermAndPaginated: async (req, res) => {
    const { term, page, limit } = req.query;
    try {
      const TypeOfLeaveInfo = await repo.findAllBySearchTermAndPaginated(term, req.companyId, page, limit)
      if (!TypeOfLeaveInfo.data.length) return respond.success(res, TypeOfLeaveInfo, `type of leave with paginated not exist`)
      respond.success(res, TypeOfLeaveInfo, `successfully get type of leave paginated`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  }
})

export default controller;