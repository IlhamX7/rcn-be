'use strict';
import { check } from 'express-validator'

import { databaseCommonErrorMessage, respond } from '../helpers/utils'
import repo from '../repository/non_taxable_incomes';

const controller = Object.freeze({
  nonTaxableIncomesValidation: [
    check('title',`input must be a not empty string`).isString().notEmpty(),
    check('rate', `input must be a integer`).isDecimal().optional({ nullable: true })
  ],

  create: async (req, res) => {
    try {
      const nonTaxableIncomesInfo = await repo.create({
        title: req.body.title,
        rate: req.body.rate,
        company_id: req.companyId
      })
      return respond.success(res, nonTaxableIncomesInfo, `Non Taxable Incomes created with id ${nonTaxableIncomesInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  update: async (req, res) => {
    try {
      const nonTaxableIncomesInfo = await repo.update({
        id: req.params.id,
        title: req.body.title,
        rate: req.body.rate
      })
      if (!nonTaxableIncomesInfo) return respond.failed(res, 400, `Non taxable incomes with id ${req.params.id} not found`)
      return respond.success(res, nonTaxableIncomesInfo, `Non taxable incomes updated with id ${nonTaxableIncomesInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  delete: async (req, res) => {
    try {
      const nonTaxableIncomesInfo = await repo.delete(req.params.id)
      if (nonTaxableIncomesInfo === 0) return respond.failed(res, 400, `Non taxable incomes info with id ${req.params.id} not found`)
      return respond.success(res, nonTaxableIncomesInfo, `Non taxable incomes deleted with id ${req.params.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findOneById: async (req, res) => {
    try {
      const nonTaxableIncomesInfo = await repo.findOneById(req.params.id, req.companyId)
      if(!nonTaxableIncomesInfo) return respond.failed(res, 400, `Non taxable incomes with id ${req.params.id} not found`)
      return respond.success(res, nonTaxableIncomesInfo, `successfully get non taxable incomes with id ${nonTaxableIncomesInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAll: async (req, res) => {
    try {
      const nonTaxableIncomesInfo = await repo.findAll(req.companyId)
      if (!nonTaxableIncomesInfo) return respond.failed(res, 400, `Non taxable incomes not exists`)
      return respond.success(res, nonTaxableIncomesInfo, `successfully get all non taxable incomes`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAllBySearchTermAndPaginated: async (req, res) => {
    const { term, page, limit } = req.query;
    try {
      const nonTaxableIncomesInfo = await repo.findAllBySearchTermAndPaginated(term, req.companyId, page, limit)
      if (!nonTaxableIncomesInfo.data.length) return respond.success(res, nonTaxableIncomesInfo, `Non taxable incomes with paginated not exist`)
      respond.success(res, nonTaxableIncomesInfo, `successfully get non taxable incomes paginated`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  }
})

export default controller;