'use strict';
import { check } from 'express-validator'

import { databaseCommonErrorMessage, respond } from '../helpers/utils'
import repo from '../repository/departements';

const controller = Object.freeze({
  departementValidation: [
    check('title', `input must be a not empty string`).isString().notEmpty(),
    check('active', `input must be a integer`).isBoolean().notEmpty()
  ],

  create: async (req, res) => {
    try {
      const departementInfo = await repo.create({
        title: req.body.title,
        active: req.body.active,
        company_id: req.companyId
      })
      return respond.success(res, departementInfo, `departement created with id ${departementInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  update: async (req, res) => {
    try {
      const departementInfo = await repo.update({
        id: req.params.id,
        title: req.body.title,
        active: req.body.active
      })
      if (!departementInfo) return respond.failed(res, 400, `departement with id ${req.params.id} not found`)
      return respond.success(res, departementInfo, `departement updated with id ${departementInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  delete: async (req, res) => {
    try {
      const departementInfo = await repo.delete(req.params.id)
      if (departementInfo === 0) return respond.failed(res, 400, `departement with id ${req.params.id} not found`)
      return respond.success(res, departementInfo, `departement deleted with id ${req.params.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findOneById: async (req, res) => {
    try {
      const departementInfo = await repo.findOneById(req.params.id, req.companyId)
      if(!departementInfo) return respond.failed(res, 400, `departement with id ${req.params.id} not found`)
      return respond.success(res, departementInfo, `successfully get departement with id ${departementInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAll: async (req, res) => {
    try {
      const departementInfo = await repo.findAll(req.companyId)
      if (!departementInfo) return respond.failed(res, 400, `departement not exists`)
      return respond.success(res, departementInfo, `successfully get all departement`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAllBySearchTermAndPaginated: async (req, res) => {
    const { term, page, limit } = req.query;
    try {
      const departementInfo = await repo.findAllBySearchTermAndPaginated(term, req.companyId, page, limit)
      if (!departementInfo.data.length) return respond.success(res, departementInfo, `departement with paginated not exist`)
      respond.success(res, departementInfo, `successfully get departement paginated`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  }
})

export default controller;