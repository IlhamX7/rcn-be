'use strict';
import { check } from 'express-validator'

import { databaseCommonErrorMessage, respond } from '../helpers/utils'
import repo from '../repository/companies';

const controller = Object.freeze({
  companyValidation: [
    check([
      'company_name',
      'organization_name'
    ], `input must be a not empty string`).isString().notEmpty(),
    check('branch_id', (value, { path }) => `${path} input must be a integer`).isInt().notEmpty(),
    check([
      'company_logo_url',
      'address',
      'telp',
      'email',
      'website_address'
    ], `input must be a string or null`).isString().optional({ nullable: true }),
    check('founded_date', `input must be a string or null`).isDate({format: 'YYYY-MM-DD'}).notEmpty(),
    check('active', `input must be a not empty boolean`).isBoolean().notEmpty()
  ],

  create: async (req, res) => {
    try {
      const companyInfo = await repo.create({
        company_name: req.body.company_name,
        organization_name: req.body.organization_name,
        branch_id: req.body.branch_id,
        company_logo_url: req.body.company_logo_url,
        address: req.body.address,
        telp: req.body.telp,
        email: req.body.email,
        founded_date: req.body.founded_date,
        website_address: req.body.website_address,
        active: req.body.active,
        created_by: req.userId,
        updated_by: req.userId,
      })
      return respond.success(res, companyInfo, `company created with id ${companyInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  update: async (req, res) => {
    try {
      const companyInfo = await repo.update({
        id: req.params.id,
        company_name: req.body.company_name,
        organization_name: req.body.organization_name,
        branch_id: req.body.branch_id,
        company_logo_url: req.body.company_logo_url,
        address: req.body.address,
        telp: req.body.telp,
        email: req.body.email,
        founded_date: req.body.founded_date,
        website_address: req.body.website_address,
        active: req.body.active,
        updated_by: req.userId
      })
      if (!companyInfo) return respond.failed(res, 400, `company with id ${req.params.id} not found`)
      return respond.success(res, companyInfo, `company updated with id ${companyInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  delete: async (req, res) => {
    try {
      const companyInfo = await repo.delete(req.params.id)
      if (companyInfo === 0) return respond.failed(res, 400, `company with id ${req.params.id} not found`)
      return respond.success(res, companyInfo, `company deleted with id ${req.params.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findOneById: async (req, res) => {
    try {
      const companyInfo = await repo.findOneById(req.params.id)
      if(!companyInfo) return respond.failed(res, 400, `company with id ${req.params.id} not found`)
      return respond.success(res, companyInfo, `successfully get company with id ${companyInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAll: async (req, res) => {
    try {
      const companyInfo = await repo.findAll()
      if (!companyInfo) return respond.failed(res, 400, `company not exists`)
      return respond.success(res, companyInfo, `successfully get all company`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAllBySearchTermAndPaginated: async (req, res) => {
    const { term, page, limit } = req.query;
    try {
      const companyInfo = await repo.findAllBySearchTermAndPaginated(term, page, limit)
      if (!companyInfo.data.length) return respond.success(res, companyInfo, `company with paginated not exist`)
      respond.success(res, companyInfo, `successfully get company paginated`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  }
})

export default controller;