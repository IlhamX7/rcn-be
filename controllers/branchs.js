'use strict';
import { check } from 'express-validator'

import { databaseCommonErrorMessage, respond } from '../helpers/utils'
import repo from '../repository/branchs';

const controller = Object.freeze({
  branchValidation: [
    check([
      'title',
      'description'
    ], `input must be a not empty string`).isString().notEmpty()
  ],

  create: async (req, res) => {
    try {
      const branchInfo = await repo.create({
        title: req.body.title,
        description: req.body.description,
        created_by: req.userId,
        updated_by: req.userId
      })
      return respond.success(res, branchInfo, `branch created with id ${branchInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  update: async (req, res) => {
    try {
      const branchInfo = await repo.update({
        id: req.params.id,
        title: req.body.title,
        description: req.body.description,
        updated_by: req.userId
      })
      if (!branchInfo) return respond.failed(res, 400, `branch with id ${req.params.id} not found`)
      return respond.success(res, branchInfo, `branch updated with id ${branchInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  delete: async (req, res) => {
    try {
      const branchInfo = await repo.delete(req.params.id)
      if (branchInfo === 0) return respond.failed(res, 400, `branch with id ${req.params.id} not found`)
      return respond.success(res, branchInfo, `branch deleted with id ${req.params.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findOneById: async (req, res) => {
    try {
      const branchInfo = await repo.findOneById(req.params.id)
      if(!branchInfo) return respond.failed(res, 400, `branch with id ${req.params.id} not found`)
      return respond.success(res, branchInfo, `successfully get branch with id ${branchInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAll: async (req, res) => {
    try {
      const branchInfo = await repo.findAll()
      if (!branchInfo) return respond.failed(res, 400, `branch not exists`)
      return respond.success(res, branchInfo, `successfully get all branch`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAllBySearchTermAndPaginated: async (req, res) => {
    const { term, page, limit } = req.query;
    try {
      const branchInfo = await repo.findAllBySearchTermAndPaginated(term, page, limit)
      if (!branchInfo.data.length) return respond.success(res, branchInfo, `branch with paginated not exist`)
      respond.success(res, branchInfo, `successfully get branch paginated`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  }
})

export default controller;