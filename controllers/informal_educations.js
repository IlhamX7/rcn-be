'use strict';
import { check } from 'express-validator'

import { databaseCommonErrorMessage, respond } from '../helpers/utils'
import repo from '../repository/informal_educations';

const controller = Object.freeze({
  informalEducationValidation: [
    check('name', `input must be a not empty string`).isString().notEmpty(),
    check('held_by', `input must be a not empty string`).isString().notEmpty(),
    check('start_date', `input must be a not empty string`).isString().notEmpty(),
    check('end_date', `input must be a not empty string`).isString().notEmpty(),
    check('fee', `input must be a not empty string`).isString().notEmpty(),
    check('certificate', `input must be a not empty string`).isString().notEmpty(),
    check('user_id', `input must be a integer`).isInt().optional({ nullable: false })
  ],

  create: async (req, res) => {
    try {
      const informalEducationInfo = await repo.create({
        name: req.body.name,
        held_by: req.body.held_by,
        start_date: req.body.start_date,
        end_date: req.body.end_date,
        fee: req.body.fee,
        certificate: req.body.certificate,
        user_id: req.body.user_id,
        company_id: req.companyId
      })
      return respond.success(res, informalEducationInfo, `informal education created with id ${informalEducationInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  update: async (req, res) => {
    try {
      const informalEducationInfo = await repo.update({
        id: req.params.id,
        name: req.body.name,
        held_by: req.body.held_by,
        start_date: req.body.start_date,
        end_date: req.body.end_date,
        fee: req.body.fee,
        certificate: req.body.certificate
      })
      if (!informalEducationInfo) return respond.failed(res, 400, `informal education with id ${req.params.id} not found`)
      return respond.success(res, informalEducationInfo, `informal education updated with id ${informalEducationInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  delete: async (req, res) => {
    try {
      const informalEducationInfo = await repo.delete(req.params.id)
      if (informalEducationInfo === 0) return respond.failed(res, 400, `informal education with id ${req.params.id} not found`)
      return respond.success(res, informalEducationInfo, `informal education deleted with id ${req.params.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findOneById: async (req, res) => {
    try {
      const informalEducationInfo = await repo.findOneById(req.params.id, req.companyId)
      if(!informalEducationInfo) return respond.failed(res, 400, `informal education with id ${req.params.id} not found`)
      return respond.success(res, informalEducationInfo, `successfully get informal education with id ${informalEducationInfo.id}`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAll: async (req, res) => {
    try {
      const informalEducationInfo = await repo.findAll(req.companyId)
      if (!informalEducationInfo) return respond.failed(res, 400, `informal education not exists`)
      return respond.success(res, informalEducationInfo, `successfully get all informal education`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  },

  findAllBySearchTermAndPaginated: async (req, res) => {
    const { term, page, limit } = req.query;
    try {
      const informalEducationInfo = await repo.findAllBySearchTermAndPaginated(term, req.companyId, page, limit)
      if (!informalEducationInfo.data.length) return respond.success(res, informalEducationInfo, `informal education with paginated not exist`)
      respond.success(res, informalEducationInfo, `successfully get informal education paginated`)
    } catch (err) {
      return respond.failed(res, 500, databaseCommonErrorMessage.message(err))
    }
  }
})

export default controller;