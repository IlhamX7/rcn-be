import express from 'express'

import { mailer } from '../helpers/utils'
const router = express.Router()

/* GET home page. */
router.get('/', async function(req, res, next) {
  res.send('Welcome to RCN Backend')
});

router.get('/testsocket', async function(req, res) {
  req.io.to('hamzah@edukita.id').emit('message', { data: 'test socket io' })
  res.send('ok')
})

router.get('/testemail', async function(req, res) {
  try {
    const dataMail = await mailer({
      to: 'if.hamzah93@gmail.com',
      subject: 'testing email',
      message: 'email dns error'
    })
    res.json(dataMail)
  } catch (error) {
    res.json(error)
  }
})

module.exports = router
