import { Router } from 'express';

import controller from '../../controllers/users';
import authJwt from '../../helpers/authJwt'
import { validationChecker } from '../../helpers/utils';

const router = Router();

router.post('/login', controller.login);
router.post('/', [authJwt.checkToken, authJwt.checkRole, controller.usersValidation, validationChecker], controller.create);
router.put('/update-password', [authJwt.checkToken, controller.passwordValidation, validationChecker], controller.updatePassword);
router.put('/:id', [authJwt.checkToken, authJwt.checkToken, controller.usersValidation, validationChecker], controller.update);
router.post('/logout',[authJwt.checkToken], controller.logout);
router.get('/profile', [authJwt.checkToken], controller.profile);
router.get('/', [authJwt.checkToken, authJwt.checkRole], controller.findAllBySearchTermAndPaginated)
router.get('/:id', [authJwt.checkToken, authJwt.checkRole], controller.findOneById)

export default router