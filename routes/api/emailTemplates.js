import { Router } from 'express';

import controller from '../../controllers/email_template';
import authJwt from '../../helpers/authJwt';
import { validationChecker } from '../../helpers/utils';

const router = Router()

router.post('/', [authJwt.checkToken, authJwt.checkRole, controller.menuTemplateValidation, validationChecker], controller.create)
router.put('/:id', [authJwt.checkToken, authJwt.checkRole, controller.menuTemplateValidation, validationChecker], controller.update)
router.delete('/:id', [authJwt.checkToken, authJwt.checkRole], controller.delete)
router.get('/', [authJwt.checkToken, authJwt.checkRole], controller.findAllBySearchTermAndPaginated)
router.get('/all', [authJwt.checkToken, authJwt.checkRole], controller.findAll)
router.get('/:id', [authJwt.checkToken, authJwt.checkRole], controller.findOneById)

module.exports = router;