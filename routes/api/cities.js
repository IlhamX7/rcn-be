import { Router } from 'express';

import controller from '../../controllers/cities';

const router = Router();

router.get('/', controller.findAllBySearchTermAndPaginated)
router.get('/all', controller.findAll)
router.get('/:id', controller.findOneById);

export default router;
