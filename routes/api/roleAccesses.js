import { Router } from 'express';

import controller from '../../controllers/role_accesses';
import authJwt from '../../helpers/authJwt';
import { validationChecker } from '../../helpers/utils';

const router = Router()

router.post('/', [authJwt.checkToken, authJwt.checkRole, controller.roleAccessValidation, validationChecker], controller.createOrUpdate)
router.get('/group/:id', [authJwt.checkToken, authJwt.checkRole], controller.findAllBySearchTermAndPaginated)
router.get('/', [authJwt.checkToken, authJwt.checkRole], controller.findByRole)
router.put('/order/down/:ordering/:roleId', [authJwt.checkToken, authJwt.checkRole], controller.moveDown)
router.put('/order/up/:ordering/:roleId', [authJwt.checkToken, authJwt.checkRole], controller.moveUp)

module.exports = router;