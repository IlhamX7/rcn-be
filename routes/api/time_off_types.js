import { Router } from 'express';

import controller from '../../controllers/time_off_types';
import authJwt from '../../helpers/authJwt';
import { validationChecker } from '../../helpers/utils';

const router = Router()

router.post('/', [authJwt.checkToken, authJwt.checkRole, controller.timeOffTypeValidation, validationChecker], controller.create)
router.put('/:id', [authJwt.checkToken, authJwt.checkRole, controller.timeOffTypeValidation, validationChecker], controller.update)
router.delete('/:id', [authJwt.checkToken, authJwt.checkRole], controller.delete)
router.get('/', [authJwt.checkToken, authJwt.checkRole], controller.findAllBySearchTermAndPaginated)
router.get('/all', [authJwt.checkToken, authJwt.checkRole], controller.findAll)
router.get('/:id', [authJwt.checkToken, authJwt.checkRole], controller.findOneById)

module.exports = router;