import { Router } from 'express';

import controller from '../../controllers/provinces';

const router = Router();

router.get('/', controller.findAllBySearchTermAndPaginated)
router.get('/all', controller.findAll)
router.get('/:id', controller.findOneById);

export default router;
