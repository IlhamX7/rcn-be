import { Router } from 'express';

import allowances from "./allowances"
import allowanceType from "./allowanceTypes"
import attendanceShifts from "./attendance_shifts"
import attendances from "./attendances"
import bankAccounts from "./bankAccounts"
import branchs from "./branchs"
import cities from './cities';
import companies from "./companies"
import departements from "./departements"
import emailTemplates from "./emailTemplates"
import formalEducations from "./formalEducations"
import groupUsers from "./groupUsers"
import incomeTaxRates from "./incomeTaxRates"
import informalEducations from "./informalEducations"
import menus from "./menus"
import nonTaxableIncomes from "./nonTaxableIncomes"
import overtimes from "./overtimes"
import positions from "./positions"
import provinces from './provinces';
import roleAccesses from "./roleAccesses"
import timeOffTypes from "./time_off_types"
import timeOffs from "./time_offs"
import typeOfLeaves from "./type_of_leaves";
import users from './users';
import workingExperiences from "./workingExperiences"

const router = Router();

router.use('/users', users);
router.use('/provinces', provinces);
router.use('/cities', cities);
router.use('/email-templates', emailTemplates);
router.use('/group-users', groupUsers);
router.use('/menus', menus);
router.use('/role-accesses', roleAccesses);
router.use('/branchs', branchs);
router.use('/non-taxable-incomes', nonTaxableIncomes);
router.use('/departements', departements);
router.use('/companies', companies);
router.use('/income-tax-rates', incomeTaxRates);
router.use('/attendances', attendances);
router.use('/attendance-shifts', attendanceShifts);
router.use('/time-off-types', timeOffTypes);
router.use('/time-offs', timeOffs);
router.use('/type-of-leaves', typeOfLeaves);
router.use('/positions', positions);
router.use('/overtimes', overtimes);
router.use('/formal-educations', formalEducations);
router.use('/informal-educations', informalEducations);
router.use('/working-experiences', workingExperiences);
router.use('/bank-accounts', bankAccounts);
router.use('/allowance-types', allowanceType);
router.use('/allowances', allowances);

export default router;
