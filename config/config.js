require('dotenv').config()
module.exports = {
  development: {
    username: process.env.DB_USERNAME || '',
    password: process.env.DB_PASSWORD || '',
    database: process.env.DB_DATABASE || '',
    host: process.env.DB_HOST || '127.0.0.1',
    dialect: 'postgresql',
    redis_port : 6379,
    redis_host : process.env.REDIS_HOST || '127.0.0.1',
    redis_jwt : process.env.JWT
  },
  test: {
    username: process.env.DB_USERNAME || '',
    password: process.env.DB_PASSWORD || '',
    database: process.env.DB_DATABASE || '',
    host: process.env.DB_HOST_TEST || '127.0.0.1',
    dialect: 'postgresql',
    redis_port : 6379,
    redis_host : process.env.REDIS_HOST_TEST || '127.0.0.1',
    redis_jwt : process.env.JWT
  },
  production: {
    username: process.env.DB_USERNAME || '',
    password: process.env.DB_PASSWORD || '',
    database: process.env.DB_DATABASE || '',
    host: process.env.DB_HOST || '127.0.0.1',
    dialect: 'postgresql',
    redis_port : 6379,
    redis_host : '127.0.0.1',
    redis_jwt : process.env.JWT
  }
};
